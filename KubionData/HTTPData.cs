﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using KubionLogNamespace;

namespace KubionDataNamespace
{
    class HTTPData:IData 
    {
        string strUrl;
        string strUserName;
        string strPassword;
        string strDomain ;
        string strProxyUrl;
        string strProxyUserName ;
        string strProxyPassword ;
        string strProxyDomain ;
        CookieContainer cookieContainer = null;

        public HTTPData(string sConnectionString)
        {
            try
            {
                strUrl = ParseConnString(sConnectionString);
                if (GetConnStringValue(sConnectionString, "Session") == "1")
                     cookieContainer = new CookieContainer();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
        }
        private string ParseConnString(string sURI)
        {
            strUrl = GetConnStringValue(sURI, "URI");
            strUserName = GetConnStringValue(sURI, "UserName");
            strPassword = GetConnStringValue(sURI, "Password");
            strDomain = GetConnStringValue(sURI, "Domain");
            strProxyUrl = GetConnStringValue(sURI, "ProxyURI");
            strProxyUserName = GetConnStringValue(sURI, "ProxyUserName");
            strProxyPassword = GetConnStringValue(sURI, "ProxyPassword");
            strProxyDomain = GetConnStringValue(sURI, "ProxyDomain");
            return strUrl;
        }
        private string GetConnStringValue(string strURI, string strKey)
        {
            int intStart = (";" + strURI).IndexOf(";" + strKey + "=", StringComparison.CurrentCultureIgnoreCase);
            if (intStart == -1) return "";
            intStart += (strKey + "=").Length;
            int intEnd = (strURI + ";").IndexOf(";", intStart);
            return (strURI + ";").Substring(intStart, (intEnd - intStart));
        }

        /*
         * ConnectionString: URI=http://service.woco.local/web.svc/;UserName=;Password=;Domain=;LoginProvider=;ProxyUrl=;ProxyUserName=;ProxyPassword=;ProxyDomain=;ProxyLoginProvider=;
         * Query:
         * [RequestUri]				GetDocuments
         * [RequestMethod]			POST
         * [RequestHeaders]			Content-Type~text/xml;charset=UTF-8|SOAPAction~urn:www-infosupport-com:corporatieservicebus:bsovereenkomst:v1:ibsovereenkomst/IBSOvereenkomst/ZoekHuuropzeggingReden|
         * [RequestDataEncoding]	utf-8
         * [RequestDataString]		<xml />
         */
        private string[] parseHTTPQuery(string sqlString)
        {
            string[] aLines = new string[5];

            for (int i = 0; i < 4; i++)
            {
                if (sqlString.IndexOf("\r\n") != -1)
                {
                    aLines[i] = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                    sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
                }
                else
                {
                    aLines[i] = sqlString;
                    sqlString = "";
                }
            }
            aLines[4] = (sqlString == "\r\n") ? "" : sqlString;
            return aLines ;
        }

        private string GetHTTPResponse(string strRequestUri, string strRequestUserName, string strRequestPassword, string strRequestDomain
            , string strProxyUri, string strProxyUserName, string strProxyPassword, string strProxyDomain
            , string strRequestMethod, string strRequestHeaders, string strRequestDataEncoding, string strRequestDataString)
        {
            if (strRequestUri == null) throw new Exception("Missing RequestUri");
            if (strRequestMethod == null || strRequestMethod == "") strRequestMethod = "GET";

            System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strRequestUri);
            if (cookieContainer != null) request.CookieContainer = cookieContainer;
            request.Method = strRequestMethod;
            foreach (string strRequestHeader in strRequestHeaders.Split('|'))
            {
                if (strRequestHeader.Split('~').Length == 2)
                {
                    if ((strRequestHeader.Split('~')[0].ToLower()) == "accept")
                    {
                        request.Accept = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "connection")
                    {
                        request.Connection = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-length")
                    {
                        request.ContentLength = long.Parse(strRequestHeader.Split('~')[1]);
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-type")
                    {
                        request.ContentType = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "date") { }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "expect")
                    {
                        request.Expect = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "host") { }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "if-modified-since")
                    {
                        request.IfModifiedSince = DateTime.Parse(strRequestHeader.Split('~')[1]);
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "range") { }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "referer")
                    {
                        request.Referer = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "transfer-encoding")
                    {
                        request.TransferEncoding = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "user-agent")
                    {
                        request.UserAgent = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "proxy-connection") { }
                    else
                    {
                        request.Headers.Add(strRequestHeader.Split('~')[0], strRequestHeader.Split('~')[1]);
                    }
                }
            }

            /* Provides credentials for password-based authentication schemes such as basic, digest, NTLM, and Kerberos authentication */
            if ((strRequestUserName == "" || strRequestUserName == null) && (strRequestPassword == "" || strRequestPassword == null) && (strRequestDomain == "" || strRequestDomain == null))
            {
                /* Set the credentials to be like impersonate */
                request.Credentials = System.Net.CredentialCache.DefaultCredentials;
            }
            else if (strRequestDomain == "" || strRequestDomain == null)
            {
                request.Credentials = new System.Net.NetworkCredential(strRequestUserName, strRequestPassword);
            }
            else
            {
                request.Credentials = new System.Net.NetworkCredential(strRequestUserName, strRequestPassword, strRequestDomain);
            }

            if (strProxyUri != "" && strProxyUri != null)
            {
                WebProxy proxy = new WebProxy();
                proxy.Address = new Uri(strProxyUri);
                if (strProxyUserName != "" && strProxyUserName != null)
                {
                    proxy.Credentials = new NetworkCredential(strProxyUserName, (strProxyPassword == null ? "" : strProxyPassword), (strProxyDomain == null ? "" : strProxyDomain));
                }
                request.Proxy = proxy;
            }

            /* Send the content */
            if (strRequestDataString.Length > 0)
            {
                Encoding objEncoding = Encoding.Default;
                if (strRequestDataEncoding != null && strRequestDataEncoding != "")
                {
                    try { objEncoding = Encoding.GetEncoding(strRequestDataEncoding); }
                    catch (Exception e) { objEncoding = Encoding.Default; }
                }
                byte[] postData = objEncoding.GetBytes(strRequestDataString);
                request.ContentLength = postData.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(postData, 0, postData.Length);
                requestStream.Close();
            }

            /* Read the response XML */
            WebResponse response = null;
            try { response = request.GetResponse(); }
            catch (WebException webex)
            {
                /* Need some special attention to get the error message from the response */
                String data = String.Empty;
                if (webex.Response != null)
                {
                    StreamReader r = new StreamReader(webex.Response.GetResponseStream());
                    data = r.ReadToEnd();
                    r.Close();
                }
                throw new Exception(webex.Message + "\r\n" + data + "\r\nURL (decoded): " + strRequestUri + "\r\nPosted data (decoded): " + System.Web.HttpUtility.UrlDecode(strRequestDataString, Encoding.Default));
            }
            Stream responseStream = response.GetResponseStream();
            StreamReader sr = new StreamReader(responseStream);
            string strResponse = sr.ReadToEnd();

            string strReplace = (char)0x0 + "|" + (char)0x1 + "|" + (char)0x2 + "|" + (char)0x3 + "|" + (char)0x4 + "|" + (char)0x5 + "|" + (char)0x6 + "|" + (char)0x7 + "|" + (char)0x8 + "|" + (char)0xB + "|" + (char)0xC + "|" + (char)0xE + "|" + (char)0xF + "|" + (char)0x10 + "|" + (char)0x11 + "|" + (char)0x12 + "|" + (char)0x13 + "|" + (char)0x14 + "|" + (char)0x15 + "|" + (char)0x1A + "|" + (char)0x1B + "|" + (char)0x1C + "|" + (char)0x1D + "|" + (char)0x1E + "|" + (char)0x1F + (char)0x16 + "|" + (char)0x17 + "|" + (char)0x18 + "|" + (char)0x19 + "|" + (char)0x7F;
            Regex pattern = new Regex(strReplace);
            strResponse = pattern.Replace(strResponse, " ");

            return strResponse;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public string GetResponse(string strCommandUrl)
        {
                string[] aLines = parseHTTPQuery(strCommandUrl);
                string strCmd = aLines [0];
                string strMethod = aLines[1];
                string strHeaders = aLines[2];
                string strContentType = aLines[3];
                string strData = aLines[4];
                strContentType = (strContentType == "") ? "application/x-www-form-urlencoded" : System.Web.HttpUtility.UrlDecode(strContentType, Encoding.Default);

                return GetHTTPResponse(strUrl + strCmd, strUserName, strPassword, strDomain, strProxyUrl, strProxyUserName, strProxyPassword, strProxyDomain, strMethod, strHeaders, strContentType, strData);
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public DataTable GetDataTable(string sqlString)
        {
            string strResponse = GetResponse(sqlString);
            DataTable ret = new DataTable();
            ret.Columns.Add("Response", System.Type.GetType("System.String"));
            DataRow objDataRow = ret.NewRow();
            objDataRow["Response"] = strResponse;
            ret.Rows.Add(objDataRow);
            return ret;
        }
        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            string strResponse = GetResponse(sqlString);
            DataTable ret = new DataTable();
            ret.Columns.Add("Response", System.Type.GetType("System.String"));
            DataRow objDataRow = ret.NewRow();
            objDataRow["Response"] = strResponse;
            ret.Rows.Add(objDataRow);
            m_Response = strResponse;
            return ret;
        }
        public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
        {
            return GetDataTable(sqlString);
        }



        public string ExecuteNonQuery(string sqlString)
        {
            string strResponse = "";
            string strOuterXml = "";
            string strInnerXml = "";

            return ExecuteNonQuery(sqlString, ref strResponse, ref strOuterXml, ref strInnerXml);
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            try
            {
                return GetResponse(sqlString);
            }
            catch (Exception ex)
            {
                throw;
            }
            return "";
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            return ExecuteNonQuery(sqlString);
        }

        public void BeginTransaction()
        {
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
        }
        public void CommitTransaction()
        {
        }
        public void RollbackTransaction()
        {
        }
        private int iCommandTimeout = 30;
        public int CommandTimeout { get { return iCommandTimeout; } set { iCommandTimeout = value; } }
        public void Dispose()
        {
        }
        private void OpenConnection()
        {
        }
        public bool ConnOpen()
        {
            return true;
        }
        public DataTable GetSchema(string sqlString)
        {
            return null;
        }
        public IDataReader GetDataReader(string sqlString)
        {
            return null;
        }

    }
}
