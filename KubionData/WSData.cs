using System;
using System.Data;
using System.Configuration;
using System.Diagnostics;
using KubionLogNamespace;

namespace KubionDataNamespace
{
    public class WSData : IData
    {
        private string sURI;
        private string sCommand;
        private WebserviceConsumer objWebserviceConsumer;
        private int iCommandTimeout = 30;
        public int CommandTimeout { get { return iCommandTimeout; } set { iCommandTimeout = value; } }


        //Provider=WS;
        //URI=http://software.kubion.nl/iris_repository/;
        //UserName=sa;
        //Password=www;
        //Domain=Kubion;
        //ProxyURI=isa.wgt.local:8080;
        //ProxyUserName=proxyuser;
        //ProxyPassword=1234;
        //ProxyDomain=Domijn;
        private string ParseURI(string sURI)
        {
            string strURI = GetURIValue(sURI, "URI");
            string strUserName = GetURIValue(sURI, "UserName");
            string strPassword = GetURIValue(sURI, "Password");
            string strDomain = GetURIValue(sURI, "Domain");
            string strProxyURI = GetURIValue(sURI, "ProxyURI");
            string strProxyUserName = GetURIValue(sURI, "ProxyUserName");
            string strProxyPassword = GetURIValue(sURI, "ProxyPassword");
            string strProxyDomain = GetURIValue(sURI, "ProxyDomain");
            string strCommand = "";
            strCommand += "&url=" + System.Web.HttpUtility.UrlEncode(strURI, System.Text.Encoding.Default);
            strCommand += "&username=" + System.Web.HttpUtility.UrlEncode(strUserName, System.Text.Encoding.Default);
            strCommand += "&password=" + System.Web.HttpUtility.UrlEncode(strPassword, System.Text.Encoding.Default);
            strCommand += "&domain=" + System.Web.HttpUtility.UrlEncode(strDomain, System.Text.Encoding.Default);
            strCommand += "&proxyurl=" + System.Web.HttpUtility.UrlEncode(strProxyURI , System.Text.Encoding.Default);
            strCommand += "&proxyusername=" + System.Web.HttpUtility.UrlEncode(strProxyUserName, System.Text.Encoding.Default);
            strCommand += "&proxypassword=" + System.Web.HttpUtility.UrlEncode(strProxyPassword, System.Text.Encoding.Default);
            strCommand += "&proxydomain=" + System.Web.HttpUtility.UrlEncode(strProxyDomain, System.Text.Encoding.Default);
            return strCommand;
        }
        private string GetURIValue(string strURI, string strKey)
        {
            int intStart = (";" + strURI).IndexOf(";" + strKey + "=", StringComparison.CurrentCultureIgnoreCase);
            if (intStart == -1) return "";
            intStart += (strKey + "=").Length;
            int intEnd = (strURI + ";").IndexOf(";", intStart);
            return (strURI + ";").Substring(intStart, (intEnd - intStart));
        }

        public WSData(string strURI)
        {
            try
            {
                sURI = strURI;
                sCommand = ParseURI(sURI);
                string strSession = GetURIValue(sURI, "Session");
                if (strSession == "1")
                    objWebserviceConsumer = new WebserviceConsumer(true);
                else
                    objWebserviceConsumer = new WebserviceConsumer();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
        }

        public void Dispose()
        {
            //objWebserviceConsumer = null;
        }

        private void OpenConnection()
        {
        }

        public bool ConnOpen()
        {
            return true ;
        }

        public DataTable GetSchema(string sqlString)
        {
            DataSet ds = new DataSet();
//            OdbcDataAdapter da = new OdbcDataAdapter(command);
            try
            {
//                da.FillSchema(ds, SchemaType.Source);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }

        public IDataReader GetDataReader(string sqlString)
        {
            return null;
        }

        private bool parseWSSQL(string sqlString, ref string sCmd, ref string sRootNode, ref string sXPath, ref string sContentType, ref string sSOAPAction, ref string sXML)
        {
            sCmd = "";
            sRootNode = "";
            sXPath = "\\";
            sXML = "";
            if (sqlString.IndexOf("\r\n") != -1)
            {
                sCmd = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
            }
            else
            {
                sCmd = sqlString;
                sqlString = "";
            }

            if (sqlString.IndexOf("\r\n") != -1)
            {
                sRootNode = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
            }
            else
            {
                sRootNode = sqlString;
                sqlString = "";
            }

            if (sqlString.IndexOf("\r\n") != -1)
            {
                sXPath = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
            }
            else
            {
                sXPath = sqlString;
                sqlString = "";
            }

            if (sqlString.IndexOf("\r\n") != -1)
            {
                sContentType = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
            }
            else
            {
                sContentType = sqlString;
                sqlString = "";
            }

            if (sqlString.IndexOf("\r\n") != -1)
            {
                sSOAPAction = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
            }
            else
            {
                sSOAPAction = sqlString;
                sqlString = "";
            }

            sXML = (sqlString == "\r\n") ? "" : sqlString;
            return true;
        }

        public string GetResponse(string sqlString)
        {
            string sCmd = "";
            string sRootNode = "";
            string sXPath = "";
            string sContentType = "";
            string sSOAPAction = "";
            string sXML = "";
            try
            {
                if (parseWSSQL(sqlString, ref sCmd, ref sRootNode, ref sXPath, ref sContentType, ref sSOAPAction, ref sXML))
                {
                    string strCommand = sCommand;
                    strCommand += "&cmd=" + System.Web.HttpUtility.UrlEncode(sCmd, System.Text.Encoding.Default);
                    strCommand += "&rootnode=" + System.Web.HttpUtility.UrlEncode(sRootNode, System.Text.Encoding.Default);
                    strCommand += "&xpath=" + System.Web.HttpUtility.UrlEncode(sXPath, System.Text.Encoding.Default);
                    strCommand += "&contenttype=" + System.Web.HttpUtility.UrlEncode(sContentType, System.Text.Encoding.Default);
                    strCommand += "&soapaction=" + System.Web.HttpUtility.UrlEncode(sSOAPAction, System.Text.Encoding.Default);
                    strCommand += "&xml=" + System.Web.HttpUtility.UrlEncode(sXML, System.Text.Encoding.Default);
                    string ret = objWebserviceConsumer.GetResponse(strCommand);
                    return ret;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
            return null;
        }
        public DataTable GetDataTable(string sqlString)
        {
            string strResponse = "";
            string strOuterXml = "";
            string strInnerXml = "";

            return GetDataTable (sqlString , ref strResponse, ref strOuterXml, ref strInnerXml);
        }

        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {

            string sCmd = "";
            string sRootNode = "";
            string sXPath = "";
            string sContentType = "";
            string sSOAPAction = "";
            string sXML = "";
            try
            {
                if (parseWSSQL(sqlString, ref sCmd, ref sRootNode, ref sXPath, ref sContentType, ref sSOAPAction, ref sXML))
                {
                    string strCommand = sCommand ;
                    strCommand += "&cmd=" + System.Web.HttpUtility.UrlEncode(sCmd, System.Text.Encoding.Default);
                    strCommand += "&rootnode=" + System.Web.HttpUtility.UrlEncode(sRootNode, System.Text.Encoding.Default);
                    strCommand += "&xpath=" + System.Web.HttpUtility.UrlEncode(sXPath, System.Text.Encoding.Default);
                    strCommand += "&contenttype=" + System.Web.HttpUtility.UrlEncode(sContentType, System.Text.Encoding.Default);
                    strCommand += "&soapaction=" + System.Web.HttpUtility.UrlEncode(sSOAPAction, System.Text.Encoding.Default);
                    strCommand += "&xml=" + System.Web.HttpUtility.UrlEncode(sXML, System.Text.Encoding.Default);
                    DataTable objDataTable = objWebserviceConsumer.GetDataTable(strCommand, ref m_Response, ref m_OuterXml, ref m_InnerXml);
                    return objDataTable;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
            return null;
        }
        public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
        {
            return GetDataTable(sqlString);
        }

        //public int ExecuteNonQuery(string sqlString)
        //{
        //    string strResponse = "";
        //    string strOuterXml = "";
        //    string strInnerXml = "";

        //    return ExecuteNonQuery(sqlString, ref strResponse, ref strOuterXml, ref strInnerXml);
        //}
        //public int ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        //{
        //    try
        //    {
        //        DataTable dt = GetDataTable(sqlString, ref m_Response, ref m_OuterXml, ref m_InnerXml);
        //        int iResult = -1;
        //        try { iResult = System.Convert.ToInt32(dt.Rows [0][0]); }
        //        catch { };
        //        return iResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //    return -1;
        //    //int iResult = 0;
        //    //try { iResult = System.Convert.ToInt32(m_InnerXml); }
        //    //catch { };
        //    //return iResult;
        //}
        //public int ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        //{
        //    return ExecuteNonQuery(sqlString );
        //}
        public string  ExecuteNonQuery(string sqlString)
        {
            string strResponse = "";
            string strOuterXml = "";
            string strInnerXml = "";

            return ExecuteNonQuery(sqlString, ref strResponse, ref strOuterXml, ref strInnerXml);
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {

            try
            {
                DataTable dt = GetDataTable(sqlString, ref m_Response, ref m_OuterXml, ref m_InnerXml);
                try { return dt.Rows[0][0].ToString().TrimEnd(); }
                catch { };
                
                //int iResult = -1;
                //try { iResult = System.Convert.ToInt32(dt.Rows[0][0]); }
                //catch { };
                //return iResult;
            }
            catch (Exception ex)
            {
                throw;
            }
            return ""; // -1;
            //int iResult = 0;
            //try { iResult = System.Convert.ToInt32(m_InnerXml); }
            //catch { };
            //return iResult;
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            return ExecuteNonQuery(sqlString);
        }

        public void BeginTransaction()
        {
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
        }
        public void CommitTransaction()
        {
        }
        public void RollbackTransaction()
        {
        }

    }
}


