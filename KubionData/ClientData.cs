using System.Data;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System;
using KubionLogNamespace;
using System.Collections.Generic;

namespace KubionDataNamespace
{
    public enum ProviderTypes
    {
        WS,
        REST,
        HTTP,
        SQLite,
        SQL,
        OLEDB,
        OLEDBPar,
        ODBC,
        none
    }
    public class ClientData:IDisposable 
    {
        public string sConnectionString = null;
        public int iConnTransaction = -1; //IsolationLevel.ReadUncommitted
        public int iCommandTimeout = 30; //default //max: 43200
        public bool  ThrowErrors = true;

        private ProviderTypes m_ProviderType = ProviderTypes.none  ;
        private IData conn = null;
        public ClientData()
        {
            sConnectionString = GetConnectionString();
        }
        public ClientData(string ConnectionString)
        {
            ConnectionStringExt = ConnectionString;
        }
        public string GetConnectionString()
        {
            string connectionString = ConfigurationManager.AppSettings["KubionDataConnectionString"];
            if (connectionString == null || connectionString == "")
            {
                Exception ex = new Exception("Connection settings [KubionDataConnectionString] is not specified in the application config file");
                KubionLog.WriteLine("*** ERROR" + ex); ;
                if (ThrowErrors) throw (ex);
            }

            return connectionString;
        }

        private IData GetConnection()
        {
            if (conn == null)
            {
                if (sConnectionString.ToUpper().StartsWith("PROVIDER=WEBSERVICE;URI="))
                {
                    m_ProviderType = ProviderTypes.WS;
                    try
                    {
						conn = (IData)new WSData(sConnectionString); //.Substring("PROVIDER=WEBSERVICE;URI=".Length)
                    }
                    catch (Exception ex)
                    {
                        if (conn != null)
                            conn.Dispose();
                        KubionLog.WriteLine("*** ERROR" + ex);
                        if (ThrowErrors) throw;
                    }
                }
                else if (sConnectionString.ToUpper().StartsWith("PROVIDER=HTTP;URI="))
                {
                    m_ProviderType = ProviderTypes.HTTP ;
                    try
                    {
                        conn = (IData)new HTTPData(sConnectionString); //.Substring("PROVIDER=HTTP;URI=".Length)
                    }
                    catch (Exception ex)
                    {
                        if (conn != null)
                            conn.Dispose();
                        KubionLog.WriteLine("*** ERROR" + ex);
                        if (ThrowErrors) throw;
                    }
                }
                else if (sConnectionString.ToUpper().StartsWith("PROVIDER=REST;URI="))
                {
                    m_ProviderType = ProviderTypes.REST;
                    try
                    {
						conn = (IData)new RestData(sConnectionString); //.Substring("PROVIDER=REST;URI=".Length)
                    }
                    catch (Exception ex)
                    {
                        if (conn != null)
                            conn.Dispose();
                        KubionLog.WriteLine("*** ERROR" + ex);
                        if (ThrowErrors) throw;
                    }
                }
                else if (sConnectionString.ToUpper().StartsWith("PROVIDER=SQLITE;"))
                {
                    m_ProviderType = ProviderTypes.SQLite ;
                    try
                    {
                        conn = (IData)new SQLiteData(sConnectionString.Substring("Provider=SQLite;".Length));
                    }
                    catch (Exception ex)
                    {
                        if (conn != null)
                            conn.Dispose();
                        KubionLog.WriteLine("*** ERROR" + ex);
                        if (ThrowErrors) throw;
                    }
                }
                else if (sConnectionString.ToUpper().StartsWith("SQL;"))
                {
                    m_ProviderType = ProviderTypes.SQL;
                    try
                    {
                        conn = (IData)new SQLData(sConnectionString.Substring("SQL;".Length));
                    }
                    catch (Exception ex)
                    {
                        if (conn != null)
                            conn.Dispose();
                        KubionLog.WriteLine("*** ERROR" + ex);
                        if (ThrowErrors) throw;
                    }
                }
                else if (sConnectionString.ToUpper().StartsWith("OLEDB;"))
                {
                    m_ProviderType = ProviderTypes.OLEDB;
                    try
                    {
                        conn = (IData)new OLEDBData(sConnectionString.Substring("OLEDB;".Length));
                    }
                    catch (Exception ex)
                    {
                        if (conn != null)
                            conn.Dispose();
                        KubionLog.WriteLine("*** ERROR" + ex);
                        if (ThrowErrors) throw;
                    }
                }
                else if (sConnectionString.ToUpper().StartsWith("OLEDBPAR;"))
                {
                    m_ProviderType = ProviderTypes.OLEDBPar;
                    try
                    {
                        conn = (IData)new OLEDBParData(sConnectionString.Substring("OLEDBPAR;".Length));
                    }
                    catch (Exception ex)
                    {
                        if (conn != null)
                            conn.Dispose();
                        KubionLog.WriteLine("*** ERROR" + ex);
                        if (ThrowErrors) throw;
                    }
                }
                else if (sConnectionString.ToUpper().StartsWith("ODBC;"))
                {
                    m_ProviderType = ProviderTypes.ODBC;
                    try
                    {
                        conn = (IData)new OdbcData(sConnectionString.Substring("Odbc;".Length));
                    }
                    catch (Exception ex)
                    {
                        if (conn != null)
                            conn.Dispose();
                        KubionLog.WriteLine("*** ERROR" + ex);
                        if (ThrowErrors) throw;
                    }
                }
                else
                    try
                    {
                        conn = (IData)new SQLData(sConnectionString);
                        m_ProviderType = ProviderTypes.SQL;
                    }
                    catch (Exception e1)
                    {
                        try
                        {
                            conn = (IData)new OLEDBData(sConnectionString);
                            m_ProviderType = ProviderTypes.OLEDB ;
                        }
                        catch (Exception e2)
                        {
                            try
                            {
                                conn = (IData)new OdbcData(sConnectionString);
                                m_ProviderType = ProviderTypes.ODBC ;
                            }
                            catch (Exception ex)
                            {
                                if (conn != null)
                                    conn.Dispose();
                                KubionLog.WriteLine("*** ERROR" + ex);
                                if (ThrowErrors) throw;
                            }
                        }
                    }
                    finally
                    {
                        //if (conn != null)
                        //    conn.Dispose();
                    }
            }
            if (conn != null)
                conn.CommandTimeout = iCommandTimeout;
            return conn;

        }
        //private IData GetConnection()
        //{
        //    if (conn == null)
        //    {
        //        if (sConnectionString.StartsWith("Provider=Webservice;"))
        //        {
        //            try
        //            {
        //                conn = (IData)new WSData(sConnectionString.Substring("Provider=Webservice;URI=".Length));
        //            }
        //            catch (Exception ex)
        //            {
        //                if (conn != null)
        //                    conn.Dispose();
        //                KubionLog.WriteLine("*** ERROR" + ex);
        //                if (ThrowErrors) throw;
        //            }
        //        }
        //        else if (sConnectionString.StartsWith("Provider=REST;"))
        //        {
        //            try
        //            {
        //                conn = (IData)new RestData(sConnectionString.Substring("Provider=REST;URI=".Length));
        //            }
        //            catch (Exception ex)
        //            {
        //                if (conn != null)
        //                    conn.Dispose();
        //                KubionLog.WriteLine("*** ERROR" + ex);
        //                if (ThrowErrors) throw;
        //            }
        //        }
        //        else
        //            try
        //            {
        //                conn = (IData)new SQLData(sConnectionString);
        //            }
        //            catch (Exception e1)
        //            {
        //                try
        //                {
        //                    conn = (IData)new OLEDBData(sConnectionString);
        //                }
        //                catch (Exception e2)
        //                {
        //                    try
        //                    {
        //                        conn = (IData)new OdbcData(sConnectionString);
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        if (conn != null)
        //                            conn.Dispose();
        //                        KubionLog.WriteLine("*** ERROR" + ex);
        //                        if (ThrowErrors) throw;
        //                    }
        //                }
        //            }
        //            finally
        //            {
        //                //if (conn != null)
        //                //    conn.Dispose();
        //            }
        //    }
        //    if (conn != null)
        //       conn.CommandTimeout = iCommandTimeout;
        //    return conn;

        //}

        public ProviderTypes ProviderType()
        {
            return m_ProviderType;
        }
        //supports:
        //<conn_string>
        //<conn_string>|<isolation_level>
        //<conn_string>|<isolation_level>|<command_timeout>
        //<conn_string>||<command_timeout>
        public string ConnectionStringExt
        {
            get { return sConnectionString + "|" + iConnTransaction + "|" + iCommandTimeout; }
            set {
                //string[] aConn = value.Split('|');
                string[] aConn = Utils.MySplit (  value,'|');
                if (aConn.Length > 0) sConnectionString = aConn[0];
                if (aConn.Length > 1) if( aConn[1]!="") iConnTransaction = System.Convert.ToInt32(aConn [1]);
                if (aConn.Length > 2) if (aConn[2] != "") iCommandTimeout = System.Convert.ToInt32(aConn[2]);
             }
        }
        public bool ConnOpen()
        {
            if (conn != null)
                return conn.ConnOpen();
            return false;
        }
        public void Dispose()
        {
            if (conn != null)
                conn.Dispose ();
        }

        public string ExecuteNonQuery(string sSQL)
        {
            string sResult = null;
            IData conn = null;
            try
            {
                conn = GetConnection();
                if (iConnTransaction > -1)
                    BeginTransaction((IsolationLevel)iConnTransaction);

                //            sResult = "Rows affected: " + conn.ExecuteNonQuery(sSQL);
                sResult = conn.ExecuteNonQuery(sSQL).ToString();
                
                if (iConnTransaction > -1)
                    CommitTransaction();
            }
            catch (Exception ex)
            {
                if (iConnTransaction > -1)
                    RollbackTransaction();
                sResult = "Error: " + ex.Message;
                if (ThrowErrors) throw;
            }
            return sResult;
        }
        public string ExecuteNonQuery(string sSQL, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            string sResult = null;
            IData conn = null;
            try
            {
                conn = GetConnection();
                if (iConnTransaction > -1)
                    BeginTransaction((IsolationLevel)iConnTransaction);

                //            sResult = "Rows affected: " + conn.ExecuteNonQuery(sSQL);
                sResult = conn.ExecuteNonQuery(sSQL, ref m_Response, ref m_OuterXml, ref m_InnerXml).ToString();

                if (iConnTransaction > -1)
                    CommitTransaction();
            }
            catch (Exception ex)
            {
                if (iConnTransaction > -1)
                    RollbackTransaction();
                sResult = "Error: " + ex.Message;
                if (ThrowErrors) throw;
            }
            return sResult;
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            string sResult = null;
            IData conn = null;
            try
            {
                conn = GetConnection();
                if (iConnTransaction > -1)
                    BeginTransaction((IsolationLevel)iConnTransaction);

                //            sResult = "Rows affected: " + conn.ExecuteNonQuery(sSQL);
                sResult = conn.ExecuteNonQuery(sqlString, arrParams).ToString();
                if (iConnTransaction > -1)
                    CommitTransaction();
            }
            catch (Exception ex)
            {
                if (iConnTransaction > -1)
                    RollbackTransaction();
                sResult = "Error: " + ex.Message;
                if (ThrowErrors) throw;
            }
            return sResult;
        }

        public string ExecuteQuery(string sSQL)
        {
            DataTable dt = null;
            string sResult = "";
            IData conn = null;
            try
            {
                conn = GetConnection();
                if (iConnTransaction > -1)
                    BeginTransaction((IsolationLevel)iConnTransaction);
                dt = conn.GetDataTable(sSQL);
                if (dt != null)
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataColumn c in dt.Columns)
                        {
                            //sResult += c.ColumnName + "=" + SQLData.GetStringResult(dt.Rows[0][c.ColumnName]) + ";";
                            sResult += GetStringResult(dt.Rows[0][c.ColumnName]); //+ "|";
                            if (dt.Columns.Count > 1)
                                sResult += "|";
                        }
                    }
                    else
                    {
                        foreach (DataColumn c in dt.Columns)
                        {
                            //sResult += c.ColumnName + "=" + "" + ";";
                            if (dt.Columns.Count > 1)
                                sResult += "|";
                        }
                    }
                if (iConnTransaction > -1)
                    CommitTransaction();
            }
            catch (Exception ex)
            {
                if (iConnTransaction > -1)
                    RollbackTransaction();
                sResult = "Error: " + ex.Message;
                if (ThrowErrors) throw;
            }
            return sResult;
        }

        public IDataReader GetDataReader(string sSQL)
        {
            IDataReader dr = null;
            IData conn = null;
            try
            {
                conn = GetConnection();
                dr = conn.GetDataReader(sSQL);
            }
            catch (Exception ex)
            {
                if (ThrowErrors) throw;
            }
            return dr;

        }

        public string GetResponse(string sSQL)
        {
            string ret = null;
            IData conn = null;
            try
            {
                conn = GetConnection();
                if (iConnTransaction > -1)
                    BeginTransaction((IsolationLevel)iConnTransaction);
                ret = conn.GetResponse (sSQL);
                if (iConnTransaction > -1)
                    CommitTransaction();
            }
            catch (Exception ex)
            {
                if (iConnTransaction > -1)
                    RollbackTransaction();
                if (ThrowErrors) throw;
            }
            return ret;
        }
        public DataTable DTExecuteQuery(string sSQL)
        {
            DataTable dt = null;
            IData conn = null;
            try
            {
                conn = GetConnection();
                if (iConnTransaction > -1)
                    BeginTransaction((IsolationLevel)iConnTransaction);
                dt = conn.GetDataTable(sSQL);
                if (iConnTransaction > -1)
                    CommitTransaction();
            }
            catch (Exception ex)
            {
                if (iConnTransaction > -1)
                    RollbackTransaction();
                if (ThrowErrors) throw;
            }
            return dt;
        }
        public DataTable DTExecuteQuery(string sSQL, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            DataTable dt = null;
            IData conn = null;
            try
            {
                conn = GetConnection();
                if (iConnTransaction > -1)
                    BeginTransaction((IsolationLevel)iConnTransaction);
                dt = conn.GetDataTable(sSQL, ref m_Response, ref m_OuterXml, ref m_InnerXml);
                if (iConnTransaction > -1)
                    CommitTransaction();
            }
            catch (Exception ex)
            {
                if (iConnTransaction > -1)
                    RollbackTransaction();
                if (ThrowErrors) throw;
            }
            return dt;
        }
        public DataTable DTExecuteQuery(string sqlString, IDataParameter[] arrParams)
        {
            DataTable dt = null;
            IData conn = null;
            try
            {
                conn = GetConnection();
                if (iConnTransaction > -1)
                    BeginTransaction((IsolationLevel)iConnTransaction);
                dt = conn.GetDataTable(sqlString,arrParams );
                if (iConnTransaction > -1)
                    CommitTransaction();
            }
            catch (Exception ex)
            {
                if (iConnTransaction > -1)
                    RollbackTransaction();
                if (ThrowErrors) throw;
            }
            return dt;
        }

        public DataTable GetSchema(string sSQL)
        {
            DataTable dt = null;
            IData conn = null;
            try
            {
                conn = GetConnection();
                dt = conn.GetSchema(sSQL);
            }
            catch (Exception ex)
            {
                if (iConnTransaction > -1)
                    RollbackTransaction();
                if (ThrowErrors) throw;
            }
            return dt;
        }

        public void BeginTransaction()
        {
            IData conn = null;
            try
            {
                conn = GetConnection();
                conn.BeginTransaction();
            }
            catch (Exception ex)
            {
                iConnTransaction = -1;
                if (ThrowErrors) throw;
            }
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            IData conn = null;
            try
            {
                conn = GetConnection();
                conn.BeginTransaction(isolationLevel);
            }
            catch (Exception ex)
            {
                iConnTransaction = -1;
                if (ThrowErrors) throw;
            }
        }
        public void CommitTransaction()
        {
            IData conn = null;
            try
            {
                conn = GetConnection();
                conn.CommitTransaction();
            }
            catch (Exception ex)
            {
                if (ThrowErrors) throw;
            }
        }
        public void RollbackTransaction()
        {
            IData conn = null;
            try
            {
                conn = GetConnection();
                conn.RollbackTransaction();
            }
            catch (Exception ex)
            {
                if (ThrowErrors) throw;
            }
        }

        #region GetResult

        public static bool GetBoolResult(object obj)
        {
            return GetBoolResult(obj, false);
        }
        public static bool GetBoolResult(object obj, bool defaultValue)
        {
            int i;
            //if (obj != null && obj is bool)
            //    return (bool)obj;
            if (obj != null && bool.TryParse(obj.ToString(), out  defaultValue))
                return defaultValue;
            if (obj != null && Int32.TryParse(obj.ToString(), out  i))
                return (i != 0);

            return defaultValue;
        }

        public static string GetStringResult(object obj, string defaultValue)
        {
            if (obj != null)
                if (obj is string)
                    return ((string)obj).TrimEnd();
                else
                    return obj.ToString();
            return defaultValue;
        }
        public static string GetStringResult(object obj)
        {
            return GetStringResult(obj, "");
        }

        public static int GetInt32Result(object obj, int defaultValue)
        {
            //            if (obj != null && obj is int)
            //            return (int)obj;
            if (obj != null && Int32.TryParse(obj.ToString(), out  defaultValue))
                return defaultValue;

            return defaultValue;
        }

        public static int GetInt32Result(object obj)
        {
            return GetInt32Result(obj, -1);
        }

        public static double GetDoubleResult(object obj, double defaultValue)
        {
            if (obj != null && obj is double)
                return (double)obj;
            return defaultValue;
        }

        public static double GetDoubleResult(object obj)
        {
            return GetDoubleResult(obj, -1);
        }

        public static DateTime GetDateTimeResult(object obj, DateTime defaultValue)
        {
            if (obj != null && obj is DateTime)
                return (DateTime)obj;
            return defaultValue;
        }
        public static DateTime GetDateTimeResult(object obj)
        {
            return GetDateTimeResult(obj, (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
        }

        public static byte[] GetBinaryResult(object obj)
        {
            if (obj != null && obj != DBNull.Value && obj is byte[])
                return (byte[])obj;
            return null;
        }

        #endregion GetResult

    }

}