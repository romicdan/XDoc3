#region using
using System;
using System.Data;
using System.Collections;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Web;
#endregion

namespace XDataSourceModule
{
    public class XDataSource : IXDataSource
    {
        #region Protected Members
        protected int m_templateID = -1;
        protected Hashtable m_SourceResults = null;
        protected XProviderData m_dataProvider = null;
        protected Hashtable m_ConfigVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_ContextSessionVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_ContextSessionVariablesString = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected string sSessionID = "";
        protected Hashtable m_TemplateText = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_TemplateIDForInclude = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Connectors m_Connectors = null;
        #endregion

        public XProviderData DataProvider
        {
            get { return m_dataProvider; }
            set { m_dataProvider = value; }
        }
        protected XDocCache m_xdocCache = null;
        public XDocCache DocCache
        {
            get { return m_xdocCache; }
            set { m_xdocCache = value; //m_xdocCache.DataProvider = m_dataProvider; 
            }
        }
        protected XDocCacheTemplates m_xdocCacheTemplates = null;
        public XDocCacheTemplates DocCacheTemplates
        {
            get { return m_xdocCacheTemplates; }
            set
            {
                m_xdocCacheTemplates = value; //m_xdocCache.DataProvider = m_dataProvider; 
            }
        }

        public event SourceResult.RequestHierarcyEventHandler RequestHierarchy;

        public XDataSource()
        {
        }


        #region IXDataSource methods

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        void IXDataSource.InitializeEvaluator(int templateID, XProviderData dataProvider, Hashtable templateParams)
        {
            m_templateID = templateID;
            m_Connectors = new Connectors();
            m_SourceResults = CollectionsUtil.CreateCaseInsensitiveHashtable();
            m_dataProvider = dataProvider;
            if (m_xdocCache == null)
            {
                m_xdocCache = new XDocCache();
            }
            if (m_xdocCacheTemplates == null)
            {
                m_xdocCacheTemplates = new XDocCacheTemplates();
            }

            if (templateParams == null)
                return;
            if (templateParams["SESSIONID"] != null) sSessionID = (string)templateParams["SESSIONID"];
            m_xdocCache.SessionID = sSessionID;
            IDictionaryEnumerator en = templateParams.GetEnumerator();
            while (en.MoveNext())
            {
                m_SourceResults.Add(en.Key.ToString().ToUpper(), en.Value);
            }
        }

        string IXDataSource.GetResponse(XProviderData dataProvider, string strSQL, string connName)
        {
            string ret = "";
            try
            {
                SourceResult result = new SourceResult();
                ret = result.GetResponse(dataProvider, strSQL, connName);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                throw;
            }
            return ret;
        }

        SourceResult IXDataSource.EvaluateSourceResult(int templateID, string sourceName, Array arrParams, out string sError, out string sErrorVerbose, out string sParameters, out double dDuration , out string key)
        {
            sError = "";
            sErrorVerbose = "";
            sParameters = "";
            dDuration  = 0;
            try
            {

                key = SourceResult.GetSourceKey(sourceName, arrParams);
                SourceResult result = (SourceResult)m_SourceResults[key];
                if (result == null)
                {
                    DateTime dt1 = DateTime.Now ;
                    //result = new SourceResult();
                    result = new SourceResult(m_Connectors );
                    result.RequestHierarchy += new SourceResult.RequestHierarcyEventHandler(result_RequestHierarchy);
                    if (!result.Initialize(templateID, m_dataProvider, sourceName, arrParams, out sError, out sErrorVerbose, out sParameters))
                    {
                        m_SourceResults[key] = result;
                        //return result;
                    }
                    m_SourceResults[key] = result;
                    DateTime dt2 = DateTime.Now;
                    TimeSpan span = dt2 - dt1;
                    dDuration  = (double)span.TotalMilliseconds;
                }
                else
                {
                    sError = result.Error;
                    sErrorVerbose = result.ErrorVerbose;
                    sParameters = result.Parameters;
                    dDuration  = -1;
                }
                return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                throw;
            }
        }
        int IXDataSource.EvaluateSource(int templateID, string sourceName, Array arrParams, out string sError, out string sErrorVerbose, out string sParameters, out double dDuration, out string key)
        {
            sError = "";
            sErrorVerbose = "";
            sParameters = "";
            dDuration = 0;
            try
            {
                key = SourceResult.GetSourceKey(sourceName, arrParams);
                SourceResult result = (SourceResult)m_SourceResults[key];
                if (result == null)
                {
                    //result = new SourceResult();
                    result = new SourceResult(m_Connectors);
                    result.RequestHierarchy += new SourceResult.RequestHierarcyEventHandler(result_RequestHierarchy);
                    if (!result.Initialize(templateID, m_dataProvider, sourceName, arrParams, out sError, out sErrorVerbose, out sParameters))
                    {
                        m_SourceResults[key] = result;

                        return -1;
                    }
                    m_SourceResults[key] = result;
                }
                else
                {
                    sError = result.Error;
                    sErrorVerbose = result.ErrorVerbose;
                    sParameters = result.Parameters;
                    dDuration = -1;
                }

                return result.GetCount();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);

                throw;
            }
        }

        void result_RequestHierarchy(SourceResult sourceResult, string parameters)
        {
            if (RequestHierarchy != null)
                RequestHierarchy(sourceResult, parameters);
        }

        void IXDataSource.GetValue(string fieldSource, out object Value)
        {
            Value = null;
            try
            {
                Value = m_SourceResults[fieldSource.ToUpper()];
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                throw;
            }


        }

        bool IXDataSource.GetValue(string fieldSource, string source, Array sourceContext, int Index, out object Value, out string sError)
        {
            sError = "";
            string sErrorVerbose = "";
            string sParameters = "";
            Value = null;
            try
            {
                string key = SourceResult.GetSourceKey(source, sourceContext);
                SourceResult result = (SourceResult)m_SourceResults[key];
                if (result == null)
                {//3apr
                    //result = new SourceResult();
                    result = new SourceResult(m_Connectors);
                    if (!result.Initialize(m_templateID, m_dataProvider, source, sourceContext, out sError, out sErrorVerbose, out sParameters))
                    {
                        return false;
                    }
                }

                return result.GetFieldValue(fieldSource, Index, out Value);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                throw;
            }

        }

        string IXDataSource.GetCfg(string sVarName, string sDefault)
        {
            return m_xdocCache.GetCfg(sVarName, sDefault, m_dataProvider); 
            //string sVal, sVar;
            //if (m_ConfigVariables.Count == 0)
            //{
            //    string sSQL = "SELECT value,ConfigClass,ConfigName FROM [S_ConfigVariables] ";
            //    DataTable dt = m_dataProvider.GetDataTable(sSQL);
            //    if (dt != null && dt.Rows.Count > 0)
            //    {
            //        foreach (DataRow row in dt.Rows)
            //        {
            //            sVal = Connectors.GetStringResult(row[0], "");
            //            sVar = Connectors.GetStringResult(row["ConfigClass"], "") + "__" + row["ConfigName"];
            //            m_ConfigVariables[sVar] = sVal;
            //        }
            //    }
            //}

            //sVal = sDefault;
            //if (!sVarName.Contains("__")) sVarName = "__" + sVarName;
            //if (m_ConfigVariables.ContainsKey(sVarName))
            //{
            //    sVal = m_ConfigVariables[sVarName].ToString();
            //}
            //return sVal;
        }
        string IXDataSource.GetIncludeOnce()
        {
            return m_xdocCache.GetIncludeOnce();
        }

        bool IXDataSource.RequestIncludeOnce(string sTemplateName)
        {
            return m_xdocCache.RequestIncludeOnce(sTemplateName);
        }
        void IXDataSource.ClearIncludeOnce()
        {
            m_xdocCache.ClearIncludeOnce();
        }
        void IXDataSource.ClearCache()
        {
            m_xdocCache.ClearCache();
        }
        void IXDataSource.ClearTemplatesCache()
        {
            m_xdocCacheTemplates.ClearTemplatesCache();
        }


        string IXDataSource.GetSV(string sVarName, string sContextName, string sDefault)
        {
            if (sVarName.ToUpper () == "INCLUDEONCE")
                return m_xdocCache.GetIncludeOnce();

            return m_xdocCache.GetSV(sVarName, sContextName, sDefault); 
            //LoadSVContext(sContextName);
            //Hashtable m_SessionVariables = (Hashtable)m_ContextSessionVariables[sContextName];

            //string sVal = sDefault;
            //if (m_SessionVariables.ContainsKey(sVarName))
            //    sVal = m_SessionVariables[sVarName].ToString();
            //if (sVal == "") sVal = sDefault;
            //sVal = ReplaceSettings(sVal, sContextName);
            //return sVal;
        }
        void IXDataSource.SetSV(string sVarName, string sContextName, string sVal)
        {
            m_xdocCache.SetSV(sVarName, sContextName, sVal);
            //LoadSVContext(sContextName);
            //Hashtable m_SessionVariables = (Hashtable)m_ContextSessionVariables[sContextName];
            //string s_SessionVariables = (string)m_ContextSessionVariablesString[sContextName];
            //if (m_SessionVariables.Contains(sVarName))
            //    s_SessionVariables = s_SessionVariables.Replace("|" + sVarName + "|", "|");
            //m_SessionVariables[sVarName] = sVal;
            //s_SessionVariables += sVarName + "|";
            //m_ContextSessionVariables[sContextName] = m_SessionVariables;
            //m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
        }
        void IXDataSource.DelSV(string sVarName, string sContextName, bool delfrom)
        {
            m_xdocCache.DelSV(sVarName, sContextName, delfrom);
            //LoadSVContext(sContextName);
            //if (sVarName == "")
            //{
            //    m_ContextSessionVariables[sContextName] = CollectionsUtil.CreateCaseInsensitiveHashtable();
            //    m_ContextSessionVariablesString[sContextName] = "|";
            //}
            //else
            //{
            //    Hashtable m_SessionVariables = (Hashtable)m_ContextSessionVariables[sContextName];
            //    string s_SessionVariables = (string)m_ContextSessionVariablesString[sContextName];
            //    if (m_SessionVariables.Contains(sVarName))
            //    {
            //        if (delfrom)
            //        {
            //            string[] a_SessionVariables = s_SessionVariables.Substring(s_SessionVariables.IndexOf("|" + sVarName + "|")).Split('|');
            //            foreach (string isVarName in a_SessionVariables)
            //            {
            //                if (isVarName != "")
            //                {
            //                    m_SessionVariables.Remove(isVarName);
            //                    s_SessionVariables = s_SessionVariables.Replace("|" + isVarName + "|", "|");
            //                }
            //            }
            //        }
            //        else
            //        {
            //            m_SessionVariables.Remove(sVarName);
            //            s_SessionVariables = s_SessionVariables.Replace("|" + sVarName + "|", "|");
            //        }
            //        m_ContextSessionVariables[sContextName] = m_SessionVariables;
            //        m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
            //    }
            //}
        }

        void IXDataSource.CommitSV()
        {
            m_xdocCache.CommitSV();
            //string sSQL = "";
            //foreach (DictionaryEntry de in m_ContextSessionVariables)
            //    sSQL += SaveSVContext(de.Key.ToString());
            //if (sSQL != "") m_dataProvider.ExecuteNonQuery(sSQL);
            //m_ContextSessionVariables.Clear();
        }
        //void LoadSVContext(string sContextName)
        //{
        //    if (!m_ContextSessionVariables.Contains(sContextName))
        //    {
        //        Hashtable m_SessionVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
        //        string s_SessionVariables = "|";
        //        string sSQL = "SELECT parameter,value FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "' order by ord";
        //        DataTable dt = m_dataProvider.GetDataTable(sSQL);
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            foreach (DataRow row in dt.Rows)
        //            {
        //                string sVar = Connectors.GetStringResult(row[0], "");
        //                string sVal = Connectors.GetStringResult(row[1], "");
        //                m_SessionVariables.Add(sVar, sVal);
        //                s_SessionVariables += sVar + "|";
        //            }
        //        }
        //        m_ContextSessionVariables[sContextName] = m_SessionVariables;
        //        m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
        //    }
        //}
        //string SaveSVContext(string sContextName)
        //{
        //    string sSQL = "DELETE FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "';";
        //    Hashtable m_SessionVariables = (Hashtable)m_ContextSessionVariables[sContextName];
        //    string s_SessionVariables = (string)m_ContextSessionVariablesString[sContextName];
        //    string[] a_SessionVariables = s_SessionVariables.Split('|');

        //    int i = 0;
        //    foreach (string sVarName in a_SessionVariables)
        //    {
        //        if (sVarName != "")
        //        {
        //            string sVarVal = m_SessionVariables[sVarName].ToString().Replace("'", "''");
        //            if (sVarVal != "")
        //                sSQL += "INSERT INTO [S_SessionVariables] ([SessionID],[InstanceID],[Parameter],[Value],[ord]) VALUES ('" + sSessionID.Replace("'", "''") + "','" + sContextName.Replace("'", "''") + "','" + sVarName + "','" + sVarVal + "'," + i + ");";
        //            i++;
        //        }
        //    }

        //    return sSQL;
        //}
        Object IXDataSource.RequestTemplateTree(int templateID,ref string sTemplateText)
        {
            return m_xdocCacheTemplates.RequestTemplateTree(templateID, ref sTemplateText );
        }
        void IXDataSource.AddTemplateTree(int templateID, Object templateRoot, string sTemplateText)
        {
            m_xdocCacheTemplates.AddTemplateTree(templateID, templateRoot,  sTemplateText);
        }

        string IXDataSource.RequestTemplateText(int templateID)
        {
            return m_xdocCacheTemplates.RequestTemplateText(templateID);
            //string sTemplateText = "notloaded";
            //if (m_TemplateText.ContainsKey(templateID))
            //    sTemplateText = (string)m_TemplateText[templateID];
            //return sTemplateText;
        }
        void IXDataSource.AddTemplateText(int templateID, string sTemplateText)
        {
            m_xdocCacheTemplates.AddTemplateText(templateID, sTemplateText);
            //m_TemplateText[templateID] = sTemplateText;
        }
        int IXDataSource.RequestTemplateIDForInclude(string templateName)
        {
            return m_xdocCacheTemplates.RequestTemplateIDForInclude(templateName);
            //int iTemplateID = -1;
            //if (m_TemplateIDForInclude.ContainsKey(templateName))
            //    iTemplateID = (int)m_TemplateIDForInclude[templateName];
            //return iTemplateID;

        }
        void IXDataSource.AddTemplateIDForInclude(string templateName, int iTemplateID)
        {
            m_xdocCacheTemplates.AddTemplateIDForInclude(templateName, iTemplateID);
            //m_TemplateIDForInclude[templateName] = iTemplateID;
        }

        private string sParserQueryLog = "";
        string IXDataSource.ParserQueryLog
        {
            get
            {
                return sParserQueryLog;
            }
            set
            {
                sParserQueryLog = value;
            }
        }
        private string sParserTrace = "";
        string IXDataSource.ParserTrace
        {
            get
            {
                //if (sParserTrace == "")
                //{
                //    if (GetParameterValue("ParserTrace", "0") == "1") sParserTrace = "1";
                //    //else if (GetSV("ParserTrace", "", "", "0") == "1") sParserTrace = "1";
                //    //else if (GetParameterValue("User", "") == GetSV("UserTrace", "", "", "nobody")) sParserTrace = "1";
                //    else
                //    {

                //        string strParserTrace = System.Configuration.ConfigurationManager.AppSettings["ParserTrace"];
                //        if (strParserTrace != null)
                //            sParserTrace = strParserTrace;
                //        else sParserTrace = "0";
                //    }
                //}
                return sParserTrace;
            }
            set
            {
                sParserTrace = value;
            }
        }

        #endregion

    }

    /// <summary>
	/// 
	/// </summary>
    //public class XDataSource : IXDataSource
    //{
    //    #region Protected Members
    //    protected int m_templateID = -1;
    //    protected Hashtable m_SourceResults = null;
    //    protected XProviderData m_dataProvider = null;
    //    protected Hashtable m_ConfigVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
    //    protected Hashtable m_ContextSessionVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
    //    protected Hashtable m_ContextSessionVariablesString = CollectionsUtil.CreateCaseInsensitiveHashtable();
    //    protected string sSessionID = "";
    //    protected Hashtable m_TemplateText = CollectionsUtil.CreateCaseInsensitiveHashtable();
    //    protected Hashtable m_TemplateIDForInclude = CollectionsUtil.CreateCaseInsensitiveHashtable();
    //    #endregion

    //    public XProviderData DataProvider
    //    {
    //        get { return m_dataProvider; }
    //        set { m_dataProvider = value; }
    //    }

    //    public event SourceResult.RequestHierarcyEventHandler RequestHierarchy;

    //    public XDataSource()
    //    {
    //    }


    //    #region IXDataSource methods

    //    void IXDataSource.InitializeEvaluator(int templateID, XProviderData dataProvider, Hashtable templateParams)
    //    {
    //        m_templateID = templateID;
    //        m_SourceResults = CollectionsUtil.CreateCaseInsensitiveHashtable();
    //        m_dataProvider = dataProvider;

    //        if(templateParams == null)
    //            return;
    //        if (templateParams["SESSIONID"] != null) sSessionID = (string)templateParams["SESSIONID"];

    //        IDictionaryEnumerator en = templateParams.GetEnumerator();
    //        while(en.MoveNext())
    //        {
    //            m_SourceResults.Add(en.Key.ToString().ToUpper(), en.Value);
    //        }
    //    }
		
    //    int IXDataSource.EvaluateSource(int templateID, string sourceName, Array arrParams, out string error)
    //    {
    //        error = "";
    //        try
    //        {
    //            string key = SourceResult.GetSourceKey(sourceName, arrParams);
    //            SourceResult result = (SourceResult)m_SourceResults[key];
    //            if(result == null)
    //            {
    //                result = new SourceResult();
    //                result.RequestHierarchy += new SourceResult.RequestHierarcyEventHandler(result_RequestHierarchy);
    //                if(!result.Initialize(templateID, m_dataProvider, sourceName, arrParams, out error))
    //                    return -1;

    //                m_SourceResults[key] = result;
    //            }

    //            return result.GetCount();
    //        }
    //        catch(Exception ex)
    //        {
    //            Trace.WriteLine(ex);
    //            throw(ex);
    //        }
    //    }

    //    void result_RequestHierarchy(SourceResult sourceResult, string parameters)
    //    {
    //        if (RequestHierarchy != null)
    //            RequestHierarchy(sourceResult, parameters);
    //    }

    //    void IXDataSource.GetValue(string fieldSource, out object Value)
    //    {
    //        Value = null;
    //        try
    //        {
    //            Value = m_SourceResults[fieldSource.ToUpper()];
    //        }
    //        catch (Exception ex)
    //        {
    //            Trace.WriteLine(ex);
    //            throw;
    //        }
            

    //    }

    //    bool IXDataSource.GetValue(string fieldSource, string source, Array sourceContext, int Index, out object Value, out string error)
    //    {
    //        error = "";
    //        Value = null;
    //        try
    //        {
    //            string key = SourceResult.GetSourceKey(source, sourceContext);
    //            SourceResult result = (SourceResult)m_SourceResults[key];
    //            if(result == null)
    //            {
    //                result = new SourceResult();
    //                if(!result.Initialize(m_templateID, m_dataProvider, source, sourceContext, out  error))
    //                {
    //                    return false;
    //                }
    //            }
				
    //            return result.GetFieldValue(fieldSource, Index, out Value);
    //        }
    //        catch(Exception ex)
    //        {
    //            Trace.WriteLine(ex);
    //            throw(ex);
    //        }

    //    }

    //    string IXDataSource.GetCfg(string sVarName, string sDefault)
    //    {
    //        string sVal, sVar;
    //        if (m_ConfigVariables.Count == 0)
    //        {
    //            string sSQL = "SELECT value,ConfigClass,ConfigName FROM [S_ConfigVariables] ";
    //            DataTable dt = m_dataProvider.GetDataTable(sSQL);
    //            if (dt != null && dt.Rows.Count > 0)
    //            {
    //                foreach (DataRow row in dt.Rows)
    //                {
    //                    sVal = Connectors.GetStringResult(row[0], "");
    //                    sVar = Connectors.GetStringResult(row["ConfigClass"], "") + "__" + row["ConfigName"];
    //                    m_ConfigVariables[sVar] = sVal;
    //                }
    //            }
    //        }

    //        sVal = sDefault;
    //        if (!sVarName.Contains("__")) sVarName = "__" + sVarName;
    //        if (m_ConfigVariables.ContainsKey(sVarName))
    //        {
    //            sVal = m_ConfigVariables[sVarName].ToString();
    //        }
    //        return sVal;
    //    }

    //    int iInReplaceSettings = 0;
    //    private string ReplaceSettings(string sLine, string sContextName)
    //    {
    //        if (iInReplaceSettings > 10) return sLine;
    //        iInReplaceSettings++;
    //        char cSep = '@';
    //        string sResponse = "";
    //        string[] aLine = sLine.Split(cSep);
    //        for (int i = 0; i < aLine.GetLength(0); i++)
    //        {
    //            sResponse += aLine[i];
    //            i++;
    //            if (i < aLine.GetLength(0))
    //            {
    //                string sVar = aLine[i];
    //                bool toEncode = false;
    //                if (sVar.ToLower().EndsWith("[encode]"))
    //                {
    //                    sVar = sVar.Substring(0, sVar.Length - "[encode]".Length);
    //                    toEncode = true;
    //                }
    //                string sVal = ((IXDataSource) this).GetSV(sVar, sContextName, "missingvalue");
    //                if (sVal == "missingvalue") sVal = ((IXDataSource)this).GetCfg(sVar, "missingvalue");
    //                if (sVal != "missingvalue")
    //                {
    //                    if (toEncode) sVal = HttpUtility.HtmlEncode(HttpUtility.UrlEncode(sVal));
    //                    sResponse += sVal;
    //                }
    //                else
    //                {
    //                    sResponse += "@";
    //                    i--;
    //                }
    //            }
    //        }
    //        iInReplaceSettings--;
    //        //if (aLine.GetLength(0) > 1) sResponse = ReplaceSettings(sResponse, sContextName);
    //        return sResponse.Trim();
    //    }

    //    string IXDataSource.GetSV(string sVarName, string sContextName, string sDefault)
    //    {
    //        LoadSVContext(sContextName);
    //        Hashtable m_SessionVariables =(Hashtable ) m_ContextSessionVariables[sContextName];

    //        string sVal = sDefault;
    //        if (m_SessionVariables.ContainsKey(sVarName))
    //            sVal = m_SessionVariables[sVarName].ToString();
    //        if (sVal == "") sVal = sDefault;
    //        sVal = ReplaceSettings(sVal, sContextName);
    //        return sVal;
    //    }
    //    void IXDataSource.SetSV(string sVarName, string sContextName, string sVal)
    //    {
    //        LoadSVContext(sContextName);
    //        Hashtable m_SessionVariables = (Hashtable) m_ContextSessionVariables[sContextName];
    //        string s_SessionVariables = (string)m_ContextSessionVariablesString[sContextName];
    //        if (m_SessionVariables.Contains(sVarName))
    //            s_SessionVariables = s_SessionVariables.Replace("|" + sVarName + "|", "|");
    //        m_SessionVariables[sVarName] = sVal;
    //        s_SessionVariables += sVarName + "|";
    //        m_ContextSessionVariables[sContextName] = m_SessionVariables;
    //        m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
    //    }
    //    void IXDataSource.DelSV(string sVarName, string sContextName, bool delfrom)
    //    {
    //        LoadSVContext(sContextName);
    //        if (sVarName == "")
    //        {
    //            m_ContextSessionVariables[sContextName] = CollectionsUtil.CreateCaseInsensitiveHashtable();
    //            m_ContextSessionVariablesString[sContextName] = "|";
    //        }
    //        else
    //        {
    //            Hashtable m_SessionVariables = (Hashtable ) m_ContextSessionVariables[sContextName];
    //            string s_SessionVariables = (string) m_ContextSessionVariablesString[sContextName];
    //            if (m_SessionVariables.Contains(sVarName))
    //            {
    //                if (delfrom)
    //                {
    //                    string[] a_SessionVariables = s_SessionVariables.Substring(s_SessionVariables.IndexOf("|" + sVarName + "|")).Split('|');
    //                    foreach (string isVarName in a_SessionVariables)
    //                    {
    //                        if (isVarName != "")
    //                        {
    //                            m_SessionVariables.Remove(isVarName);
    //                            s_SessionVariables = s_SessionVariables.Replace("|" + isVarName + "|", "|");
    //                        }
    //                    }
    //                }
    //                else
    //                {
    //                    m_SessionVariables.Remove(sVarName);
    //                    s_SessionVariables = s_SessionVariables.Replace("|" + sVarName + "|", "|");
    //                }
    //                m_ContextSessionVariables[sContextName] = m_SessionVariables;
    //                m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
    //            }
    //        }
    //    }

    //    void IXDataSource.CommitSV()
    //    {
    //        string sSQL = "";
    //        foreach (DictionaryEntry de in m_ContextSessionVariables)
    //            sSQL+=SaveSVContext(de.Key .ToString());
    //        if (sSQL !="") m_dataProvider.ExecuteNonQuery(sSQL);
    //        m_ContextSessionVariables.Clear ();
            

    //    }
    //    void LoadSVContext(string sContextName)
    //    {
    //        if (!m_ContextSessionVariables.Contains(sContextName))
    //        {
    //            Hashtable m_SessionVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
    //            string s_SessionVariables = "|";
    //            string sSQL = "SELECT parameter,value FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "' order by ord";
    //            DataTable dt = m_dataProvider.GetDataTable(sSQL);
    //            if (dt != null && dt.Rows.Count > 0)
    //            {
    //                foreach (DataRow row in dt.Rows)
    //                {
    //                    string sVar = Connectors.GetStringResult(row[0], "");
    //                    string sVal = Connectors.GetStringResult(row[1], "");
    //                    m_SessionVariables.Add(sVar, sVal);
    //                    s_SessionVariables += sVar + "|";
    //                }
    //            }
    //            m_ContextSessionVariables[sContextName] = m_SessionVariables;
    //            m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
    //        }
    //    }
    //    string  SaveSVContext(string sContextName)
    //    {
    //        string sSQL = "DELETE FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "';";
    //        Hashtable m_SessionVariables = (Hashtable)m_ContextSessionVariables[sContextName];
    //        string s_SessionVariables = (string)m_ContextSessionVariablesString[sContextName];
    //        string[] a_SessionVariables = s_SessionVariables.Split('|');
 
    //        int i = 0;
    //        foreach (string sVarName  in a_SessionVariables) 
    //        {
    //            if (sVarName != "")
    //            {
    //                string sVarVal = m_SessionVariables[sVarName].ToString ().Replace("'", "''");
    //                if(sVarVal !="")
    //                    sSQL += "INSERT INTO [S_SessionVariables] ([SessionID],[InstanceID],[Parameter],[Value],[ord]) VALUES ('" + sSessionID.Replace("'", "''") + "','" + sContextName.Replace("'", "''") + "','" + sVarName + "','" + sVarVal + "'," + i + ");";
    //                i++;
    //            }
    //        }

    //        return sSQL;
    //    }

    //    string IXDataSource.RequestTemplateText(int templateID)
    //    {
    //        string sTemplateText = "notloaded";
    //        if (m_TemplateText.ContainsKey (templateID ))
    //            sTemplateText = (string ) m_TemplateText[templateID ];
    //        return sTemplateText ;
    //    }
    //    void IXDataSource.AddTemplateText(int templateID, string sTemplateText)
    //    {
    //        m_TemplateText[templateID] = sTemplateText;
    //    }
    //    int IXDataSource.RequestTemplateIDForInclude(string templateName)
    //    {
    //        int iTemplateID = -1;
    //        if (m_TemplateIDForInclude.ContainsKey(templateName))
    //            iTemplateID = (int) m_TemplateIDForInclude[templateName];
    //        return iTemplateID;

    //    }
    //    void IXDataSource.AddTemplateIDForInclude(string templateName, int iTemplateID)
    //    {
    //        m_TemplateIDForInclude[templateName] = iTemplateID;
    //    }

    //    private string sParserTrace = "";
    //    string IXDataSource.ParserTrace
    //    {
    //        get
    //        {
    //            //if (sParserTrace == "")
    //            //{
    //            //    if (GetParameterValue("ParserTrace", "0") == "1") sParserTrace = "1";
    //            //    //else if (GetSV("ParserTrace", "", "", "0") == "1") sParserTrace = "1";
    //            //    //else if (GetParameterValue("User", "") == GetSV("UserTrace", "", "", "nobody")) sParserTrace = "1";
    //            //    else
    //            //    {

    //            //        string strParserTrace = System.Configuration.ConfigurationManager.AppSettings["ParserTrace"];
    //            //        if (strParserTrace != null)
    //            //            sParserTrace = strParserTrace;
    //            //        else sParserTrace = "0";
    //            //    }
    //            //}
    //            return sParserTrace;
    //        }
    //        set
    //        {
    //            sParserTrace = value;
    //        }
    //    }

    //    #endregion

    //}
}
