
using System;

namespace XDataSourceModule
{
    internal class S_QUERIES
    {
        public static string
            SYSTABLENAME = "S_QUERIES";
        public static string
            QUERYID = "QueryID",
            TEMPLATEID = "TemplateID",
            QUERYNAME = "QueryName",
            CONNID = "ConnID",
            QUERYTEXT = "QueryText";
    }

    internal class S_CONNECTIONS
    {
        public static string
            SYSTABLENAME = "S_CONNECTIONS";
        public static string
            CONNID = "ConnID",
            CONNNAME = "ConnName",
            CONNSTRING = "ConnString";
    }

	internal class SQL
	{
        public static string
            SQLGetQueryStatement = "SELECT S_QUERIES.QUERYTEXT, S_CONNECTIONS.ConnString AS CONNSTRING FROM S_QUERIES LEFT OUTER JOIN S_CONNECTIONS ON (S_QUERIES.CONNID = S_CONNECTIONS.CONNID) WHERE QUERYNAME = @QUERYNAME  AND TEMPLATEID = @TEMPLATEID",
            SQLGetTemplateID = "SELECT TEMPLATEID FROM S_TEMPLATES WHERE TEMPLATENAME = @TEMPLATENAME",
            SQLConnectionString = "SELECT S_CONNECTIONS.ConnString AS CONNSTRING FROM S_CONNECTIONS WHERE cast(CONNID as varchar)='#P1#' OR CONNNAME='#P1#'";
	}
}