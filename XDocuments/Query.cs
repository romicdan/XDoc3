using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using XDataSourceModule;
using System.Data.SqlClient;
using KubionLogNamespace;

namespace XDocuments
{
    internal class Query : IXQuery
    {
        #region Private members

        private int 
            m_ID = -1,
            m_connectionID = -1,
            m_templateID = -1;
        private string 
            m_name = "",
            m_queryText = "";

        private XProviderData m_dataProvider = null;

        #endregion Private members


        #region Public properties

        public int ID
        {
            get { return m_ID; }
        }

        public int ConnectionID
        {
            get { return m_connectionID; }
            set 
            {
                m_connectionID = value;
            }
        }

        public int TemplateID
        {
            get { return m_templateID; }
            set 
            {
                m_templateID = value; 
            }
        }

        public string Name
        {
            get { return m_name; }
            set 
            {
                m_name = value; 
            }
        }

        public string QueryText
        {
            get { return m_queryText; }
            set 
            {
                m_queryText = value; 
            }
        }

        #endregion Public properties


        #region Constructors

        internal Query(XProviderData dataProvider)
        {
            m_dataProvider = dataProvider;
        }

        internal Query(string name, int templateID, XProviderData dataProvider)
        {
            m_dataProvider = dataProvider;
            Load(name, templateID);
        }

        internal Query(int id, XProviderData dataProvider)
        {
            m_dataProvider = dataProvider;
            Load(id);
        }

        #endregion Constructors


        #region Internal methods

        internal void Load(DataRow row)
        {
            m_ID = Connectors.GetInt32Result(row[S_QUERIES.QUERYID]);
            m_name = Connectors.GetStringResult(row[S_QUERIES.QUERYNAME]);
            m_queryText = Connectors.GetStringResult(row[S_QUERIES.QUERYTEXT]);
            m_connectionID = Connectors.GetInt32Result(row[S_QUERIES.CONNID]);
            m_templateID = Connectors.GetInt32Result(row[S_QUERIES.TEMPLATEID]);

        }


        #endregion Internal methods


        #region Public methods

        public void Load(string name, int templateID)
        {
            //string sQueryStatement;
            IDataParameter[] pars = new IDataParameter[]
            {
                new SqlParameter("@" + S_QUERIES.TEMPLATEID, templateID),
                new SqlParameter("@" + S_QUERIES.QUERYNAME, name)
            };

            try
            {
                //sQueryStatement = SQL.LoadQueryByName;
                //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.TEMPLATEID, templateID.ToString());
                //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYNAME, "'" + name + "'");
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadQueryByName, pars);
                //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception("Could not find query");

                Load(dtResult.Rows[0]);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not load query '" + name + "' for TemplateID = " + templateID.ToString() + Environment.NewLine + ex.Message);
            }
        }


        public void Load(int id)
        {
            //string sQueryStatement;
            IDataParameter[] pars = new IDataParameter[]
            {
                new SqlParameter("@" + S_QUERIES.QUERYID, id)
            };

            try
            {
                //sQueryStatement = SQL.LoadQueryByName;
                //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYID, id.ToString());
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadQueryByID, pars);
                //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception("Could not find query");

                Load(dtResult.Rows[0]);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not load query ID='" + id.ToString() + Environment.NewLine + ex.Message);
            }
        }

        public void Save()
        {
            //string sQueryStatement;
            IDataParameter[] pars = new IDataParameter[]
            {
                new SqlParameter("@" + S_QUERIES.QUERYID, m_ID),
                new SqlParameter("@" + S_QUERIES.QUERYNAME, m_name),
                new SqlParameter("@" + S_QUERIES.QUERYTEXT, m_queryText),
                new SqlParameter("@" + S_QUERIES.CONNID, m_connectionID),
                new SqlParameter("@" + S_QUERIES.TEMPLATEID, m_templateID),
            };
            if (m_ID == -1)
            {
                try
                {
                    CheckValid();
                    //sQueryStatement = SQL.CreateNewQuery;
                    //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYID, m_ID.ToString());
                    //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYNAME, "'" + m_name + "'");
                    //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYTEXT, "'" + m_queryText + "'");
                    //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.CONNID, m_connectionID.ToString());
                    //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.TEMPLATEID, m_templateID.ToString());
                    DataTable dtResult = m_dataProvider.GetDataTable(SQL.CreateNewQuery, pars);
                    //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                    if (dtResult != null && dtResult.Rows.Count > 0)
                        m_ID = Connectors.GetInt32Result(dtResult.Rows[0][S_QUERIES.QUERYID]);
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    string message = "Could not save new query '" + m_name + "' for templateID = " + m_templateID.ToString() + Environment.NewLine + ex.Message;
                    throw new Exception(message);
                }
            }
            else
            {
                try
                {
                    CheckValid();
                    //sQueryStatement = SQL.UpdateQuery;
                    //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYID, m_ID.ToString());
                    //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYNAME, "'" + m_name + "'");
                    //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYTEXT, "'" + m_queryText + "'");
                    //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.CONNID, m_connectionID.ToString());
                    //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.TEMPLATEID, m_templateID.ToString());
                    m_dataProvider.ExecuteNonQuery(SQL.UpdateQuery, pars);
                    //m_dataProvider.ExecuteNonQuery(sQueryStatement);
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    string message = "Could not update query '" + m_name + "' for templateID = " + m_templateID.ToString() + Environment.NewLine + ex.Message;
                    throw new Exception(message);
                }
            }

        }


        #endregion Public methods

        #region Public static methods

        public static void Delete(int id, XProviderData dataProvider)
        {
            //string sQueryStatement;
            try
            {
                //sQueryStatement = SQL.DeleteQuery;
                //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYID, id.ToString());
                dataProvider.ExecuteNonQuery(SQL.DeleteQuery, new IDataParameter[] { new SqlParameter("@" + S_QUERIES.QUERYID, id) });
                //dataProvider.ExecuteNonQuery(sQueryStatement);
            }
            catch (Exception ex)
            {
                string message = "Could not delete query with ID = " + id.ToString() + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
        }

        public static void DeleteForTemplate(int id, XProviderData dataProvider)
        {
            //string sQueryStatement;
            try
            {
                //sQueryStatement = SQL.DeleteQueries;
                //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.TEMPLATEID, id.ToString());
                dataProvider.ExecuteNonQuery(SQL.DeleteQueries, new IDataParameter[] { new SqlParameter("@" + S_QUERIES.TEMPLATEID, id) });
                //dataProvider.ExecuteNonQuery(sQueryStatement);
            }
            catch (Exception ex)
            {
                string message = "Could not delete queries for template ID = " + id.ToString() + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
        }

        public static void DeleteForTemplate(string  name, XProviderData dataProvider)
        {
            //string sQueryStatement;
            try
            {
                //sQueryStatement = SQL.DeleteQueries;
                //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.TEMPLATEID, id.ToString());
                dataProvider.ExecuteNonQuery(SQL.DeleteQueriesN, new IDataParameter[] { new SqlParameter("@" + S_QUERIES.TEMPLATENAME , name) });
                //dataProvider.ExecuteNonQuery(sQueryStatement);
            }
            catch (Exception ex)
            {
                string message = "Could not delete queries for template " + name + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
        }
        #endregion Public static methods

        #region Private methods

        void CheckValid()
        {
            if (m_templateID == -1)
                throw new Exception("TemplateID was not specified");
            if (m_queryText == "")
                throw new Exception("The query text was not specified");
        }

        #endregion Private methods

    }
}
