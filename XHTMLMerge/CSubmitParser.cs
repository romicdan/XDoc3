using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Data.SqlClient;
using System.IO;
using XDataSourceModule;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using KubionLogNamespace;


namespace XHTMLMerge
{
    public class CSubmitParser
	{
		#region Protected members

		XDataSourceModule.XDataUpdate m_Update = null;

        
        Regex regExTags = new Regex("<\\/?\\w+((\\s+\\w+(\\s*=\\s*(?:\\\".*?\\\"|'.*?'|[^'\\\">\\s]+))?)+\\s*|\\s*)\\/?>", RegexOptions.IgnoreCase);
		Regex regExAttachmentsTags = new Regex("(<img(.)*>)|(<a(\x20)+(.)*>)");
        //Regex regExUpdateTags = new Regex("<INPUT(\x20)+(.)*/>", RegexOptions.IgnoreCase);
		Regex regExParam = new Regex ("param=\"");
		Regex regExID = new Regex("(id=\"[^\">]*\")|(id=[^\x20>]+)");

		#endregion

		#region Const members
		protected const string 
            SYS_DOCID = "DOCID",
            SYS_TEMPLATEID = "TEMPLATEID",
            ATTR_ONCHANGE = "onchange",
            ATTR_VALUE = "value",
            ATTR_PARAM = "param",
            ATTR_INPUT_TYPE = "type",
            ATTR_UPD = "upd",
            UPD = "UPD";
		#endregion

		public CSubmitParser( )
		{
			m_Update = new XDataSourceModule.XDataUpdate();
		}

		public ArrayList GetAttachmentsFileNames(string htmlBody)
		{
			ArrayList ret = new ArrayList();
			MatchCollection mc = regExAttachmentsTags.Matches(htmlBody);
			foreach (Match m in mc)
			{
				string tag = m.Value;
				AddAttachmentToList(tag,ref ret);
			}

			return ret;
		}

		private void AddAttachmentToList(string tag, ref ArrayList arrFiles)
		{
            StringCollection collKeys = new StringCollection();
            collKeys.Add("src=\"");
            collKeys.Add("href=\"");

            foreach (string key in collKeys)
            {
                int i1 = tag.IndexOf(key);
                if (i1 != -1)
                {
                    int i2 = tag.IndexOf("\"", i1 + key.Length + 1);
                    if (i2 != -1)
                    {
                        string file = tag.Substring(i1 + key.Length, i2 - i1 - key.Length);
                        /*int i3 = file.LastIndexOf("/");
                        if (i3 != -1)
                            file = file.Substring(i3 + 1);*/
                        arrFiles.Add(file);
                    }
                }
            }
		}


        string GetTagValue(string tag)
        {
            if (tag == "" || tag == null)
                return "";

            Regex rexValue = new Regex(@"value(\s)*=(\s)*", RegexOptions.IgnoreCase);
            Match m = rexValue.Match(tag);
            if (m.Success)
            {
                int startIndex = m.Index + m.Length;
                if (startIndex < tag.Length - 1)
                {

                    string delimiter = ""; // this is the surrounding " or '
                    if (tag[startIndex] == '\'' || tag[startIndex] == '"')
                        delimiter = tag[startIndex].ToString();
                    startIndex += delimiter.Length;
                    int endIndex = -1;
                    if (delimiter != "")
                    {
                        endIndex = tag.IndexOf(delimiter, startIndex);
                    }
                    else
                    {
                        endIndex = tag.IndexOf(" ", startIndex);
                        if (endIndex == -1)
                            endIndex = tag.IndexOf("\t");
                        if (endIndex == -1)
                            endIndex = tag.IndexOf("/>");
                        if (endIndex == -1)
                            endIndex = tag.IndexOf(">");
                    }
                    if (endIndex == -1)
                        return "";
                    if (startIndex <= endIndex)
                    {
                        return tag.Substring(startIndex, endIndex - startIndex);
                    }

                }

            }

            return "";
        }



		public bool PromoteUpdates( int templateID, XProviderData dataProvider, int documentID, string documentBody, ref Hashtable newParamsValues, out string error)
		{
			error = "";
			if (documentID != -1) 
			{
				if(!newParamsValues.ContainsKey(SYS_DOCID))
					newParamsValues[SYS_DOCID] = documentID;
                if(!newParamsValues.ContainsKey(SYS_TEMPLATEID))
                    newParamsValues[SYS_TEMPLATEID] = templateID;
			}
			string searchKey = "upd=\"";

            m_Update.Initialize(templateID, newParamsValues);
			MatchCollection mcTags = regExTags.Matches(documentBody);
			foreach (Match mTag in mcTags)
			{
				Match mParam = regExParam.Match(mTag.Value);
				string tag = mTag.Value;
				int i1 = tag.IndexOf(searchKey);
				if (i1 == -1) 
                    continue;
				int i2 = tag.IndexOf("\"", i1 + searchKey.Length);
				if (i2 == -1) 
                    continue;

				string updateStatement = tag.Substring(i1 + searchKey.Length, i2 - i1 - searchKey.Length); 
				string[] updStatements = updateStatement.Split(';');
				foreach(string param in updStatements)
				{
					string val = GetTagValue(tag);

                    val = Utils.Encode(val, EncodeOption.Decode);

					int iodp = param.IndexOf(":");
					
					string []arrUpdParams = null;
                    string templateName = "";
					string updBody = "";
					if (iodp > -1)
					{
						arrUpdParams = param.Substring(iodp+1).Split(',');
						updBody = param.Substring(0, iodp);
					}
					else 
						updBody = param;
					string [] parts = updBody.Split('.');
					int len = parts.GetLength(0);
					if (len < 2 || len > 3)
					{
						error = "Invalid update command: " + param;
						Trace.WriteLine(error);
                        KubionLogNamespace.KubionLog.WriteLine(error);
                        return false;
					}

                    if (parts[0].Trim().ToUpper() != "UPD")
                        templateName = parts[0];                        

					int ioOpenBr = parts[1].IndexOf("(");
					int ioCloseBr = parts[1].LastIndexOf(")");
					string [] arrParams  = null;
                    string updateName = ioOpenBr != -1 ? parts[1].Substring(0, ioOpenBr) : parts[1];
					if(ioOpenBr != -1 && ioCloseBr != -1)
					{
						string strParams = parts[1].Substring(ioOpenBr+1, ioCloseBr-ioOpenBr-1);
                        if (strParams != "")
                        {
                            string parametersText = parts[1].Substring(ioOpenBr + 1, ioCloseBr - ioOpenBr - 1);
                            List<ParseUtils.ListParameter> paramsColl = ParseUtils.GetParameterList(parametersText, '\'', ',', false);
                            arrParams = ParseUtils.StringArrayFromParamList(paramsColl);
                        }
					}

					string fieldName = "";
					if (len == 3)
						fieldName = parts[2];

					fieldName = fieldName.TrimEnd().ToUpper();
		
					m_Update.AddUpdateField(templateName, updateName, arrParams, arrUpdParams, fieldName, val);
				}
			}

			if(!m_Update.Execute(dataProvider, out error))
			{
				Trace.WriteLine(error);
                KubionLogNamespace.KubionLog.WriteLine(error);
                return false;
			}

			return true;
			
		}

        string TagAttributeValue(string tag, string attribute, ref int index)
        {
            string sAtt = "";
            string sAtt1 = "";
            Regex regexAttribute = new Regex(attribute + "=('(([^']*\\\\')*[^']*|[^']*)'|\"(([^\"]*\\\\\")*[^\"]*|[^\"]*)\"|[^\\s]*)", RegexOptions.IgnoreCase);
            Match m = regexAttribute.Match(tag);
            if (m.Success)
            {
                sAtt = m.ToString ();
                if (sAtt.StartsWith(attribute + "='"))
                    sAtt1 = sAtt.Substring((attribute + "='").Length, sAtt.Length - 1 - (attribute + "='").Length);
                else if (sAtt.StartsWith(attribute + "=\""))
                    sAtt1 = sAtt.Substring((attribute + "=\"").Length, sAtt.Length - 1 - (attribute + "=\"").Length);
                else if (sAtt.StartsWith(attribute + "="))
                    sAtt1 = sAtt.Substring((attribute + "=").Length, sAtt.Length  - (attribute + "=").Length);

            }
            return sAtt1;

            //Regex rxAttribute = new Regex(attribute + @"(\s)*=", RegexOptions.IgnoreCase);

            //Match m = rxAttribute.Match(tag);

            //if (m.Success)
            //{
            //    int i1 = m.Index + m.Length;
            //    if (attribute.Length > i1 + 1)
            //    {
            //        string quote = "";
            //        int quoteIndex = -1;
            //        if (attribute[i1 + 1] != ' ' && attribute[i1 + 1] != '\'' && attribute[i1 + 1] != '"')
            //        {
                        
            //            //search the quote
            //            for (int i = i1 + 1; i < attribute.Length; i ++)
            //                if (attribute[i
            //        }

            //    }
            //}

            ////bool quote = true;
            ////attribute = " " + attribute + "=\"";
            ////int i1 = tag.ToLower().IndexOf(attribute);
            ////if (i1 == -1)
            ////{
            ////    attribute = attribute.Remove(attribute.Length - 1);
            ////    quote = false;
            ////    i1 = tag.ToLower().IndexOf(attribute);
            ////    if (i1 == -1)
            ////        return "";
            ////}
            ////string closingKey = quote ? "\"" : " ";
            ////int i2 = tag.IndexOf(closingKey, i1 + attribute.Length);
            ////if (i2 == -1)
            ////    return "";
            ////string ret = tag.Substring(i1 + attribute.Length, i2 - i1 - attribute.Length);
            ////index = i1;
            ////return ret;
        }

		public string RemoveUpdAttributes(string text)
		{
			string ret = RemoveAttribute (" upd=\"", text);
			return ret;
		}

		public string RemoveParamAttributes(string text)
		{
			string ret = RemoveAttribute (" param=\"", text);
			return ret;
		}

		private string RemoveAttribute(string key, string text)
		{
			SortedList removeLengths = new SortedList();
			MatchCollection mcTags = regExTags.Matches(text);
			foreach (Match mTag in mcTags)
			{
				string tag = mTag.Value;
				int i1 = tag.IndexOf(key);
				if (i1 != -1)
				{
					int i2 = tag.IndexOf("\"",i1+key.Length);
					if (i2 != -1)
						removeLengths.Add(mTag.Index + i1, i2-i1+1);
				}
			}

			IDictionaryEnumerator en = removeLengths.GetEnumerator();
			int offset = 0;


            StringBuilder sb = new StringBuilder(text);
            while (en.MoveNext())
			{
				int index = (int)en.Key - offset;
				int removeLength = (int) removeLengths[en.Key];
				sb = sb.Remove (index, removeLength);
				offset += removeLength;
			}
            text = sb.ToString();
			return text;		
		}

        public string InsertOnChange(string text)
        {
            //NO onchange event
            return text;

            SortedList insertStrings = new SortedList();

            MatchCollection mcTags = regExTags.Matches(text);
            foreach (Match mTag in mcTags)
            {
                string tag = mTag.Value;

                string strJS = "";

                if (tag.ToUpper().StartsWith("<INPUT"))
                {
                    int attrIndex = -1;
                    string inputType = TagAttributeValue(tag, ATTR_INPUT_TYPE, ref attrIndex);
                    if (inputType.ToUpper() == "RADIO" || inputType.ToUpper() == "CHECKBOX")
                    {
                        strJS = "this.setAttribute('checked', this.checked);";
                    }
                    else if (inputType.ToUpper() == "PASSWORD" || inputType.ToUpper() == "HIDDEN" ||
                        inputType.ToUpper() == "TEXT")
                        strJS = "this.setAttribute('value', this.value);";
                }
                else if (tag.ToUpper().StartsWith("<SELECT"))
                    strJS = "for (i=0; i < this.options.length; i++){var opt = this.options[i];opt.removeAttribute('selected');if (opt.selected == true)opt.setAttribute('selected', true);}";
                else if (tag.ToUpper().StartsWith("<TEXTAREA"))
                    strJS = "var oTextNode = document.createTextNode(this.value); for (var i = this.childNodes.length - 1; i >= 0; i--) this.removeChild(this.childNodes[i]); this.appendChild(oTextNode);";

                if (strJS == "")
                    continue;


                int iOnChg = -1;
                string onChange = TagAttributeValue(tag, ATTR_ONCHANGE, ref iOnChg);
                if (iOnChg == -1) // the input does not have an onchange
                {
                    string closing = "/>";
                    int iEnd = tag.IndexOf(closing);
                    if (iEnd == -1)
                    {
                        closing = ">";
                        iEnd = tag.IndexOf(closing);
                    }

                    if (iEnd == -1)
                        throw new Exception(">>InsertOnChange - Tag " + tag + " does not have a closing bracket");

                    insertStrings.Add(mTag.Index + iEnd, " onchange=\"" + strJS + "\"");
                }
                else // input has onchange; append at the end
                {
                    int idxToInsert = mTag.Index + iOnChg + (ATTR_ONCHANGE + "=\"").Length + onChange.Length + 1;
                    insertStrings.Add(idxToInsert, ";" + strJS);
                }
            }

            IDictionaryEnumerator en = insertStrings.GetEnumerator();
            int offset = 0;

            StringBuilder sbText = new StringBuilder(text);
            while (en.MoveNext())
            {
                int index = offset + (int)en.Key;
                string strInsert = (string)en.Value;
                sbText = sbText.Insert(index, strInsert);
                offset += strInsert.Length;
            }

            return sbText.ToString();
        }

        public Hashtable GetNewParamValues(string html, Hashtable prevParams)
        {
            Hashtable ret = CollectionsUtil.CreateCaseInsensitiveHashtable();
            IDictionaryEnumerator en = prevParams.GetEnumerator();
            while (en.MoveNext())
                ret[en.Key] = en.Value;

            MatchCollection mcTags = regExTags.Matches(html);
            foreach (Match mTag in mcTags)
            {
                string tag = mTag.Value;
                if (! tag.ToUpper().StartsWith("<INPUT"))
                    continue;
                int iValue = 0, iParam = 0;

                string val = TagAttributeValue(tag, ATTR_VALUE, ref iValue);

                val = Utils.Encode(val, EncodeOption.Decode);

                string param = TagAttributeValue(tag, ATTR_PARAM, ref iParam);
                if (param == "")
                    continue;
                string key = "";
                if (ret.ContainsKey(param.ToUpper()))
                    key = param.ToUpper();
                else 
                    key = param;
                ret[key] = Utils.Encode(val, EncodeOption.Decode);
//                ret[key] = Utils.Encode(val, EncodeOption.HTMLDecode );
            }

            return ret;

        }

	}
}
