using System;
using System.Collections;
using System.Collections.Specialized;

namespace XHTMLMerge
{
    [Serializable]
    public class CContext
	{
		protected Hashtable m_htQueryIndexes = null;
        public Hashtable Results = CollectionsUtil.CreateCaseInsensitiveHashtable();
        public  Hashtable Keys = CollectionsUtil.CreateCaseInsensitiveHashtable();

		public CContext()
		{
			m_htQueryIndexes = CollectionsUtil.CreateCaseInsensitiveHashtable();
		}
        public int OIncrementQIndex(Object obj)
        {
            return IncrementQIndex((string)Keys[obj]);
        }

        public void OResetQIndex(Object obj)
        {
            ResetQIndex((string)Keys[obj]);
        }

        public int OGetQueryIndex(Object obj)
        {
            return GetQueryIndex((string)Keys[obj]);
        }

        public void OSetQueryIndex(Object obj, int index)
        {
            SetQueryIndex((string)Keys[obj], index);
        }


		private  int IncrementQIndex (string queryName)
		{
			if (m_htQueryIndexes[queryName] == null)
				m_htQueryIndexes[queryName] = 0;
			int index = (int) m_htQueryIndexes[queryName];
			m_htQueryIndexes[queryName] = index +1;
			return index+1;
		}

        private void ResetQIndex(string queryName)
		{
			m_htQueryIndexes[queryName] = 0;
		}

        private int GetQueryIndex(string queryName)
		{
			int index = -1;
			if (m_htQueryIndexes[queryName] != null)
				index = (int) m_htQueryIndexes[queryName];
			return index;
		}

        private void SetQueryIndex(string queryName, int index)
        {
            m_htQueryIndexes[queryName] = index;
        }


	}
}
