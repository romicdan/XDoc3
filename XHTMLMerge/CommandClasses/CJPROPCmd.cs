﻿using System;
using System.Diagnostics;
using XDataSourceModule;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace XHTMLMerge
{
    [Serializable]
    public class CJPROPCmd : CCmd
    {
        # region Protected members

        protected string m_ParamName = "";
        protected string m_Source = "";
        protected string m_JPath = "";
        protected string m_ErrorParamName = "ERROR";

        #endregion Protected members

        #region Public properties

        public string ParameterName
        {
            get { return m_ParamName; }
            set { m_ParamName = value; }
        }
        public string Source
        {
            get { return m_Source; }
            set { m_Source = value; }
        }
        public string JPath
        {
            get { return m_JPath; }
            set { m_JPath = value; }
        }
        public string ErrorParamName
        {
            get { return m_ErrorParamName; }
            set { m_ErrorParamName = value; }
        }
        protected string m_isParSource = "0";
        public string isParSource
        {
            get { return m_isParSource; }
            set { m_isParSource = value; }
        }


        #endregion Public properties

        public CJPROPCmd()
            : base()
        {
            this.m_enType = CommandType.JPROPCommand ;
            m_bIsBlockCommand = true;
        }

        public override string Execute(CParser m_parser)
        {
            string l_ParamName, l_Source, l_JPath, l_ErrorParamName;
            l_ParamName = m_parser.ReplaceParameters(m_ParamName);
            l_Source = m_parser.ReplaceParameters(m_Source);
            l_JPath = m_parser.ReplaceParameters(m_JPath);
            l_ErrorParamName = m_parser.ReplaceParameters(m_ErrorParamName);

            if (m_isParSource == "1") l_Source = (string)m_parser.TemplateParams[l_Source];

            string val = "";
            string sError = "";
            string sErrorVerbose = "";
            System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();

            try
            {
                JObject o = JObject.Parse(l_Source);
                if (l_ParamName == "")
                    return o.SelectToken(l_JPath).ToString();
                else
                {
                    JToken jt = o.SelectToken(l_JPath);
                    val = jt.ToString();
                    if (jt is JArray)
                        m_parser.TemplateParams[l_ParamName + "__count"] = ((JArray)jt).Count.ToString();

                    m_parser.TemplateParams[l_ParamName] = val;

                    foreach (JProperty jp in jt.Children<JProperty>())
                    {
                        m_parser.TemplateParams[l_ParamName + "_name" ] = jp.Name ;
                        if (jp.Value.Type == JTokenType.Date)
                        {
                            DateTime d = (DateTime)jp.Value;
                            m_parser.TemplateParams[l_ParamName + "_value"] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        }
                        else
                            m_parser.TemplateParams[l_ParamName + "_value"] = jp.Value;
                        string output = base.Execute(m_parser);
                        sbRetVal.Append(output);
                        m_parser.TemplateParams.Remove(l_ParamName + "_name");
                        m_parser.TemplateParams.Remove(l_ParamName + "_value");
                    }
                    return sbRetVal.ToString();
                }
            }
            catch (Exception ex)
            {
                sError = "Error executing JPROP command";
                sErrorVerbose = ex.Message;
                sErrorVerbose = "Error executing JPROP command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sErrorVerbose;
                sbRetVal.Clear();
            }

            if (l_ErrorParamName != "")
            {
                m_parser.TemplateParams[l_ErrorParamName] = sError;
                m_parser.TemplateParams[l_ErrorParamName + "Verbose"] = sErrorVerbose;
            }

            return sbRetVal.ToString();
        }

    }
}