using System;
using System.IO;
using System.Diagnostics;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CAttachCmd : CCmd
	{
		#region Private members

		AttachType m_attachType;
		string m_queryName = "";
		string m_fieldName = "";
		string m_ext = "";
		string m_fileName = "";

		XDataSourceModule.IXDataSource m_evaluator = null;
		Array m_parameters = null;
		CCmd m_parent = null;

        //protected CContext m_context = null;

		#endregion

		#region Public properties

		public string Extension
		{
			get { return m_ext; }
			set { m_ext = value; }
		}

		public string FileName
		{
			get { return m_fileName; }
			set { m_fileName = value; }
		}

		public AttachType AttachType
		{
			get { return m_attachType; }
			set { m_attachType = value; }
		}
		
		public string QueryName
		{
			get { return m_queryName; }
			set { m_queryName = value; }
		}

		public string FieldName
		{
			get { return m_fieldName; }
			set { m_fieldName = value; }
		}


        //public CContext Context
        //{
        //    get { return m_context; }
        //    set { m_context = value; }
        //}

		public XDataSourceModule.IXDataSource Evaluator
		{
			get { return m_evaluator; }
			set { m_evaluator = value; }
		}

		public Array Parameters
		{
			get {return m_parameters;}
			set {m_parameters = value;}
		}

		public CCmd Parent
		{
			get { return m_parent; }
			set { m_parent = value; }
		}

		#endregion


		public CAttachCmd():base()
		{
			m_enType = CommandType.AttachCommand;
			m_bIsBlockCommand = false;
		}

        public override string Execute(CParser m_parser)
        {
			string retVal = "";
			if (!m_ext.StartsWith("."))
				m_ext = "." + m_ext;

			string fileNameDest = Guid.NewGuid().ToString("N") + m_ext;
			string filePathDest = Path.Combine(m_parser.OutputPath,fileNameDest);

            try
            {
                bool fileCopied = false;
                if (m_attachType == AttachType.Database)
                {
                    int queryIndex = m_parser.Context.OGetQueryIndex(m_parent);
//                    int queryIndex = m_context.GetQueryIndex(m_queryName);
                    object oVal = "";

                    bool bDefQry = (m_parser.Queries[m_queryName] != null);
                    Array parameters = null;
                    if (m_parent is CREPCmd)
                        parameters = ((CREPCmd)m_parent).GetParamArray(m_parser, bDefQry);
                    else if (m_parent is CQRYCmd)
                        parameters = ((CQRYCmd)m_parent).GetParamArray(m_parser, bDefQry);

                    string error = "";

                    if (!m_evaluator.GetValue(m_fieldName, m_queryName, parameters, queryIndex, out oVal, out error) && error != "")
                        throw (new Exception(error));

                    if ((oVal is byte[] || oVal is string) && m_parser.VirtualPath != "" && m_parser.OutputPath != "")
                    {
                        byte[] stream = null;
                        if (oVal.GetType() == typeof(byte[]))
                            stream = (byte[])oVal;
                        if (oVal.GetType() == typeof(string))
                            stream = Encoding.UTF8.GetBytes((string)oVal);

                        FileStream fs = null;
                        try
                        {
                            fs = File.Create(filePathDest);
                            fs.Write(stream, 0, stream.Length);
                            fileCopied = true;
                        }
                        catch (Exception ex)
                        {
                            throw ;
                        }
                        finally
                        {
                            if (fs != null) fs.Close();
                        }
                    }
                }
                else // file attachment
                {
                    if (m_vChildCmds.Count == 1)
                        m_fileName = m_vChildCmds[0].Execute(m_parser);
                    if (!File.Exists(m_fileName))
                    {
                        string temp = Path.Combine(m_parser.OutputPath, m_fileName);
                        if (!File.Exists(temp))
                            throw new Exception("Cannot attach file " + m_fileName + ". File cannot be found.");
                        m_fileName = temp;
                    }
                    File.Copy(m_fileName, filePathDest);
                    fileCopied = true;
                }
                if (fileCopied)
                    retVal = "%ATTPATH%/" + fileNameDest;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                string queryName = m_queryName == "" ? "" : "for query " + m_queryName + " and field " + m_fieldName + " ";
                throw new Exception("Error executing ATTACH command " + queryName + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + ex.Message);
            }
			return retVal;

		}


	}
}
