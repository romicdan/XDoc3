using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;


namespace XHTMLMerge
{
    [Serializable]
    public class CCmd
	{
		#region Protected members

        protected int m_ID;
        protected int 
			m_nStartIndex = -1,
			m_nEndIndex = -1;
		protected bool m_bIsBlockCommand;
		protected CommandType m_enType;
		protected CmdCollection m_vChildCmds;
        [NonSerialized]
        protected CParser orig_m_parser = null;
        protected string newLine = Environment.NewLine;

		#endregion Protected members

		#region Public properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        
        public int StartIndex
		{
			get	{ return m_nStartIndex;	}
			set	{ m_nStartIndex = value;	}
		}

		public int EndIndex
		{
			get	{ return m_nEndIndex; }
			set	{ m_nEndIndex = value; }
		}

		public CommandType Type
		{
			get { return m_enType; }
			set {
				m_enType = value; 
			}
		}

        [System.Xml.Serialization.XmlIgnore]
        public CParser Parser
		{
			get { return orig_m_parser; }
			set { orig_m_parser = value; }
		}


		public CmdCollection ChildCommands
		{
			get { return m_vChildCmds; }
			set { m_vChildCmds = value; }
		}

		public bool IsBlockComand
		{
			get { return m_bIsBlockCommand; }
		}

		#endregion Public properties

        public CCmd()
            : this(-1)
		{
		}

        public CCmd(int i)
        {
            m_ID = i;
            m_vChildCmds = new CmdCollection();
        }

        //public virtual string Execute()
        //{
        //    string retVal = "";
        //    foreach (CCmd cmd in m_vChildCmds)
        //        retVal += cmd.Execute();

        //    return retVal;
        //}

        public virtual string Execute(CParser m_parser)
        {
            //System.Diagnostics.Debug.WriteLine(this.m_enType);
            //string retVal = "";
            System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();
            foreach (CCmd cmd in m_vChildCmds)
            {
                //System.Diagnostics.Debug.WriteLine(cmd.Type );
                //retVal += cmd.Execute(m_parser);
                    sbRetVal.Append(cmd.Execute(m_parser));
            }
            //return retVal;
            return sbRetVal.ToString();
        }

	}
}
