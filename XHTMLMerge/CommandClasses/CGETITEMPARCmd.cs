using System;
using KubionLogNamespace;

namespace XHTMLMerge
{
    [Serializable]
    public class CGETITEMPARCmd : CCmd
    {
        string m_ParName = "";
        string m_Indexes = "";
        string m_VarName = "";
        string m_ContextName = "";
        string m_ID = "ID";

        public string ParName
        {
            get { return m_ParName; }
            set { m_ParName = value; }
        }
        public string Indexes
        {
            get { return m_Indexes; }
            set { m_Indexes = value; }
        }
        public string VarName
        {
            get { return m_VarName; }
            set { m_VarName = value; }
        }
        public string ContextName
        {
            get { return m_ContextName; }
            set { m_ContextName = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public CGETITEMPARCmd()
            : base()
        {
            m_enType = CommandType.GETITEMPARCommand ;
            this.m_bIsBlockCommand = false;
        }


        public override string Execute(CParser m_parser)
        {
            string l_VarName, l_ContextName, l_ID, l_Indexes, l_ParName;

            l_VarName = m_parser.ReplaceParameters(m_VarName);
            l_ContextName = m_parser.ReplaceParameters(m_ContextName);
            l_ID = m_parser.ReplaceParameters(m_ID);
            l_Indexes = m_parser.ReplaceParameters(m_Indexes);
            l_ParName = m_parser.ReplaceParameters(m_ParName);

            string sPar = m_parser.GetSV(l_VarName, l_ContextName, l_ID, "");
            if (sPar == "missing_setting") sPar = "";
            int index2=-1;
            if (l_Indexes.Contains(","))
            {
                index2 = int.Parse(l_Indexes.Substring(l_Indexes.IndexOf(",") + 1));
                l_Indexes = l_Indexes.Substring(0, l_Indexes.IndexOf(","));
            }
            int index1 = int.Parse ( l_Indexes) ;

            //sPar = sPar.Replace("\\|", "<tilda>");
            //string[] aData = sPar.Split ('|'); 
            string[] aData =Utils.MySplit( sPar,'|'); 
            //sPar  = "";
            if (index1 == 0) sPar = (sPar == "" ? "0" : (aData.Length).ToString());
            else if (aData.Length >= index1) sPar = aData[index1 - 1]; else sPar = "";
            //sPar = sPar.Replace("<tilda>", "|");

            if (index2 > -1)
            {
                //sPar = sPar.Replace("\\,", "<tilda>");
                //aData = sPar.Split(',');
                aData =Utils.MySplit(sPar,','); 
                if (index2 == 0) sPar = (sPar == "" ? "0" : (aData.Length).ToString());
                else if (aData.Length >= index2) sPar = aData[index2 - 1]; else sPar = "";
                //sPar = sPar.Replace("<tilda>", ",");
            }
            m_parser.ParamDefaults[l_ParName] = sPar;
            m_parser.TemplateParams[l_ParName] = sPar;

            return "";
        }

    }
}

