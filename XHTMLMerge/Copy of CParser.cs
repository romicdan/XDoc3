using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using XDataSourceModule;
using System.IO;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Web;
using System.Collections.ObjectModel;
using System.Collections.Generic;



namespace XHTMLMerge
{
    public class CParser
    {

        public delegate void RequestTemplateText_Handler(CParser sender, int templateID);
        public event RequestTemplateText_Handler RequestTemplateText;

        public delegate void RequestTemplateID_Handler(CParser sender, string templateName);
        public event RequestTemplateID_Handler RequestTemplateID;

        public event SourceResult.RequestHierarcyEventHandler RequestHierarchy;

        #region Protected Members

        Stack m_stack = null;
        string m_text = "",
            m_outputPath = "",
            m_virtualPath = "",
            m_textForInclude = "",
            m_originalText = "";


        CContext m_context = null;
        IXDataSource m_evaluator = null;
        XProviderData m_dataProvider = null;
        int m_templateID, m_idForInclude = -1;

        ArrayList m_previousTemplateIDs = new ArrayList();

        Hashtable m_additionalIncludeParams = CollectionsUtil.CreateCaseInsensitiveHashtable();
        Hashtable m_paramDefaults = CollectionsUtil.CreateCaseInsensitiveHashtable();
        IndexMappings m_mappings = null;

        const int SEPARATOR_LENGTH = 1;

        Regex
            //includeREx = new Regex(@"<!--INCLUDE[\s\S]*<!--ENDINCLUDE-->"),
            wordREx = new Regex(@"(\w)+"),
            ifParamREx = new Regex(@"(#[^#]+#)|[^#]+"),
            generalFieldREx = new Regex(@"(#((COUNT)|(UDF)|(DEFPAR)|(REP)|(QRY)|(IF)|(ELSEIF)|(PAR)|(FLD)|(FLDBIN)|(ATTACH)|(ENDREP)|(ENDIF)|(ENDQRY)|(INCLUDE))[^#\x28]*(\x28[^\x29]*\x29)?(.\x28[^\x29]*\x29)?#)|[^#]+", RegexOptions.IgnoreCase);

        struct ParserParam
        {
            public int Index;
            public string Value;
        }


        #endregion

        #region Public Methods
        public CParser(int templateID, XProviderData dataProvider, string text, Hashtable templateParams, string virtualPath, string outputPath)
        {
            m_stack = new Stack();
            m_text = text;
            m_context = new CContext();
            m_evaluator = new XDataSource();
            m_evaluator.Initialize(templateID, dataProvider, templateParams);
            m_outputPath = outputPath;
            m_virtualPath = virtualPath;
            m_templateID = templateID;
            m_dataProvider = dataProvider;
        }

        public CParser(int templateID, XProviderData dataProvider, string text, Hashtable templateParams)
            : this(templateID, dataProvider, text, templateParams, "", "")
        {
        }



        #region Public properties

        /// <summary>
        /// Initial text, eventually containing STARTBLOCK and ENDBLOCK commands
        /// </summary>
        public string OriginalText
        {
            get { return m_originalText; }
            set { m_originalText = value; }
        }


        /// <summary>
        /// Holds the index ranges where unparsable blocks of text are found
        /// </summary>
        public IndexMappings Mappings
        {
            get { return m_mappings; }
            set { m_mappings = value; }
        }

        /// <summary>
        /// Set/Get template text that is used to generate document
        /// </summary>
        public string TemplateText
        {
            get { return m_text; }
            set { m_text = value; }
        }


        /// <summary>
        /// Get/Set an IXDataSource instance
        /// </summary>
        public IXDataSource Evaluator
        {
            get { return m_evaluator; }
            set { m_evaluator = value; }
        }

        /// <summary>
        /// Get/Set output path
        /// </summary>
        public string OutputPath
        {
            get { return m_outputPath; }
            set { m_outputPath = value; }
        }

        /// <summary>
        /// Get/Set Virtual Path. It is used to set correct image source files
        /// </summary>
        public string VirtualPath
        {
            get { return m_virtualPath; }
            set { m_virtualPath = value; }
        }

        public int TemplateID
        {
            get { return m_templateID; }
            set { m_templateID = value; }
        }

        /// <summary>
        /// Text of a requested template for include
        /// </summary>
        public string TextForInclude
        {
            get { return m_textForInclude; }
            set { m_textForInclude = value; }
        }

        public int IDForInclude
        {
            get { return m_idForInclude; }
            set { m_idForInclude = value; }
        }

        /// <summary>
        /// List of previous included templates, used to check for circular inclusions
        /// </summary>
        public ArrayList PreviousTemplateIDs
        {
            get { return m_previousTemplateIDs; }
            set { m_previousTemplateIDs = value; }
        }

        /// <summary>
        /// Additional parameters passed to the parser when it requests the text of a template to include
        /// </summary>
        public Hashtable AdditionalIncludeParams
        {
            get { return m_additionalIncludeParams; }
            set { m_additionalIncludeParams = value; }
        }

        /// <summary>
        /// Default values for parameters
        /// </summary>
        public Hashtable ParamDefaults
        {
            get { return m_paramDefaults; }
            set { m_paramDefaults = value; }
        }


        /// <summary>
        /// Get/Set the XProviderData object used for parsing. This property is also used to execute commands
        /// </summary>
        public XProviderData DataProvider
        {
            get { return m_dataProvider; }
            set { m_dataProvider = value; }
        }

        ///// <summary>
        ///// When parsing for redirect URLs, the PAR command will apply by default URL encoding unless otherwise specified
        ///// </summary>
        //public bool ParseForRedirect
        //{
        //    get { return m_parseForRedirect; }
        //    set { m_parseForRedirect = value; }
        //}

        #endregion Public properties

        public int GetLine(int index)
        {
            if (m_originalText == null || m_originalText == "")
                return -1;

            int io = 0;
            int ret = 0;

            int originalIndex = m_mappings.GetOriginalIndex(index);

            while (io != -1 && io < index)
            {
                ret++;
                io = m_originalText.IndexOf(Environment.NewLine, io + 1);
            }

            if (ret > 0)
                ret--;

            return ret;
        }

        public string Parse()
        {
            IndexMappings mappings;
            string temp = IndexMappings.StripDocument(m_text, out mappings);
            //m_text = temp;
            m_originalText = m_text;
            m_text = temp;
            return Parse(mappings);
        }

        private string Parse(IndexMappings mappings)
        {
            
            CCmd root = new CCmd();
            root.Type = CommandType.GenericCommand;
            m_stack.Clear();
            m_stack.Push(root);

            if (mappings == null)
                mappings = new IndexMappings();

            m_mappings = mappings;

            MatchCollection mc = generalFieldREx.Matches(m_text);
            foreach (Match m in mc)
            {
                CCmd cmd = GetNewCmd(m.Value, m.Index, true, false);
                if (cmd != null)
                {
                    if (cmd.IsBlockComand)
                    {
                        if (cmd.Type == CommandType.ElseIFCommand)
                        {
                            CCmd cmdIf = (CCmd)m_stack.Pop();
                            if (!(cmdIf is CIFCmd))
                                throw new ApplicationException("ELSEIF command at line " + GetLine(m.Index).ToString() + " does not have a correspondig IF command associated.");
                            else
                            {
                                if (cmdIf.Type == CommandType.IFCommand)
                                    ((CCmd)m_stack.Peek()).ChildCommands.Add(cmdIf);
                            }
                            ((CIFCmd)cmdIf).ElseIfCommand = (CIFCmd)cmd;

                        }
                        m_stack.Push(cmd);
                    }
                    else
                    {
                        cmd.EndIndex = m.Index + m.Value.Length;
                        ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
                    }
                }
                else // this means we're dealing with a closing #*# tag
                {
                    bool bCloseOK = VerifyClosingTag(m);
                    if (bCloseOK)
                    {
                        CCmd cmdOK = (CCmd)m_stack.Pop();
                        if (m_stack.Count == 0)
                            throw new ApplicationException("Empty stack!");
                        cmdOK.EndIndex = m.Index + m.Value.Length;
                        if (cmdOK.Type != CommandType.ElseIFCommand)
                            ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmdOK);
                    }
                    else
                        throw new ApplicationException("The closing tag " + m.Value + " at line " + GetLine(m.Index).ToString() + " is not valid. Check the command it was intended for!");
                }
            }

            if (m_stack.Count > 1)
                foreach (CCmd stackCommand in m_stack)
                    ValidateCommandTree(stackCommand);
            string content = root.Execute();
            return content;
        }


        /// <summary>
        /// Encodes or decodes a string by meaning of URL and HTML integrity
        /// </summary>
        /// <param name="s">String to encode or decode</param>
        /// <param name="encOption">Option to encode or decode</param>
        /// <returns></returns>
        public static string Encode(string s, EncodeOption encOption)
        {
            string retVal = s;

            switch (encOption)
            {
                case EncodeOption.Encode:
                    retVal = HttpUtility.HtmlEncode(HttpUtility.UrlEncode(retVal));
                    break;
                case EncodeOption.Decode:
                    retVal = HttpUtility.UrlDecode(HttpUtility.HtmlDecode(retVal));
                    break;
                case EncodeOption.HTMLDecode:
                    retVal = HttpUtility.HtmlDecode(retVal);
                    break;
                case EncodeOption.HTMLEncode:
                    retVal = HttpUtility.HtmlEncode(retVal);
                    break;
                case EncodeOption.URLDecode:
                    retVal = HttpUtility.UrlDecode(retVal);
                    break;
                case EncodeOption.URLEncode:
                    retVal = HttpUtility.UrlEncode(retVal);
                    break;
                case EncodeOption.None:
                    break;
                default:
                    break;
            }

            return retVal;
        }


        #endregion


        #region Private Methods


        CCmd GetNewCmd(string txt, int initialIndex, bool acceptClosingBlockCommands, bool forceText)
        {
            bool removedSeparator = false;
            if (txt.StartsWith("#") && txt.EndsWith("#"))
            {
                txt = txt.Substring(SEPARATOR_LENGTH, txt.Length - 2 * SEPARATOR_LENGTH);
                removedSeparator = true;
            }

            CCmd cmd = null;

            try
            {

                    if (txt.ToUpper().StartsWith("REP."))
                    {
                        cmd = GetREPCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("COUNT."))
                    {
                        cmd = GetCOUNTCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("QRY."))
                    {
                        cmd = GetQRYCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("PAR."))
                    {
                        cmd = GetPARCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("FLD."))
                    {
                        cmd = GetFLDCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("FLDBIN."))
                    {
                        cmd = GetFLDBINCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("ATTACH."))
                    {
                        cmd = GetAttachCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("INCLUDE") || txt.ToUpper().StartsWith("UDF"))
                    {
                        cmd = GetIncludeCommand(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("IF") || txt.ToUpper().StartsWith("ELSEIF"))
                    {
                        // because of a limitation of generalFieldREx which does not report the starting "#" 
                        //when the IF condition contains brackets (other than the start and end brackets)
                        int ifIndex = removedSeparator ? initialIndex : initialIndex - 1;

                        cmd = GetIFCommand(txt, ifIndex);
                    }
                    else if (txt.ToUpper().StartsWith("DEFPAR"))
                    {
                        cmd = GetDefParCmd(txt, initialIndex);
                    }
                    else if (cmd == null && txt.IndexOf("#") == -1 && !txt.ToUpper().StartsWith("ENDREP")
                        && !txt.ToUpper().StartsWith("ENDQRY") && !txt.ToUpper().StartsWith("ENDIF"))
                    {
                        cmd = new CTextCmd();
                        if (forceText)
                        {
                            //if (txt.ToUpper() == END_BLOCK)
                            //    txt = "#" + END_BLOCK + "#";
                            (cmd as CTextCmd).ForceText(txt);
                        }
                    }



                if (cmd != null)
                {
                    cmd.StartIndex = initialIndex;
                    cmd.Parser = this;
                }
                else if (!acceptClosingBlockCommands)
                    throw new Exception(" ");

            }
            catch (Exception ex)
            {
                string message = string.Format("Could not create new command from text {0} at line {1}", txt, GetLine(initialIndex)) + Environment.NewLine + ex.Message;
                throw new ApplicationException(message);
            }
            return cmd;
        }


        private CCmd GetDefParCmd(string txt, int initialIndex)
        {
            string[] parts = txt.Split('.');
            if (parts.Length < 2)
                throw new Exception(string.Format("The DefPar command at line {0} is not valid", GetLine(initialIndex)));

            string parameterName = parts[1];
            string val = "";
            for (int i = 2; i < parts.Length; i++)
                val += parts[i] + ".";
            if (val.EndsWith("."))
                val = val.Substring(0, val.Length - 1);
            if (val.StartsWith("'"))
                val = val.Substring(1);
            if (val.EndsWith("'"))
                val = val.Substring(0, val.Length - 1);

            m_paramDefaults[parameterName] = val;

            CDefParCmd cmd = new CDefParCmd();
            return cmd;

        }

        CCmd GetAttachCmd(string text, int initialIndex)
        {
            CAttachCmd cmd = new CAttachCmd();

            //consider the text to the first '(' for splitting
            int ioBrOpen = text.IndexOf("(");
            string tempText = text;
            if (ioBrOpen != -1)
                tempText = text.Substring(0, ioBrOpen);
            List<ParseUtils.ListParameter> parts = ParseUtils.GetParameterList(tempText, '\'', '.', false);

            string attType = parts[1].Text;
            if (attType.ToUpper() == "DB")
                cmd.AttachType = AttachType.Database;
            else if (attType.ToUpper() == "FILE")
                cmd.AttachType = AttachType.File;
            else
                throw new ApplicationException("Command " + text + " at line " + GetLine(initialIndex).ToString() + " does not have an attachment type specified.");

            cmd.Extension = parts[2].Text;
            if (cmd.AttachType == AttachType.Database)
            {
                cmd.QueryName = parts[3].Text;
                cmd.FieldName = parts[4].Text;
                cmd.Context = m_context;
                cmd.Evaluator = m_evaluator;
                cmd.Parent = GetParentQueryCmd(cmd.QueryName);
                if (cmd.Parent == null)
                    throw new ApplicationException("Command " + text + " at line " + GetLine(initialIndex).ToString() + " does not have a parent REP or QRY for query " + cmd.QueryName);
            }
            else
            {
                int ioBrClose = text.IndexOf(")", ioBrOpen + 1);
                string textToLookUp = text.Substring(ioBrOpen, ioBrClose - ioBrOpen + 1);
                int startIndex = initialIndex + ioBrOpen + 1; // 1 because we exclude the bracket
                textToLookUp = textToLookUp.Replace("(", "").Replace(")", "");
                if (textToLookUp.IndexOf("#") == -1)
                    cmd.FileName = textToLookUp;
                else // we know we have a non-block command
                {
                    CCmd childCmd = GetNewCmd(textToLookUp, startIndex, false, true);
                    if (childCmd != null)
                        cmd.ChildCommands.Add(childCmd);
                }
            }

            return (CCmd)cmd;

        }

        CCmd GetIFCommand(string txt, int initialIndex)
        {

            CCmd cmd = new CIFCmd();
            int ioOpenBr = txt.IndexOf('(');

            string errorBr = "IF command " + txt + " at line " + GetLine(initialIndex).ToString() + " does not contain valid brackets.";
            if (ioOpenBr == -1)
                throw new ApplicationException(errorBr);
            int ioCloseBr = txt.LastIndexOf(')');
            if (ioCloseBr == -1)
                throw new ApplicationException(errorBr);
            string condition = "";
            int conditionLength = ioCloseBr - ioOpenBr - 1;
            if (conditionLength >= 0)
                condition = txt.Substring(ioOpenBr + 1, conditionLength);
            int offset = initialIndex + ioOpenBr + 1 + SEPARATOR_LENGTH;// 1 because we step over the opening bracket

            MatchCollection mcParams = ifParamREx.Matches(condition);

            foreach (Match m in mcParams)
            {
                int nNewIndex = offset + m.Index;
                CCmd condCmd = GetNewCmd(m.Value, nNewIndex, false, true);
                condCmd.EndIndex = offset + m.Index + m.Value.Length;
                ((CIFCmd)cmd).ConditionCmds.Add(condCmd);
            }

            if (txt.ToUpper().StartsWith("IF"))
                ((CIFCmd)cmd).Type = CommandType.IFCommand;
            else
                ((CIFCmd)cmd).Type = CommandType.ElseIFCommand;

            return cmd;
        }


        CCmd GetREPCmd(string txt, int initialIndex)
        {
            CCmd cmd = new CREPCmd();

            Match mTemp = wordREx.Match(txt.Substring(4));
            if (mTemp.Success)
                ((CREPCmd)cmd).QueryName = mTemp.Value;

            string paging = "";
            int pagingInitialIndex = -1;

            string paramString = "";
            int paramStringInitialIndex = -1;

            int io1 = txt.IndexOf("(");
            int io2 = txt.IndexOf(")");
            
            if (io1 > 0 && io2 != -1 && io2 > io1)
            {
                string list = txt.Substring(io1 + 1, io2 - io1 - 1);
                int index = initialIndex + SEPARATOR_LENGTH + io1 + 1;
                if (txt[io1 - 1] == '.')
                {
                    paging = list;
                    pagingInitialIndex = index;
                }
                else
                {
                    paramString = list;
                    paramStringInitialIndex = index;
                    int io21 = txt.IndexOf("(", io2 + 1);
                    if (io21 != -1)
                    {
                        int io22 = txt.IndexOf(")", io21 + 1);
                        if (io22 != -1)
                        {
                            paging = txt.Substring(io21 + 1, io22 - io21 - 1);
                            pagingInitialIndex = initialIndex + SEPARATOR_LENGTH + io21 + 1;
                        }
                        else
                            throw new ApplicationException("The REP command at line " + GetLine(initialIndex).ToString() + " is missing a bracket");
                    }
                }
            }

            if (paging != "")
            {
                List<ParseUtils.ListParameter> parts = ParseUtils.GetParameterList(paging, '\'', ',', false);

                if (parts.Count == 2)
                {
                    int pageSizeCmdIndex = pagingInitialIndex + parts[0].Index;
                    int pageNumberCmdIndex = pagingInitialIndex + parts[1].Index;
                    CCmd pageSizeCmd = GetNewCmd(parts[0].Text, pageSizeCmdIndex, false, false);
                    CCmd pageNumberCmd = GetNewCmd(parts[1].Text, pageNumberCmdIndex, false, false);
                    pageSizeCmd.EndIndex = pageSizeCmdIndex + parts[0].Text.Length;
                    pageNumberCmd.EndIndex = pageNumberCmdIndex + parts[1].Text.Length;
                    ((CREPCmd)cmd).PageNumberCmd = pageNumberCmd;
                    ((CREPCmd)cmd).PageSizeCmd = pageSizeCmd;
                }
            }

            if (paramString != "")
            {
                ArrayList arrParams = GetParserParameters(paramString);
                ((CREPCmd)cmd).Parameters = new CCmd[arrParams.Count];

                for (int iParam = 0; iParam < arrParams.Count; iParam++)
                {
                    ParserParam pp = (ParserParam)arrParams[iParam];
                    ((CREPCmd)cmd).Parameters[iParam] = GetNewCmd(pp.Value.Trim(), paramStringInitialIndex + pp.Index, false, true);
                    ((CREPCmd)cmd).Parameters[iParam].EndIndex = paramStringInitialIndex + pp.Index + pp.Value.Length;
                }
            }
            else
                ((CREPCmd)cmd).Parameters = new CCmd[0];
            ((CREPCmd)cmd).Context = m_context;
            ((CREPCmd)cmd).Evaluator = m_evaluator;

            return cmd;
        }

        CCmd GetCOUNTCmd(string txt, int initialIndex)
        {
            CCmd cmd = new CCountCmd();

            Match mTemp = wordREx.Match(txt.Substring(6));
            if (mTemp.Success)
                ((CCountCmd)cmd).QueryName = mTemp.Value;

            string paging = "";
            int pagingInitialIndex = -1;

            string paramString = "";
            int paramStringInitialIndex = -1;

            int io1 = txt.IndexOf("(");
            int io2 = txt.IndexOf(")");

            if (io1 > 0 && io2 != -1 && io2 > io1)
            {
                string list = txt.Substring(io1 + 1, io2 - io1 - 1);
                int index = initialIndex + SEPARATOR_LENGTH + io1 + 1;
                if (txt[io1 - 1] == '.')
                {
                    paging = list;
                    pagingInitialIndex = index;
                }
                else
                {
                    paramString = list;
                    paramStringInitialIndex = index;
                    int io21 = txt.IndexOf("(", io2 + 1);
                    if (io21 != -1)
                    {
                        int io22 = txt.IndexOf(")", io21 + 1);
                        if (io22 != -1)
                        {
                            paging = txt.Substring(io21 + 1, io22 - io21 - 1);
                            pagingInitialIndex = initialIndex + SEPARATOR_LENGTH + io21 + 1;
                        }
                        else
                            throw new ApplicationException("The COUNT command at line " + GetLine(initialIndex).ToString() + " is missing a bracket");
                    }
                }
            }

            if (paging != "")
            {
                int pageSizeCmdIndex = pagingInitialIndex;
                CCmd pageSizeCmd = GetNewCmd(paging, pageSizeCmdIndex, false, false);
                pageSizeCmd.EndIndex = pageSizeCmdIndex + paging.Length;
                ((CCountCmd)cmd).PageSizeCmd = pageSizeCmd;
            }

            if (paramString != "")
            {
                ArrayList arrParams = GetParserParameters(paramString);
                ((CCountCmd)cmd).Parameters = new CCmd[arrParams.Count];

                for (int iParam = 0; iParam < arrParams.Count; iParam++)
                {
                    ParserParam pp = (ParserParam)arrParams[iParam];
                    ((CCountCmd)cmd).Parameters[iParam] = GetNewCmd(pp.Value.Trim(), paramStringInitialIndex + pp.Index, false, true);
                    ((CCountCmd)cmd).Parameters[iParam].EndIndex = paramStringInitialIndex + pp.Index + pp.Value.Length;
                }
            }
            else
                ((CCountCmd)cmd).Parameters = new CCmd[0];
            ((CCountCmd)cmd).Context = m_context;
            ((CCountCmd)cmd).Evaluator = m_evaluator;

            return cmd;
        }

        CCmd GetQRYCmd(string txt, int initialIndex)
        {
            CCmd cmd = new CQRYCmd();

            Match mTemp = wordREx.Match(txt.Substring(4));
            if (mTemp.Success)
                ((CQRYCmd)cmd).QueryName = mTemp.Value;
            string prametersString = txt.Substring(4 + mTemp.Value.Length);
            int offset = initialIndex + 4 + mTemp.Value.Length + SEPARATOR_LENGTH;

            ArrayList arrParams = GetParserParameters(prametersString);
            ((CQRYCmd)cmd).Parameters = new CCmd[arrParams.Count];


            for (int iParam = 0; iParam < arrParams.Count; iParam++)
            {
                ParserParam pp = (ParserParam)arrParams[iParam];
                ((CQRYCmd)cmd).Parameters[iParam] = GetNewCmd(pp.Value.Trim(), offset + pp.Index, false, true);
                ((CQRYCmd)cmd).Parameters[iParam].EndIndex = offset + pp.Index + pp.Value.Length;
            }
            ((CQRYCmd)cmd).Context = m_context;
            ((CQRYCmd)cmd).Evaluator = m_evaluator;

            return cmd;
        }


        CCmd GetPARCmd(string txt)
        {
            CCmd cmd = new CPARCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            ((CPARCmd)cmd).ParameterName = parts[1];
            if (parts.GetLength(0) >= 3)
            {
                EncodeOption encOpt = GetEncodeOption(parts[2]);
                bool requestEncode = encOpt != EncodeOption.None;

                ((CPARCmd)cmd).EncodeOption = encOpt;

                if (encOpt == EncodeOption.None)
                    ((CPARCmd)cmd).Format = parts[2];
            }

            //if (((CPARCmd)cmd).EncodeOption == EncodeOption.None && m_parseForRedirect)
            //((CPARCmd)cmd).EncodeOption = EncodeOption.URLEncode;
            return cmd;
        }


        CCmd GetFLDCmd(string txt)
        {
            CCmd cmd = new CFLDCmd();

            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            if (parts.Length >= 3)
            {
                ((CFLDCmd)cmd).QueryName = parts[1];
                if (parts[2].Trim().ToUpper() == "FETCHID")
                    ((CFLDCmd)cmd).IsFetchID = true;
                else
                {
                    if (parts[2].Trim().ToUpper() == "ODDEVEN")
                        ((CFLDCmd)cmd).OddEven = true;
                    else
                    {
                        ((CFLDCmd)cmd).FieldName = parts[2];
                        if (parts.Length >= 4)
                        {
                            EncodeOption encOpt = GetEncodeOption(parts[3]);
                            bool requestEncode = encOpt != EncodeOption.None;
                            ((CFLDCmd)cmd).EncodeOption = encOpt;
                            if (encOpt == EncodeOption.None)
                                ((CFLDCmd)cmd).Format = parts[3];
                        }
                    }
                }
                ((CFLDCmd)cmd).Context = m_context;
                ((CFLDCmd)cmd).Evaluator = m_evaluator;
                ((CFLDCmd)cmd).Parent = GetParentQueryCmd(((CFLDCmd)cmd).QueryName);
                if (((CFLDCmd)cmd).Parent == null)
                    throw new ApplicationException("Command " + txt + " does not have a parent QRY or REP for query " + ((CFLDCmd)cmd).QueryName);
            }

            //if (((CFLDCmd)cmd).EncodeOption == EncodeOption.None && m_parseForRedirect)
            //((CFLDCmd)cmd).EncodeOption = EncodeOption.URLEncode;


            return cmd;

        }

        CCmd GetFLDBINCmd(string txt)
        {
            CCmd cmd = new CFLDCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            if (parts.Length == 3)
            {
                ((CFLDCmd)cmd).QueryName = parts[1];
                ((CFLDCmd)cmd).FieldName = parts[2];
                ((CFLDCmd)cmd).Context = m_context;
                ((CFLDCmd)cmd).Evaluator = m_evaluator;
                ((CFLDCmd)cmd).Parent = GetParentQueryCmd(((CFLDCmd)cmd).QueryName);
                if (((CFLDCmd)cmd).Parent == null)
                    throw new ApplicationException("Command " + txt + " does not have a parent QRY or REP for query " + ((CFLDCmd)cmd).QueryName);

                ((CFLDCmd)cmd).BinaryField = true;
            }

            return cmd;
        }

        CCmd GetIncludeCommand(string txt, int initialIndex)
        {
            CCmd cmd = new CIncludeCmd();
            if (txt.ToUpper().StartsWith("INCLUDE"))
            {
                string paramList = txt.Substring(txt.IndexOf('(') + 1);
                int lio = paramList.LastIndexOf(')');
                paramList = paramList.Remove(lio);
                int offset = initialIndex + txt.IndexOf('(') + 2;// 1 pt ca trec de paranteza.

                MatchCollection mcParams = ifParamREx.Matches(paramList);

                foreach (Match m in mcParams)
                {
                    int nNewIndex = offset + m.Index;
                    CCmd paramCmd = GetNewCmd(m.Value, nNewIndex, false, true);
                    paramCmd.EndIndex = offset + m.Index + m.Value.Length;
                    ((CIncludeCmd)cmd).ParamCmds.Add(paramCmd);
                }
            }
            else //UDF command
            {
                int ioBr1 = txt.IndexOf("(");
                int ioBr2 = txt.LastIndexOf(")");
                if (ioBr1 == -1 || ioBr2 == -1)
                    throw new Exception(string.Format("The UDF command at line {0} is missing one or both brackets", GetLine(initialIndex)));
                string firstPart = txt.Substring(0, ioBr1);
                string templateName = firstPart.Replace("UDF.", "");

                string paramList = txt.Substring(ioBr1 + 1, ioBr2 - ioBr1 - 1);

                int offset = initialIndex + ioBr1 + 2;

                MatchCollection mcParams = ifParamREx.Matches(paramList);

                foreach (Match m in mcParams)
                {
                    int nNewIndex = offset + m.Index;
                    CCmd paramCmd = GetNewCmd(m.Value, nNewIndex, false, true);
                    paramCmd.EndIndex = offset + m.Index + m.Value.Length;
                    ((CIncludeCmd)cmd).ParamCmds.Add(paramCmd);
                }

                (cmd as CIncludeCmd).IsUDF = true;
                (cmd as CIncludeCmd).UDFTemplateName = templateName;
            }

            return cmd;
        }


        bool VerifyClosingTag(Match m)
        {
            CCmd comm = (CCmd)m_stack.Peek();
            string tag = m.Value.Replace("#", "");
            if (tag.ToUpper() == "ENDIF" && !(comm is CIFCmd))
                return false;
            if (tag.ToUpper().StartsWith("ENDQRY."))
            {
                if (!(comm is CQRYCmd))
                    return false;
                string queryName = tag.Substring(tag.IndexOf(".") + 1);
                if (((CQRYCmd)comm).QueryName != queryName)
                    return false;
            }

            if (tag.ToUpper().StartsWith("ENDREP."))
            {
                if (!(comm is CREPCmd))
                    return false;
                string queryName = tag.Substring(tag.IndexOf(".") + 1);
                if (((CREPCmd)comm).QueryName != queryName)
                    return false;
            }
            return true;
        }


        CCmd GetParentQueryCmd(string queryName)
        {
            object[] arrStack = m_stack.ToArray();
            for (int i = arrStack.GetLength(0) - 1; i >= 0; i--)
            {
                if (arrStack[i] is CQRYCmd && ((CQRYCmd)arrStack[i]).QueryName == queryName)
                    return (CCmd)arrStack[i];
                else if (arrStack[i] is CREPCmd && ((CREPCmd)arrStack[i]).QueryName == queryName)
                    return (CCmd)arrStack[i];
            }
            return null;
        }

        ArrayList GetParserParameters(string text)
        {
            int offset = 0;
            int io = text.IndexOf("(");
            if (io != -1)
            {
                //text = text.Replace("(","").Replace(")","");
                offset += text.IndexOf("(") + 1; // we removed the opening bracket
                int lio = text.LastIndexOf(")");
                if (lio != -1)
                {
                    if (io + 1 > lio - io - 1)
                        text = "";
                    else
                        text = text.Substring(io + 1, lio - io - 1);
                }
                else throw new ApplicationException("No closing bracket has been found while parsing command parameters: " + text);
            }

            ArrayList arrRet = new ArrayList();

            List<ParseUtils.ListParameter> parameters = ParseUtils.GetParameterList(text, '\'', ',', false);
            foreach (ParseUtils.ListParameter param in parameters)
            {
                ParserParam pp;
                pp.Index = param.Index;
                pp.Value = param.Text;//.Trim();
                arrRet.Add(pp);
                //offset += param.Length + 1; // +1 because we step over the comma
            }

            return arrRet;
        }

        /// <summary>
        /// Identifies an encode option from a keyword
        /// </summary>
        /// <param name="s">The keyword found in the command syntax</param>
        /// <returns></returns>
        EncodeOption GetEncodeOption(string s)
        {
            EncodeOption encOpt = EncodeOption.None;
            foreach (string enumName in Enum.GetNames(typeof(EncodeOption)))
            {
                if (s.ToUpper().Trim() == enumName.ToUpper())
                {
                    encOpt = (EncodeOption)Enum.Parse(typeof(EncodeOption), enumName, false);
                    break;
                }
            }
            return encOpt;
        }

        private void ValidateCommandTree(CCmd root)
        {
            if (root == null)
                return;
            if (root.EndIndex == -1 && root.Type != CommandType.GenericCommand && root.IsBlockComand)
                if (!(root is CIFCmd && (root as CIFCmd).ElseIfCommand != null))
                    throw new ApplicationException("The command " + root.Type.ToString() + " at line " + GetLine(root.StartIndex).ToString() + " does not have a valid closing tag!");
            if (root.Type == CommandType.IFCommand)
                ValidateCommandTree((root as CIFCmd).ElseIfCommand);
            foreach (CCmd command in root.ChildCommands)
                ValidateCommandTree(command);

        }

        internal void RequestTemplate(int templateID)
        {
            if (RequestTemplateText != null)
                RequestTemplateText(this, templateID);
        }

        internal void RequestTemplateIDForInclude(string templateName)
        {
            if (RequestTemplateID != null)
                RequestTemplateID(this, templateName);
        }




        #endregion

        public void RaiseRequestHierarchy(SourceResult sourceResult, string parameters)
        {
            if (RequestHierarchy != null)
            {
                RequestHierarchy(sourceResult, parameters);
            }
        }

    }

}
