using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using XDataSourceModule;
using System.IO;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Web;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Text;
using XDocuments;
using System.Globalization;
using KubionLogNamespace;


namespace XHTMLMerge
{
    public class CParser
    {


        public delegate void RequestTemplateText_Handler(CParser sender, int templateID);
        public event RequestTemplateText_Handler RequestTemplateText;

        public delegate void RequestTemplateID_Handler(CParser sender, string templateName);
        public event RequestTemplateID_Handler RequestTemplateID;

        public delegate void LogInclude_Handler(CParser sender, LogIncludeEventArgs e);
        public event LogInclude_Handler LogInclude;

        public event SourceResult.RequestHierarcyEventHandler RequestHierarchy;

        #region Protected Members
        IXManagerImportExport m_manager = null;  
        Stack m_stack = null;
        string
            m_outputPath = "",
            m_virtualPath = "",
            m_textForInclude = "";

        //StringBuilder m_text = null;
        //StringBuilder m_partialText = null;
        string  m_text = null;
        string m_partialText = null;

        CContext m_context = null;
        IXDataSource m_evaluator = null;
        XProviderData m_dataProvider = null;
        int m_templateID, m_idForInclude = -1;
        string m_templateName = "";
        int matchIndex = -1;
        int m_partialTextOffset = 0;
        int m_includeLogIndentation = 1;
        public bool m_UseParameters = false;


        bool
            m_skipCurrentMatch = false,
            m_removeNewLine = false;
        bool m_removeTab = false;


        MatchCollection mc = null;

        bool m_commentBlock = false;
        bool
            m_ignoreBlock = false
            //,
            //m_parseForRedirect = false
            ;

        ArrayList m_previousTemplateIDs = new ArrayList();

        Hashtable m_additionalIncludeParams = CollectionsUtil.CreateCaseInsensitiveHashtable();

        Hashtable m_queries = CollectionsUtil.CreateCaseInsensitiveHashtable();
        Hashtable m_paramDefaults = CollectionsUtil.CreateCaseInsensitiveHashtable();
        Hashtable m_globalParams = CollectionsUtil.CreateCaseInsensitiveHashtable();
        Hashtable m_templateParams = CollectionsUtil.CreateCaseInsensitiveHashtable();
        //Hashtable m_SessionVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
        //Hashtable m_SessionVariablesUpdates = CollectionsUtil.CreateCaseInsensitiveHashtable();
        //Hashtable m_ConfigVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
        Hashtable m_MacroTemplates = CollectionsUtil.CreateCaseInsensitiveHashtable();
        const int SEPARATOR_LENGTH = 1;

        internal static string UserIDKey
        {
            get { return "USERID"; }
        }
        internal static string UserLoginKey
        {
            get { return "USERLOGIN"; }
        }
        internal static string UserNameKey
        {
            get { return "USERNAME"; }
        }
        internal static string ServerKey
        {
            get { return "SERVER"; }
        }
        internal static string DatabaseKey
        {
            get { return "DATABASE"; }
        }

        Regex
            wordREx = new Regex(@"(\w)+"),
            ifParamREx = new Regex(@"(#[^#]+#)|[^#]+"),
//good            generalFieldREx = new Regex(@"(#((GETPAR)|(SETPAR)|(SETFLD)|(SETVAL)|(COUNT)|(UDF)|(DEFPAR)|(STARTBLOCK)|(ENDBLOCK)|(XPATH)|(REP)|(QRY)|(IF)|(ELSEIF)|(PAR)|(FLD)|(FLDBIN)|(ATTACH)|(ENDXPATH)|(ENDREP)|(ENDIF)|(ENDQRY)|(INCLUDE)|(INCLUDES))[^#\x28]*(\x28[^\x29]*\x29)?(.\x28[^\x29]*\x29)?#)|[^#]+", RegexOptions.IgnoreCase);
//            generalFieldREx = new Regex(@"(#((GETPAR)|(SETPAR)|(SETFLD)|(SETVAL)|(COUNT)|(UDF)|(DEFPAR)|(STARTBLOCK)|(ENDBLOCK)|(STARTCOMMENT)|(ENDCOMMENT)|(XPATH)|(TXPATH)|(REP)|(QRY)|(IF)|(ELSEIF)|(PAR)|(FLD)|(FLDBIN)|(ATTACH)|(ENDXPATH)|(ENDREP)|(ENDIF)|(ENDQRY)|(INCLUDE)|(IMPORT)|(EXPORT)|(TRANSFER)|(CFG)|(USEPARAMETERS))[^#\x28]*(\x28[^\x29]*\x29)?(.\x28[^\x29]*\x29)?#)|[^#]+", RegexOptions.IgnoreCase);
            generalFieldREx = new Regex(@"(#((SPARP)|(SPARA)|(SPAR)|(JINS)|(JAPP)|(JUPD)|(JDEL)|(JPAR)|(JPAR)|(JDATA)|(JDATA)|(JLOOP)|(ENDJLOOP)|(JPROP)|(ENDJPROP)|(ADDPAR)|(GETPARALL)|(GETPARSTRICT)|(GETPAR)|(GETCFGPAR)|(GETPARREPLACE)|(GETDEFPAR)|(GETITEMPAR)|(FINDITEMPAR)|(APPVAL)|(APPENDVAL)|(APPFLD)|(APPENDFLD)|(DEL)|(DELFROM)|(DELALL)|(DELITEM)|(SETPAR)|(SETPAREXT)|(SETFLD)|(SETFLDEXT)|(SETVAL)|(SETNULLPAR)|(SETNULLPAREXT)|(SETNULLFLD)|(SETNULLFLDEXT)|(SETNULLVAL)|(MSG)|(THROW)|(THROWPAR)|(FILE)|(COUNT)|(UDF)|(DEFPAR)|(DEFQRY)|(STARTBLOCK)|(ENDBLOCK)|(STARTCOMMENT)|(ENDCOMMENT)|(XPATH)|(XPATHDATA)|(TXPATH)|(TXPATHDATA)|(JPATH)|(JPATHDATA)|(REP)|(REPDATA)|(QRY)|(QRYDATA)|(IF)|(ELSEIF)|(PAR)|(FLD)|(FLDBIN)|(ATTACH)|(ENDXPATH)|(ENDJPATH)|(ENDREP)|(ENDIF)|(ENDQRY)|(INCLUDEONCE)|(PARINCLUDE)|(INCLUDE)|(IMPORT)|(IMPORTFLD)|(EXPORT)|(DELETETEMPLATE)|(TRANSFERLIKE)|(TRANSFER)|(CFG)|(USEPARAMETERS)|(FUNC))[^#\x28]*(\x28[^\x29]*\x29)?(.\x28[^\x29]*\x29)?#)|[^#]+", RegexOptions.IgnoreCase);
//            generalFieldREx = new Regex(@"(#((GETPAR)|(SETPAR)|(COUNT)|(UDF)|(DEFPAR)|(STARTBLOCK)|(ENDBLOCK)|(XPATH)|(REP)|(QRY)|(IF)|(ELSEIF)|(PAR)|(FLD)|(FLDBIN)|(ATTACH)|(ENDXPATH)|(ENDREP)|(ENDIF)|(ENDQRY)|(INCLUDE)|(INCLUDES))[^#\x28]*(\x28[^\x29]*\x29)?(.\x28[^\x29]*\x29)?#)|[^#]+|#[^#]+#|", RegexOptions.IgnoreCase);

        const string
            START_BLOCK = "STARTBLOCK",
            END_BLOCK = "ENDBLOCK",
            START_COMMENT = "STARTCOMMENT",
            END_COMMENT = "ENDCOMMENT";
        //private const string
        //    Utils.CT_ENDCDATA = "}}>",
        //    Utils.CT_ENDCDATA2 = "}.}.>",
        //    Utils.CT_ENDCDATA3 = "}..}..>";

        struct ParserParam
        {
            public int Index;
            public string Value;
        }


        #endregion


        #region Public properties
        public CContext Context
        {
            get { return m_context; }
            set { m_context = value; }
        }

        public XDocCache DocCache
        {
            get { if (m_evaluator != null) return m_evaluator.DocCache;  else return null; }
            set { if (m_evaluator != null) m_evaluator.DocCache = value; }
        }
        public XDocCacheTemplates DocCacheTemplates
        {
            get { if (m_evaluator != null) return m_evaluator.DocCacheTemplates; else return null; }
            set { if (m_evaluator != null) m_evaluator.DocCacheTemplates = value; }
        }

        public IXManagerImportExport Manager
        {
            get { return m_manager; }
            set { m_manager = value; }
        }
       /// <summary>
        /// Amount of white spaces used to indent the inclusion calls in the include log file
        /// </summary>
        public int IncludeLogIndentation
        {
            get { return m_includeLogIndentation; }
            set { m_includeLogIndentation = value; }
        }

        /// <summary>
        /// If set to true, the parser removes the newline character from the start and end of text commands
        /// </summary>
        public bool RemoveNewLine
        {
            get { return m_removeNewLine; }
            set { m_removeNewLine = value; }
        }
        public bool RemoveTab
        {
            get { return m_removeTab; }
            set { m_removeTab = value; }
        }

        /// <summary>
        /// Set/Get template text that is used to generate document
        /// </summary>
        public string TemplateText
        {
            //get { return m_text == null ? "" : m_text.ToString(); }
            //set { m_text = new StringBuilder(value); }
            get { return m_text == null ? "" : m_text; }
            set { m_text = value; }
        }

        /// <summary>
        /// Get/Set an IXDataSource instance
        /// </summary>
        public IXDataSource Evaluator
        {
            get { return m_evaluator; }
            set { m_evaluator = value; }
        }

        /// <summary>
        /// Get/Set output path
        /// </summary>
        public string OutputPath
        {
            get { return m_outputPath; }
            set { m_outputPath = value; }
        }

        /// <summary>
        /// Get/Set Virtual Path. It is used to set correct image source files
        /// </summary>
        public string VirtualPath
        {
            get { return m_virtualPath; }
            set { m_virtualPath = value; }
        }

        public int TemplateID
        {
            get { return m_templateID; }
            set { m_templateID = value; }
        }

        /// <summary>
        /// Text of a requested template for include
        /// </summary>
        public string TextForInclude
        {
            get { return m_textForInclude; }
            set { m_textForInclude = value; }
        }

        public int IDForInclude
        {
            get { return m_idForInclude; }
            set { m_idForInclude = value; }
        }

        /// <summary>
        /// List of previous included templates, used to check for circular inclusions
        /// </summary>
        public ArrayList PreviousTemplateIDs
        {
            get { return m_previousTemplateIDs; }
            set { m_previousTemplateIDs = value; }
        }

        /// <summary>
        /// Additional parameters passed to the parser when it requests the text of a template to include
        /// </summary>
        public Hashtable AdditionalIncludeParams
        {
            get { return m_additionalIncludeParams; }
            set { m_additionalIncludeParams = value; }
        }

        /// <summary>
        /// Default values for parameters
        /// </summary>
        public Hashtable ParamDefaults
        {
            get { return m_paramDefaults; }
            set { m_paramDefaults = value; }
        }
        public Hashtable Queries
        {
            get { return m_queries; }
            set { m_queries = value; }
        }

        public Hashtable TemplateParams
        {
            get { return m_templateParams; }
            set { m_templateParams = value; }
        }

        public Hashtable GlobalParams
        {
            get { return m_globalParams ; }
            set { m_globalParams = value; }
        }

        //public Hashtable SessionVariables
        //{
        //    get { return m_SessionVariables; }
        //    set { m_SessionVariables = value; }
        //}
        //public Hashtable ConfigVariables
        //{
        //    get { return m_ConfigVariables; }
        //    set { m_ConfigVariables = value; }
        //}
        
        /// <summary>
        /// Get/Set the XProviderData object used for parsing. This property is also used to execute commands
        /// </summary>
        public XProviderData DataProvider
        {
            get { return m_dataProvider; }
            set { m_dataProvider = value; }
        }

        ///// <summary>
        ///// When parsing for redirect URLs, the PAR command will apply by default URL encoding unless otherwise specified
        ///// </summary>
        //public bool ParseForRedirect
        //{
        //    get { return m_parseForRedirect; }
        //    set { m_parseForRedirect = value; }
        //}
        
        private string sParserQueryLog = "";
        public string ParserQueryLog
        {
            get
            {
                if (m_evaluator != null) sParserQueryLog = m_evaluator.ParserQueryLog;
                if (sParserQueryLog == "")
                {
                    string strParserQueryLog = System.Configuration.ConfigurationManager.AppSettings["ParserQueryLog"];
                    if (strParserQueryLog != null)
                        sParserQueryLog = strParserQueryLog;
                    else sParserQueryLog = "0";
                }
                return sParserQueryLog;
            }
            set
            {
                sParserQueryLog = value;
                if (m_evaluator != null) m_evaluator.ParserQueryLog = sParserQueryLog;
            }
        }
        private string sParserTrace = "";
        public string ParserTrace
        {
            get
            {
                if (sParserTrace == "")
                {
                    if(m_evaluator != null) sParserTrace = m_evaluator.ParserTrace;
                    if (sParserTrace == "")
                    {
                        if (GetParameterValue("ParserTrace", "0") == "1") sParserTrace = "1";
                        else
                        {
//                            string sTraceUser = GetCfg("TraceUser", "nobody");
                            string sTraceUser = System.Configuration.ConfigurationManager.AppSettings["TraceUser"];
                            if (sTraceUser != null)
                                sTraceUser = "";

                            if ((sTraceUser == "all") || (sTraceUser == "*")) sParserTrace = "1";
                            else if (GetParameterValue("UserName", "") == sTraceUser) sParserTrace = "1";
                            //else if (GetSV("ParserTrace", "", "", "0") == "1") sParserTrace = "1";
                            else
                            {

                                string strParserTrace = System.Configuration.ConfigurationManager.AppSettings["ParserTrace"];
                                if (strParserTrace != null)
                                    sParserTrace = strParserTrace;
                                else sParserTrace = "0";
                            }
                            if (m_evaluator != null) m_evaluator.ParserTrace = sParserTrace;
                        }
                    }
                }
                return sParserTrace;
            }
            set
            {
                sParserTrace = value;
                if (m_evaluator != null) m_evaluator.ParserTrace = sParserTrace;
            }
        }

#endregion Public properties


        #region Public Methods
        public CParser(IXManagerImportExport manager, int templateID, IXDataSource evaluator, string text, Hashtable templateParams, string virtualPath, string outputPath)
        {
            m_manager = manager;
            m_stack = new Stack();
            //            m_text = new StringBuilder(text);
            m_text = text;
            m_context = new CContext();
            m_evaluator = evaluator;
            m_templateParams = templateParams;
            m_outputPath = outputPath;
            m_virtualPath = virtualPath;
            m_templateID = templateID;
            m_dataProvider = evaluator.DataProvider;
            if (templateParams != null && templateParams["RemoveNewLine"] != null)
            {
                if (templateParams["RemoveNewLine"].ToString().ToLower() == "true" ||
                    templateParams["RemoveNewLine"].ToString() == "1")
                    m_removeNewLine = true;
            }
            if (templateParams != null && templateParams["RemoveTab"] != null)
            {
                if (templateParams["RemoveTab"].ToString().ToLower() == "true" ||
                    templateParams["RemoveTab"].ToString() == "1")
                    m_removeTab= true;
            }
        }
        public CParser(IXManagerImportExport manager, int templateID, IXDataSource evaluator, string text, Hashtable templateParams)
            : this(manager, templateID, evaluator, text, templateParams, "", "")
        {
        }

        public CParser(IXManagerImportExport manager, int templateID, XProviderData dataProvider, string text, Hashtable templateParams, string virtualPath, string outputPath)
        {
            m_manager = manager;
            m_stack = new Stack();
//            m_text = new StringBuilder(text);
            m_text = text;
            m_context = new CContext();
            m_evaluator = new XDataSource();
            m_evaluator.InitializeEvaluator(templateID, dataProvider, templateParams);
            m_templateParams = templateParams;
            m_outputPath = outputPath;
            m_virtualPath = virtualPath;
            m_templateID = templateID;
            m_dataProvider = dataProvider;
            if (templateParams != null && templateParams["RemoveNewLine"] != null)
            {
                if (templateParams["RemoveNewLine"].ToString().ToLower() == "true" ||
                    templateParams["RemoveNewLine"].ToString() == "1")
                    m_removeNewLine = true;
            }
            if (templateParams != null && templateParams["RemoveTab"] != null)
            {
                if (templateParams["RemoveTab"].ToString().ToLower() == "true" ||
                    templateParams["RemoveTab"].ToString() == "1")
                    m_removeTab = true;
            }
        }

        public CParser(IXManagerImportExport manager, int templateID, XProviderData dataProvider, string text, Hashtable templateParams)
            : this(manager, templateID, dataProvider, text, templateParams, "", "")
        {
        }




        public int GetLine(int index)
        {
            //if (m_text == null || m_text.ToString() == "")
            //    return -1;
            if (m_text == null || m_text == "")
                return -1;

            int io = 0;
            int ret = 0;
            while (io != -1 && io < index)
            {
                ret++;
                //io = m_text.ToString().IndexOf(Environment.NewLine, io + 1);
                io = m_text.IndexOf(Environment.NewLine, io + 1);
            }

            if (ret > 0)
                ret--;

            return ret;
        }
 
        private string ReplaceMeta(string sText, string sMetaName, string sMetaContent, string sSep1, string sSep2)
        {

            //Regex regexMeta = new Regex(@"#META." + sMetaName + "((\x5B[0-9]*\x5D)?(\x5B[0-9]*\x5D)?)#", RegexOptions.IgnoreCase);
            Regex regexMeta = new Regex(@"#META." + sMetaName + "((\x5B[0-9]*\x5D)?(\x5B[0-9]*\x5D)?)(.(URLENCODE|URLDECODE|HTMLENCODE|HTMLDECODE|ENCODE|DECODE))?#", RegexOptions.IgnoreCase);
            MatchCollection mc = null;
            string sVal;
            string sIndex1, sIndex2;
            int index1, index2;
            string sMetaContent1;
            sMetaContent = sMetaContent.Replace("~", "<tilda>");
            sMetaContent = sMetaContent.Replace(sSep1, "~");
            string[] aMetaContent = sMetaContent.Split ('~');
            sMetaContent = sMetaContent.Replace("~", sSep1 );
            sMetaContent = sMetaContent.Replace("<tilda>", "~");

            mc = regexMeta.Matches(sText);
            for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
            {
                Match m = mc[matchIndex];
                if (m.Value.Contains ("][") )
                {
                    sIndex1 = m.Value.Substring (m.Value .IndexOf ("[")+1,m.Value .IndexOf ("]")-m.Value .IndexOf ("[")-1) ;
                    sIndex2 = m.Value.Substring (m.Value .IndexOf ("][")+2,m.Value.LastIndexOf  ("]")-m.Value .IndexOf ("][")-2) ;
                    int.TryParse (sIndex1 ,out index1 );
                    int.TryParse (sIndex2 , out index2 );
                    if (aMetaContent .Length > index1 )

                    {
                        sMetaContent1 = aMetaContent[index1].Replace("<tilda>", "~");
                        sMetaContent1 = sMetaContent1.Replace (sSep2 , "~");
                        string[] aaMetaContent = sMetaContent1.Split ('~' );
                    if (aaMetaContent .Length > index2 ) sVal= aaMetaContent [index2].Replace ("<tilda>","~");
                    else sVal ="";

                    }
                    else sVal = "";

                }
                else if (m.Value.Contains ("[") )
                {
                     sIndex1 = m.Value.Substring (m.Value .IndexOf ("[")+1,m.Value .IndexOf ("]")-m.Value .IndexOf ("[")-1) ;
                    int.TryParse (sIndex1 , out index1 );
                    if (aMetaContent .Length > index1 ) sVal= aMetaContent [index1].Replace ("<tilda>","~");
                    else sVal = "";

                }
                else 
                    sVal =sMetaContent ;
                string[] parts = m.Value.Split('.');
                if (parts.Length >= 3)
                {
                    EncodeOption encOpt = GetEncodeOption(parts[2].Replace ("#",""));
                    sVal = Utils.Encode(sVal, encOpt);
                }

                sText  = sText .Replace (m.Value , sVal );

            }
            return sText;
       }

        //int iInReplaceSettings = 0; 
        //private string ReplaceSettings(string sLine, string sContextName)
        //{
        //    if (iInReplaceSettings > 10) return sLine;
        //    iInReplaceSettings++;
        //    char cSep = '@';
        //    string sResponse = "";
        //    string[] aLine = sLine.Split(cSep);
        //    for (int i = 0; i < aLine.GetLength(0); i++)
        //    {
        //        sResponse += aLine[i];
        //        i++;
        //        if (i < aLine.GetLength(0))
        //        {
        //            string sVar = aLine[i];
        //            bool toEncode = false;
        //            if (sVar.ToLower().EndsWith("[encode]"))
        //            {
        //                sVar = sVar.Substring(0, sVar.Length - "[encode]".Length);
        //                toEncode = true;
        //            }
        //            string sVal = GetSV( sVar,  sContextName,"ID","missingvalue");
        //            //if (sVal == "missingvalue") sVal = GetCfg(sVar, "missingvalue");
        //            if (sVal == "missingvalue") sVal = GetSV(sVar, "", "ID", "missingvalue");
        //            if (sVal != "missingvalue")
        //            {
        //                if (toEncode) sVal = HttpUtility.HtmlEncode(HttpUtility.UrlEncode(sVal));
        //                sResponse += sVal;
        //            }
        //            else
        //            {
        //                sResponse += "@";
        //                i--;
        //            }
        //        }
        //    }
        //    iInReplaceSettings--;
        //    //if (aLine.GetLength(0) > 1) sResponse = ReplaceSettings(sResponse, sContextName);
        //    return sResponse.Trim();
        //}

        int iInReplaceSettings = 0;
        public string ReplaceSettings(string sLine, string psContextName, string sID)
        {
            return ReplaceSettings(sLine, psContextName, sID, false,false);
        }
        public string ReplaceSettings(string sLine, string psContextName, string sID, bool bAll)
        {
            return ReplaceSettings(sLine, psContextName, sID, bAll, false);
        }
        public string ReplaceSettings(string sLine, string psContextName, string sID, bool bAll, bool bStrict)
        {
            if (iInReplaceSettings > 10) return sLine;
            iInReplaceSettings++;
            char cSep = '@';
            string sResponse = "";
            string[] aLine = sLine.Split(cSep);
            for (int i = 0; i < aLine.GetLength(0); i++)
            {
                sResponse += aLine[i];
                i++;
                if (i < aLine.GetLength(0))
                {
                    string sVar = aLine[i] + "..";
                    if (sVar.Contains(">") || sVar.Contains(" ") || (i == aLine.GetLength(0) - 1))
                    {
                        sResponse += cSep;
                        i--;
                    }
                    else
                    {
//                        string[] aVar = sVar.Split('.');
                        string[] aVar = ParseUtils.MySplit (  sVar,'.');

                        //string sVal = GetParameterValue(aVar[0], "missingvalue");
                        string sVal = GetSV(aVar[0], psContextName, sID, "missingvalue");
                        if (!bStrict && (sVal == "missingvalue")) sVal = GetSV(aVar[0], "", "", "missingvalue");
                        if (bAll && (sVal == "missingvalue")) sVal = "";

                        if (sVal != "missingvalue")
                        {
                            EncodeOption encOpt = GetEncodeOption(aVar[1]);
                            sVal = ApplyFormat(sVal, aVar[2]);
                            sVal = Utils.Encode(sVal, encOpt);
                            sResponse += sVal;
                        }
                        else
                        {
                            sResponse += cSep;
                            i--;
                        }
                    }
                }
            }
            iInReplaceSettings--;
            return sResponse;
        }

        //public string ReplaceSettings(string sLine,string psContextName, string sID, bool bAll)
        //{
        //    if (iInReplaceSettings > 10) return sLine;
        //    iInReplaceSettings++;
        //    char cSep = '@';
        //    string sResponse = "";
        //    string[] aLine = sLine.Split(cSep);
        //    for (int i = 0; i < aLine.GetLength(0); i++)
        //    {
        //        sResponse += aLine[i];
        //        i++;
        //        if (i < aLine.GetLength(0))
        //        {
        //            string sVar = aLine[i] + "..";
        //            string[] aVar = sVar.Split('.');
                    
        //            //string sVal = GetParameterValue(aVar[0], "missingvalue");
        //            string sVal = GetSV(aVar[0], psContextName, sID, "missingvalue");
        //            if (sVal == "missingvalue") sVal = GetSV(aVar[0], "", "", "missingvalue");
        //            if (bAll && (sVal == "missingvalue")) sVal = "";

        //            if (sVal != "missingvalue")
        //            {
        //                EncodeOption encOpt = GetEncodeOption(aVar[1]);
        //                sVal = ApplyFormat(sVal, aVar[2]);
        //                sVal = Encode(sVal, encOpt);
        //                sResponse += sVal;
        //            }
        //            else
        //            {
        //                sResponse += cSep;
        //                i--;
        //            }
        //        }
        //    }
        //    iInReplaceSettings--;
        //    return sResponse;
        //}



        //public string GetSV(string sVarName, string psContextName, string sID)
        //{
        //    return GetSV(sVarName, psContextName, sID, "");
        //}
        //public string GetSV(string sVarName, string psContextName, string sID, string sDefault)
        //{
        //    string sVal = sDefault ;
        //    string sSessionID = "";
        //    string sContextName = psContextName;
        //    if (m_paramDefaults["SESSIONID"] != null) sSessionID = (string)m_paramDefaults["SESSIONID"];
        //    if (m_templateParams["SESSIONID"] != null) sSessionID = (string)m_templateParams["SESSIONID"];
        //    if (sID == "ID")
        //    {
        //        sID = "";
        //        if (m_paramDefaults["ID"] != null) sID = (string)m_paramDefaults["ID"];
        //        if (m_templateParams["ID"] != null) sID = (string)m_templateParams["ID"];
        //    }
        //    //if (sContextName != "") 
        //    sContextName += "_" + sID;
        //    if (m_SessionVariables.ContainsKey(sVarName + "_" + sContextName))
        //    {
        //        sVal = m_SessionVariables[sVarName + "_" + sContextName].ToString();
        //    }
        //    else
        //    {
        //        //               string sSQL = "SELECT value FROM S_Settings WHERE SessionID='" + sSessionID + "' and Parameter='" + sVarName + "' and InstanceID='" + sContextName + "'";
        //        string sSQL = "SELECT value FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and Parameter='" + sVarName.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "'";
        //        DataTable dt = m_dataProvider.GetDataTable(sSQL);
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            sVal = Connectors.GetStringResult(dt.Rows[0][0], "");
        //        }
        //        m_SessionVariables[sVarName + "_" + sContextName] = sVal;
        //    }
        //    sVal = ReplaceSettings(sVal, psContextName);
        //    return sVal;
        //}
        //public void SetSV(string sVarName, string sContextName, string sVal, string sID)
        //{
        //    string sSessionID = "";
        //    if (m_paramDefaults["SESSIONID"] != null) sSessionID = (string)m_paramDefaults["SESSIONID"];
        //    if (m_templateParams["SESSIONID"] != null) sSessionID = (string)m_templateParams["SESSIONID"];
        //    if (sID == "ID")
        //    {
        //        sID = "";
        //        if (m_paramDefaults["ID"] != null) sID = (string)m_paramDefaults["ID"];
        //        if (m_templateParams["ID"] != null) sID = (string)m_templateParams["ID"];
        //    }
        //    //if (sContextName != "") 
        //    sContextName += "_" + sID;
        //    //           string sSQL = "DELETE FROM S_Settings WHERE SessionID='" + sSessionID + "' and Parameter='" + sVarName + "' and InstanceID='" + sContextName + "';";
        //    //           string sSQL1 = "INSERT INTO S_Settings ([SessionID],[InstanceID],[Parameter],[Value]) VALUES ('" + sSessionID + "','" + sContextName + "','" + sVarName + "','" + sVal.Replace("'", "''") + "');";
        //    string sSQL = "DELETE FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and Parameter='" + sVarName.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "';";
        //    //string sSQL1 = "INSERT INTO [S_SessionVariables] ([SessionID],[InstanceID],[Parameter],[Value]) VALUES ('" + sSessionID.Replace("'", "''") + "','" + sContextName.Replace("'", "''") + "','" + sVarName.Replace("'", "''") + "','" + sVal.Replace("'", "''") + "');";
        //    string sSQL1 = "INSERT INTO [S_SessionVariables] ([SessionID],[InstanceID],[Parameter],[Value]) VALUES ('" + sSessionID.Replace("'", "''") + "','" + sContextName.Replace("'", "''") + "','" + sVarName.Replace("'", "''") + "','%" + sVarName + "%');";
        //    //           m_dataProvider.ExecuteNonQuery(sSQL+sSQL1 );
        //    m_SessionVariables[sVarName + "_" + sContextName] = sVal;
        //    m_SessionVariablesUpdates[sVarName + "_" + sContextName] = sSQL + sSQL1;
        //}
        //public void DelSV(string sVarName, string sContextName,  string sID)
        //{
        //    string sSessionID = "";
        //    if (m_paramDefaults["SESSIONID"] != null) sSessionID = (string)m_paramDefaults["SESSIONID"];
        //    if (m_templateParams["SESSIONID"] != null) sSessionID = (string)m_templateParams["SESSIONID"];
        //    if (sID == "ID")
        //    {
        //        sID = "";
        //        if (m_paramDefaults["ID"] != null) sID = (string)m_paramDefaults["ID"];
        //        if (m_templateParams["ID"] != null) sID = (string)m_templateParams["ID"];
        //    }
        //    //if (sContextName != "") 
        //    sContextName += "_" + sID;
        //    //           string sSQL = "DELETE FROM S_Settings WHERE SessionID='" + sSessionID + "' and Parameter='" + sVarName + "' and InstanceID='" + sContextName + "';";
        //    //           string sSQL1 = "INSERT INTO S_Settings ([SessionID],[InstanceID],[Parameter],[Value]) VALUES ('" + sSessionID + "','" + sContextName + "','" + sVarName + "','" + sVal.Replace("'", "''") + "');";
        //    string sSQL = "DELETE FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and Parameter='" + sVarName.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "';";
        //    //           m_dataProvider.ExecuteNonQuery(sSQL+sSQL1 );
        //    m_SessionVariables[sVarName + "_" + sContextName] = "missing_setting";
        //    m_dataProvider.ExecuteNonQuery(sSQL);
        //}
        //public void DelFromSV(string sVarName, string sContextName, string sID)
        //{
        //    string sSessionID = "";
        //    if (m_paramDefaults["SESSIONID"] != null) sSessionID = (string)m_paramDefaults["SESSIONID"];
        //    if (m_templateParams["SESSIONID"] != null) sSessionID = (string)m_templateParams["SESSIONID"];
        //    if (sID == "ID")
        //    {
        //        sID = "";
        //        if (m_paramDefaults["ID"] != null) sID = (string)m_paramDefaults["ID"];
        //        if (m_templateParams["ID"] != null) sID = (string)m_templateParams["ID"];
        //    }
        //    //if (sContextName != "") 
        //    sContextName += "_" + sID;

        //    string sSQL1 = "SELECT parameter FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "' and timestamp>= isnull((SELECT timestamp FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "'  and Parameter='" + sVarName.Replace("'", "''") + "' ),getdate())";
        //    if (sVarName == "") sSQL1 = "SELECT parameter FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "'";
        //    DataTable dt = m_dataProvider.GetDataTable(sSQL1);
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        foreach (DataRow row in dt.Rows)
        //            m_SessionVariables[Connectors.GetStringResult(row[0], "") + "_" + sContextName] = "missing_setting";
        //    }


        //    //           string sSQL = "DELETE FROM S_Settings WHERE SessionID='" + sSessionID + "' and Parameter='" + sVarName + "' and InstanceID='" + sContextName + "';";
        //    //           string sSQL1 = "INSERT INTO S_Settings ([SessionID],[InstanceID],[Parameter],[Value]) VALUES ('" + sSessionID + "','" + sContextName + "','" + sVarName + "','" + sVal.Replace("'", "''") + "');";
        //    string sSQL = "DELETE FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "' and timestamp>= isnull((SELECT timestamp FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "'  and Parameter='" + sVarName.Replace("'", "''") + "' ), getdate())";
        //    if (sVarName == "") sSQL = "DELETE FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "'";
        //    m_dataProvider.ExecuteNonQuery(sSQL);
        //}

        public string GetSV(string sVarName, string psContextName, string sID)
        {
            return GetSV(sVarName, psContextName,sID, "",false);
        }
        public string GetSV(string sVarName, string psContextName, string sID,string sDefault)
        {
            return GetSV(sVarName, psContextName,sID , sDefault,false);
        }
        public string GetSV(string sVarName, string psContextName, string sID, string sDefault,bool bStrict)
        {
            string sContextName = psContextName;
            if (sID == "ID")
            {
                sID = "";
                if (m_paramDefaults["ID"] != null) sID = (string)m_paramDefaults["ID"];
                if (m_templateParams["ID"] != null) sID = (string)m_templateParams["ID"];
            }
            sContextName += "_" + sID;

            string sVal = m_evaluator.GetSV(sVarName, sContextName, sDefault);
            sVal = ReplaceSettings(sVal, psContextName,sID,false,bStrict );
            sVal = ReplaceParameters(sVal);
            return sVal;

//            return m_evaluator.GetSV(sVarName, sContextName, sDefault);
        }
        public void SetSV(string sVarName, string psContextName, string sVal, string sID)
        {
            string sContextName = psContextName;
            if (sID == "ID")
            {
                sID = "";
                if (m_paramDefaults["ID"] != null) sID = (string)m_paramDefaults["ID"];
                if (m_templateParams["ID"] != null) sID = (string)m_templateParams["ID"];
            }
            sContextName += "_" + sID;
            m_evaluator.SetSV(sVarName, sContextName, sVal);
        }
        
        public void DelSV(string sVarName, string psContextName, string sID)
        {
            string sContextName = psContextName;
            if (sID == "ID")
            {
                sID = "";
                if (m_paramDefaults["ID"] != null) sID = (string)m_paramDefaults["ID"];
                if (m_templateParams["ID"] != null) sID = (string)m_templateParams["ID"];
            }
            sContextName += "_" + sID;
            m_evaluator.DelSV(sVarName, sContextName, false);
        }
        public void DelFromSV(string sVarName, string psContextName, string sID)
        {
            string sContextName = psContextName;
            if (sID == "ID")
            {
                sID = "";
                if (m_paramDefaults["ID"] != null) sID = (string)m_paramDefaults["ID"];
                if (m_templateParams["ID"] != null) sID = (string)m_templateParams["ID"];
            }
            sContextName += "_" + sID;
            m_evaluator.DelSV(sVarName, sContextName, true);
        }
        public void DelAll()
        {
            m_evaluator.DelSV("", "*", true);
        }
        public bool RequestIncludeOnce(string sTemplateName)
        {
            return m_evaluator.RequestIncludeOnce(sTemplateName);
        }
        public void ClearIncludeOnce()
        {
            m_evaluator.ClearIncludeOnce();
        }
        public void ClearCache()
        {
            m_evaluator.ClearCache();
        }
        public void ClearTemplatesCache()
        {
            m_evaluator.ClearTemplatesCache();
        }
        
        public string GetCfg(string sVarName)
        {
            return GetCfg(sVarName, "");
        }
        public string GetCfg(string sVarName, string sDefault)
        {
            string sVal = m_evaluator.GetCfg(sVarName, sDefault);
            sVal = ReplaceSettings(sVal, "","");
            sVal = ReplaceParameters(sVal);
            return sVal;
        }
        //public string GetCfg(string sVarName, string sDefault)
        //{
        //    string sVal, sVar;
        //    if (m_ConfigVariables.Count == 0)
        //    {
        //        string sSQL = "SELECT value,ConfigClass,ConfigName FROM [S_ConfigVariables] ";
        //        DataTable dt = m_dataProvider.GetDataTable(sSQL);
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            foreach (DataRow row in dt.Rows)
        //            {
        //                sVal = Connectors.GetStringResult(row[0], "");
        //                sVar = Connectors.GetStringResult(row["ConfigClass"], "") + "__" + row["ConfigName"];
        //                m_ConfigVariables[sVar] = sVal;
        //            }
        //        }
        //    }

        //    string sVal = sDefault;
        //    if (!sVarName.Contains("__")) sVarName = "__" + sVarName;
        //    if (m_ConfigVariables.ContainsKey(sVarName))
        //    {
        //        sVal = m_ConfigVariables[sVarName].ToString();
        //    }
        //    return sVal;
        //}
        //public string GetCfg1(string sVarName, string sDefault)
        //{
        //    string sVal = sDefault;
        //    if (!sVarName.Contains("__")) sVarName = "__" + sVarName;
        //    if (m_ConfigVariables.ContainsKey(sVarName))
        //    {
        //        sVal = m_ConfigVariables[sVarName].ToString();
        //    }
        //    else
        //    {
        //        int i = sVarName.IndexOf("__");
        //        string sCfgClass = sVarName.Substring(0, i);
        //        string sCfgName = sVarName.Substring(i + 2, sVarName.Length - i - 2);
        //        string sSQL = "SELECT value FROM [S_ConfigVariables] WHERE isnull(ConfigClass,'')='" + sCfgClass.Replace("'", "''") + "' and ConfigName='" + sCfgName.Replace("'", "''") + "'";
        //        DataTable dt = m_dataProvider.GetDataTable(sSQL);
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            sVal = Connectors.GetStringResult(dt.Rows[0][0], "");
        //        }
        //        m_ConfigVariables[sVarName] = sVal;
        //    }
        //    return sVal;
        //}

        private string sGlobalMacroName = "";
        public string GlobalMacroName
        {
            get
            {
                if (sGlobalMacroName == "")
                {
                    string strGlobalMacroName = System.Configuration.ConfigurationManager.AppSettings["GlobalMacroName"];
                    if (strGlobalMacroName != null)
                        sGlobalMacroName = strGlobalMacroName;
                    else sGlobalMacroName = "MACRO";
                }
                return sGlobalMacroName;
            }
        }
        private string escapeMacroPar(string sPar)
        {
            string sPar1 = sPar;
            sPar1 = sPar1.Replace(",", "\\,");
            sPar1 = sPar1.Replace(")", "\\)");
            return sPar1;
        }
        public string GetGlobalMacro(string sMacroTemplate, string sMacroName, string[] aMacroPars)
        {
            string s_text = "";

            if (m_MacroTemplates.ContainsKey(sMacroTemplate))
           {
               s_text = m_MacroTemplates[sMacroTemplate].ToString();
           }
           else
           {
               RequestTemplateIDForInclude(sMacroTemplate);
               RequestTemplate(this.IDForInclude);
               if (this.TextForInclude == null)
                   throw new Exception("Could not retrieve template content for TemplateID=" + this.IDForInclude.ToString());

               s_text = this.TextForInclude;
               m_MacroTemplates[sMacroTemplate] = s_text;
           }

            string sMacroContent = "";
            int i1 = s_text.IndexOf("#STARTMACRO." + sMacroName, StringComparison.OrdinalIgnoreCase);
            int i21 = s_text.IndexOf("#", i1 + 1);
            int i22 = s_text.IndexOf("(", i1 + 1);
            int i2;
            if (i21 == -1) i21 = 100000;
            if (i22 == -1) i22 = 100000;
            i2 = (i21 < i22) ? i21 : i22;
            if (i2 == 100000) i2 = -1;
            if ((i1 != -1) && (i2 != -1))
            {
                s_text = s_text.Replace("\r\n#endmacro." + sMacroName + "#", "#endmacro." + sMacroName + "#");
                s_text = s_text.Replace("#endmacro." + sMacroName + "#\r\n", "#endmacro." + sMacroName + "#");
                s_text = s_text.Replace("\r\n#ENDMACRO." + sMacroName + "#", "#ENDMACRO." + sMacroName + "#");
                s_text = s_text.Replace("#ENDMACRO." + sMacroName + "#\r\n", "#ENDMACRO." + sMacroName + "#");
                int i3 = s_text.IndexOf("#ENDMACRO." + sMacroName + "#", i1 + 1, StringComparison.OrdinalIgnoreCase);
                int i4 = i3 + "#ENDMACRO.".Length + sMacroName.Length + "#".Length;
                if (i3 == -1)
                    throw new Exception("invalid definition for ENDMACRO " + sMacroName);
                string sMacroText = s_text.Substring(i1 + ("#STARTMACRO." + sMacroName + "").Length, i3 - i1 - ("#STARTMACRO." + sMacroName + "").Length);
                string sMacroDefParameters = "";
                string[] aMacroDefParameters = new string[0];
                if (sMacroText.StartsWith("("))
                {
                    int i = sMacroText.IndexOf(")");
                    if (i == -1) throw new Exception("invalid parameters for MACRO " + sMacroName);
                    sMacroDefParameters = sMacroText.Substring(1, i - 1);
                    aMacroDefParameters = sMacroDefParameters.Split(',');
                    sMacroContent = sMacroText.Substring(i + 1 + 1); // )#
                }
                else
                    sMacroContent = sMacroText.Substring(1); //#
                while (sMacroContent.StartsWith("\r\n")) sMacroContent = sMacroContent.Substring(2);
                string sMacroPar = "";
                for (int i = 0; i < aMacroDefParameters.Length; i++)
                {
                    if (aMacroPars.Length < i + 1)
                        sMacroPar = "";
                    else
                        sMacroPar = aMacroPars[i].Replace("<tilda>", ",");
                    sMacroContent = sMacroContent.Replace("%" + aMacroDefParameters[i] + ".esc%", escapeMacroPar(sMacroPar));
                    sMacroContent = sMacroContent.Replace("%" + aMacroDefParameters[i] + "%", sMacroPar);
                }

            }
            return sMacroContent;
        }
        public string ReplaceSuperMacro(string s_text)
        {
            string s_text1 = s_text.Replace("\\)", "<tilda>");
            Regex regexMacro_before = new Regex(@"@@[^\.\( \)]*?\(([^\)]*?)\)", RegexOptions.IgnoreCase);
            Regex regexMacro = new Regex(@"@@[^\(\)]*?\((([^\)]*\\\))*[^\)]*|[^\)]*)\)", RegexOptions.IgnoreCase);
            MatchCollection mc = regexMacro.Matches(s_text1);
            while (mc.Count > 0)
            {
                for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
                {
                    Match m = mc[matchIndex];
                    string sMacro = m.Value.Replace("<tilda>", ")");
                    int i1 = 1;
                    string sMacroTemplate = GlobalMacroName;
                    int i2 = sMacro.IndexOf('(', i1 + 1);
                    string sMacroName = sMacro.Substring(i1 + 1, i2 - i1 - 1);
                    string sMacroParameters = sMacro.Substring(i2 + 1, sMacro.Length - i2 - 2 );
                    sMacroParameters = sMacroParameters.Replace("\\,", "<tilda>");
                    string[] aMacroPars = sMacroParameters.Split(',');
                    string sResult = GetGlobalMacro(sMacroTemplate, sMacroName, aMacroPars);
                    s_text = s_text.Replace(m.Value, sResult);
                }
                s_text1 = s_text.Replace("\\)", "<tilda>");
                mc = regexMacro.Matches(s_text1);
            }
            return s_text;
        }
        public string ReplaceGlobalMacro(string s_text)
        {
            string s_text1 = s_text.Replace("\\)", "<tilda>");
            Regex regexMacro = new Regex(@"\#MACRO\.[^\.\(\)]*?\.[^\.\(\)]*?\(([^\)]*?)\)\#", RegexOptions.IgnoreCase);
            MatchCollection mc = regexMacro.Matches(s_text1);
            while (mc.Count > 0)
            {
                for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
                {
                    Match m = mc[matchIndex];
                    string sMacro = m.Value.Replace("<tilda>", ")");
                    int i1 = sMacro.IndexOf('.', "#MACRO.".Length);
                    string sMacroTemplate = sMacro.Substring("#MACRO.".Length, i1 - "#MACRO.".Length);
                    int i2 = sMacro.IndexOf('(', i1 + 1);
                    string sMacroName = sMacro.Substring(i1 + 1, i2 - i1 - 1);
                    string sMacroParameters = sMacro.Substring(i2 + 1, sMacro.Length - i2 - 2 - 1);
                    sMacroParameters = sMacroParameters.Replace("\\,", "<tilda>");
                    string[] aMacroPars = sMacroParameters.Split(',');
                    string sResult = GetGlobalMacro(sMacroTemplate, sMacroName, aMacroPars);
                    s_text = s_text.Replace(m.Value, sResult);
                }
                s_text1 = s_text.Replace("\\)", "<tilda>");
                mc = regexMacro.Matches(s_text1);
            }
            return s_text;
        }
        public string ReplaceOverMacro(string s_text)
        {
            string s_text1 = s_text.Replace("\\)", "<tilda>");
            Regex regexMacro = new Regex(@"\#MACRO\.*?\((\.*?)\)\#", RegexOptions.IgnoreCase);
            //good
            //Regex regexMacro = new Regex(@"\#MACRO\.[^\.\(\)]*?\(([^\)]*?)\)\#", RegexOptions.IgnoreCase);
            
            MatchCollection mc = regexMacro.Matches(s_text1);
            for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
            {
                Match m = mc[matchIndex];
                string sResult = "";
                s_text = s_text.Replace(m.Value, sResult);
            }
            return s_text;
        }
        public string ReplaceMacro(string s_text, string sMacroName, string sMacroContent, string[] aMacroDefParameters)
        {
            string s_text1 =s_text.Replace ("\\)","<tilda>") ;
            Regex regexMacro = new Regex(@"\#MACRO\." + sMacroName + @"\((.*?)\)\#", RegexOptions.IgnoreCase);
            MatchCollection mc = regexMacro.Matches(s_text1);
            for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
            {
                Match m = mc[matchIndex];
                string sMacro = m.Value.Replace("<tilda>", ")");
                string sMacroParameters = sMacro.Substring("#MACRO.".Length + sMacroName.Length + 1, sMacro.Length - "#MACRO.".Length - sMacroName.Length - 1 - ")#".Length);
                sMacroParameters = sMacroParameters.Replace("\\,", "<tilda>");
                string[] aMacroPars = sMacroParameters.Split(',');
                string sMacroPar;
                string sMacroContent1 = sMacroContent;
                for (int i = 0; i < aMacroDefParameters.Length; i++)
                {
                    if (aMacroPars.Length < i + 1)
                        sMacroPar = "";
                    else
                        sMacroPar = aMacroPars[i].Replace("<tilda>", ",");
                    sMacroContent1 = sMacroContent1.Replace("%" + aMacroDefParameters[i] + ".esc%", escapeMacroPar(sMacroPar));
                    sMacroContent1 = sMacroContent1.Replace("%" + aMacroDefParameters[i] + "%", sMacroPar); 
                }
                s_text = s_text.Replace(m.Value, sMacroContent1);
            }
            return s_text;
            //string s_text1 =s_text.Replace ("\\)","<tilda>") ;
            //Regex regexMacro = new Regex(@"\#MACRO\." + sMacroName + @"\((.*)\)\#", RegexOptions.IgnoreCase);
            //MatchCollection mc = null;

            //mc = regexMacro.Matches(s_text1);
            //for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
            //{
            //    Match m = mc[matchIndex];
            //    string sMacro = m.Value.Replace("<tilda>", ")");
            //    sMacro = sMacro.Replace("\\,", "<tilda>");
            //    string[] aMacroPars = sMacro.Split(',');
            //    foreach (string sMacroPar in aMacroPars)
            //    {
            //        sMacroPar = sMacroPar.Replace("<tilda>", ",");
            //    }

            //}
//            return s_text;
        }
        public string ProcessMacros(string s_text)
        {
            int i1 = s_text.IndexOf("#STARTMACRO.", StringComparison.OrdinalIgnoreCase);
            int i21 = s_text.IndexOf("#", i1 + 1);
            int i22 = s_text.IndexOf("(", i1 + 1);
            int i2;
            if (i21 == -1) i21 = 100000;
            if (i22 == -1) i22 = 100000;
            i2 = (i21 < i22) ? i21 : i22;
            if (i2 == 100000) i2 = -1;
            while ((i1 != -1) && (i2 != -1))
            {
                string sMacroName = s_text.Substring(i1 + "#STARTMACRO.".Length, i2 - i1 - "#STARTMACRO.".Length);
                s_text = s_text.Replace("\r\n#endmacro." + sMacroName + "#", "#endmacro." + sMacroName + "#");
                s_text = s_text.Replace("#endmacro." + sMacroName + "#\r\n", "#endmacro." + sMacroName + "#");
                s_text = s_text.Replace("\r\n#ENDMACRO." + sMacroName + "#", "#ENDMACRO." + sMacroName + "#");
                s_text = s_text.Replace("#ENDMACRO." + sMacroName + "#\r\n", "#ENDMACRO." + sMacroName + "#");
                int i3 = s_text.IndexOf("#ENDMACRO." + sMacroName + "#", i1 + 1, StringComparison.OrdinalIgnoreCase);
                int i4 = i3 + "#ENDMACRO.".Length + sMacroName.Length + "#".Length;
                if (i3 == -1)
                    throw new Exception("invalid definition for ENDMACRO " + sMacroName);
                string sMacroText = s_text.Substring(i1 + ("#STARTMACRO." + sMacroName + "").Length, i3 - i1 - ("#STARTMACRO." + sMacroName + "").Length);
                s_text = s_text.Substring(0, i1) + s_text.Substring(i4);
                string sMacroContent = "";
                string sMacroDefParameters = "";
                string[] aMacroDefParameters = new string[0];
                if (sMacroText.StartsWith("("))
                {
                    int i = sMacroText.IndexOf(")");
                    if (i == -1) throw new Exception("invalid parameters for MACRO " + sMacroName);
                    sMacroDefParameters = sMacroText.Substring(1, i - 1);
                    aMacroDefParameters = sMacroDefParameters.Split(',');
                    sMacroContent = sMacroText.Substring(i + 1 + 1); // )#
                }
                else
                    sMacroContent = sMacroText.Substring(1); //#
                while (sMacroContent.StartsWith("\r\n")) sMacroContent = sMacroContent.Substring(2);
                s_text = ReplaceMacro(s_text, sMacroName, sMacroContent, aMacroDefParameters);

                i1 = s_text.IndexOf("#STARTMACRO.", StringComparison.OrdinalIgnoreCase);
                i21 = s_text.IndexOf("#", i1 + 1);
                i22 = s_text.IndexOf("(", i1 + 1);
                if (i21 == -1) i21 = 100000;
                if (i22 == -1) i22 = 100000;
                i2 = (i21 < i22) ? i21 : i22;
                if (i2 == 100000) i2 = -1;
            }
            return s_text;
        }

        int iInReplaceParameters = 0;
        public string ReplaceParameters(string sLine)
        {
            return ReplaceParameters(sLine, false);
        }

        public string ReplaceParameters(string sLine,bool bAll)
        {
            if (iInReplaceParameters > 10) return sLine;
            iInReplaceParameters++;
            char cSep = '%';
            string sResponse = "";
            string[] aLine = sLine.Split(cSep);
            for (int i = 0; i < aLine.GetLength(0); i++)
            {
                sResponse += aLine[i];
                i++;
                if (i < aLine.GetLength(0))
                {
                    string sVar = aLine[i]+"..";
                    if (sVar.Contains(">") || sVar.Contains(" ") || (i == aLine.GetLength(0) - 1))
                    {
                        sResponse += cSep;
                        i--;
                    }
                    else
                    {
//                        string[] aVar = sVar.Split('.');
                        string[] aVar = ParseUtils.MySplit(sVar, '.');
                        string sVal = GetParameterValue(aVar[0], "missingvalue");
                        if (bAll && (sVal == "missingvalue")) sVal = "";
                        if (sVal != "missingvalue")
                        {
                            EncodeOption encOpt = GetEncodeOption(aVar[1]);
                            sVal = ApplyFormat(sVal, aVar[2]);
                            sVal = Utils.Encode(sVal, encOpt);
                            sResponse += sVal;
                        }
                        else
                        {
                            sResponse += "%";
                            i--;
                        }
                    }
                }
            }
            iInReplaceParameters--;
            //if (aLine.GetLength(0) > 1) sResponse = ReplaceSettings(sResponse, sContextName);
//            return sResponse.Trim();
            return sResponse;

        }
        public string GetParameterValue(string m_strParamName)
        {
            return GetParameterValue(m_strParamName, "");
        }
        public string GetParameterValue(string m_strParamName, string s_default)
        {
            string retVal = s_default;
            if (m_strParamName.ToUpper() == "NOW")
                retVal = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            if (m_strParamName.ToUpper() == "UTCNOW")
                retVal = System.DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
            if (m_strParamName.ToUpper() == "_OPEN_BRACKET_")
                retVal = "(";
            if (m_strParamName.ToUpper() == "_CLOSE_BRACKET_")
                retVal = ")";
            if (m_strParamName.ToUpper() == "_DASHSIGN_")
                retVal = "#";
            if (m_strParamName.ToUpper() == "_PERCENTSIGN_")
                retVal = "%";
            if (m_strParamName.ToUpper() == "_ATSIGN_")
                retVal = "@";
            if (m_strParamName.ToUpper() == "_DOTSIGN_")
                retVal = ".";
            //if (m_strParamName.ToUpper() == "INCLUDEONCE")
            //    retVal = m_evaluator.GetIncludeOnce();
            if (TemplateParams[m_strParamName] != null)
                retVal = (string)TemplateParams[m_strParamName].ToString();
            else
                if (m_UseParameters && (GlobalParams[m_strParamName] != null))
                    retVal = (string)GlobalParams[m_strParamName].ToString();
                else
                    if (ParamDefaults[m_strParamName] != null)
                        retVal = (string)ParamDefaults[m_strParamName].ToString();
            return retVal;
        }
        public string Parameters2Values(string s_text)
        {
            Regex regexPar = new Regex(@"\#PAR(\.([^\.\#]+))*\#", RegexOptions.IgnoreCase);
            MatchCollection mc = regexPar.Matches(s_text);
            for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
            {
                Match m = mc[matchIndex];
                string sPar = m.Value;
                sPar = sPar.Substring(1, sPar.Length - 2);  
                string[] parts = sPar.Split('.');
                string sParName = (parts.Length > 1) ? parts[1] : "";
                string sEncOpt = (parts.Length > 2) ? parts[2] : "";
                string sVal = GetParameterValue(sParName);
                sVal = Utils.Encode(sVal, GetEncodeOption(sEncOpt));

                s_text = s_text.Replace(m.Value , sVal);
            }
            return s_text;
        }
        public string ProcessMeta(string s_text)
        {
            int i1 = s_text.IndexOf("#STARTMETA.", StringComparison.OrdinalIgnoreCase);
            int i2 = s_text.IndexOf("#", i1 + 1);
            while ((i1 != -1) && (i2 != -1))
            {
                string sMetaName = s_text.Substring(i1 + "#STARTMETA.".Length, i2 - i1 - "#STARTMETA.".Length);
                s_text = s_text.Replace("\r\n#endmeta." + sMetaName + "#", "#endmeta." + sMetaName + "#");
                s_text = s_text.Replace("#endmeta." + sMetaName + "#\r\n", "#endmeta." + sMetaName + "#");
                s_text = s_text.Replace("\r\n#ENDMETA." + sMetaName + "#", "#ENDMETA." + sMetaName + "#");
                s_text = s_text.Replace("#ENDMETA." + sMetaName + "#\r\n", "#ENDMETA." + sMetaName + "#");
                //    int i3 = s_text.IndexOf("\r\n#ENDMETA." + sMetaName + "#", i1 + 1);
                //int i4 = i3 + "\r\n#ENDMETA.".Length  + sMetaName.Length  + "#".Length ;
                //if (i3 == -1) { 
                int i3 = s_text.IndexOf("#ENDMETA." + sMetaName + "#", i1 + 1, StringComparison.OrdinalIgnoreCase);
                int i4 = i3 + "#ENDMETA.".Length + sMetaName.Length + "#".Length;
                //}
                if (i3 == -1)
                    throw new Exception("invalid definition for META " + sMetaName);
                string sMetaText = s_text.Substring(i1 + ("#STARTMETA." + sMetaName + "#").Length, i3 - i1 - ("#STARTMETA." + sMetaName + "#").Length);

                //string sMetaContent = ParseParExecute(sMetaText);
                m_partialTextOffset = 0;
                string sMetaContent = ParseExecute(sMetaText,false );
                m_partialTextOffset = 0;
                //s_text = s_text.Replace("\r\n#STARTMETA." + sMetaName + "#\r\n", "#STARTMETA." + sMetaName + "#");
                //s_text = s_text.Replace("\r\n#ENDMETA." + sMetaName + "#\r\n", "#ENDMETA." + sMetaName + "#");
                //s_text = s_text.Replace("#STARTMETA." + sMetaName + "#" + sMetaText + "#ENDMETA." + sMetaName + "#", "");
                s_text = s_text.Substring(0, i1) + s_text.Substring(i4);
                string sSep1 = (string)m_paramDefaults["META1"];
                string sSep2 = (string)m_paramDefaults["META2"];
                if (sSep1 == null) sSep1 = "\r\n";
                if (sSep2 == null) sSep2 = "||";
                if (sSep1 == "") sSep1 = "\r\n";
                if (sSep2 == "") sSep2 = "||";
                s_text = ReplaceMeta(s_text, sMetaName, sMetaContent, sSep1, sSep2);


                i1 = s_text.IndexOf("#STARTMETA.", StringComparison.OrdinalIgnoreCase);
                i2 = s_text.IndexOf("#", i1 + 1);
            }
            return s_text;
        }
        
        private void Trace(string sLabel, string sText)
        {
//m_dataProvider.ExecuteNonQuery(s_Trace);

            try
            {
                string sSessionID = "";
                if (m_paramDefaults["SESSIONID"] != null) sSessionID = (string)m_paramDefaults["SESSIONID"];
                if (m_templateParams["SESSIONID"] != null) sSessionID = (string)m_templateParams["SESSIONID"];
                sText = sText.Replace("'", "''");
                string sPrevTemplates="";
                foreach (int t in m_previousTemplateIDs)
                {
                    sPrevTemplates += t.ToString () + ",";
                }

                string sUserName = "";
                string sUserLogin = "";
                string sParams = "";
                string sExcludeParameters = ",DATABASE,SERVER,INCLUDE,BLOCK,callbackGuid,SessionID,SESSIONID,INITIALSESSIONID,UUID,USERID,USERLOGIN,USERNAME,";
                foreach (DictionaryEntry dParam in m_templateParams)
                {
                    if (dParam.Key.ToString () == "USERNAME") sUserName = dParam.Value.ToString ();
                    if (dParam.Key.ToString () == "USERLOGIN") sUserLogin = dParam.Value.ToString ();

                    if(!sExcludeParameters.Contains (","+dParam.Key+","))
                        sParams += dParam.Key + "=" + dParam.Value + "&";
                };
//                string sTrace = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[S_Trace]') AND type in (N'U')) 
//                CREATE TABLE [dbo].[S_Trace](
//                	[TraceID] [int] IDENTITY(1,1) NOT NULL,
//                	[SessionID] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
//                	[TemplateID] [int] NULL,
//                	[Step] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
//                	[Parameters] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
//                	[PrevTemplates] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
//                	[Content] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
//                	[TraceDateTime] [datetime] NULL CONSTRAINT [DF_S_Trace_TraceDateTime]  DEFAULT (getdate()),
//                 CONSTRAINT [PK_S_Trace] PRIMARY KEY CLUSTERED ([TraceID] ASC) WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY]";

                string sTrace = "";

                 sTrace += " insert into s_trace (sessionid,templateid,step,parameters,prevtemplates,content,userlogin, username) values ('"
                    + sSessionID + "'," + m_templateID + ",'" + sLabel + "','" + sParams.Replace("'", "''") + "','" + sPrevTemplates + "','" + sText + "','" + sUserLogin  + "','" + sUserName + "' )";
                m_dataProvider.ExecuteNonQuery(sTrace);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);  
            }
        }
        public void QueryLog(string sQueryName, string sParameters, int iResultCount, string sResultError, string sResultErrorVerbose, double dDuration)
        {
            if (sQueryName == "SPLITXX") return;
            string sQueryLog = "";
            try
            {
                string sSessionID = "";
                if (m_paramDefaults["SESSIONID"] != null) sSessionID = (string)m_paramDefaults["SESSIONID"];
                if (m_templateParams["SESSIONID"] != null) sSessionID = (string)m_templateParams["SESSIONID"];

                sQueryLog += " insert into s_querylog (sessionid,templateid,queryname,parameters,resultcount,resulterror,resulterrorverbose,duration) values ('"
                    + sSessionID + "'," + m_templateID + ",'" + sQueryName + "','" + sParameters.Replace("'", "''") + "'," + iResultCount + ",'" + sResultError.Replace("'", "''") + "','" + sResultErrorVerbose.Replace("'", "''") + "'," + dDuration .ToString () + " )";
                m_dataProvider.ExecuteNonQuery(sQueryLog);
            }
            catch (Exception ex)
            {
                //                System.Diagnostics.Debug.WriteLine(ex.Message);
                if (ex.Message.StartsWith("Invalid object name"))
                {
                    string sQueryLog1 = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[S_QueryLog]') AND type in (N'U')) 
                                    CREATE TABLE [dbo].[S_QueryLog](
                                    	[QueryLogID] [int] IDENTITY(1,1) NOT NULL,
                                    	[SessionID] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[TemplateID] [int] NULL,
                                    	[QueryName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[Parameters] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[ResultCount] [int] NULL,
                                    	[ResultError] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[ResultErrorVerbose] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[Duration] [int] NULL,
                                    	[LogDateTime] [datetime] NULL CONSTRAINT [DF_S_S_QueryLog_LogDateTime]  DEFAULT (getdate()),
                                     CONSTRAINT [PK_S_QueryLog] PRIMARY KEY CLUSTERED ([QueryLogID] ASC) WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] ";
                    try
                    {
                        m_dataProvider.ExecuteNonQuery(sQueryLog1 + " " + sQueryLog);
                    }
                    catch (Exception) { };
                }
            }
        }
        public void Message(string  sLevel, string sText)
        {
            //System.Diagnostics.Debug.WriteLine("MSG:" + sLevel + " : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            string sMessage = "";
            try
            {
                string sSessionID = "";
                if (m_paramDefaults["SESSIONID"] != null) sSessionID = (string)m_paramDefaults["SESSIONID"];
                if (m_templateParams["SESSIONID"] != null) sSessionID = (string)m_templateParams["SESSIONID"];
                sLevel = sLevel.Replace("'", "''");
                sText = sText.Replace("'", "''");
                sMessage += " insert into s_Message (sessionid,Level,content) values ('" + sSessionID + "','" + sLevel + "','" + sText + "')";
                m_dataProvider.ExecuteNonQuery(sMessage);
            }
            catch (Exception ex)
            {
//                System.Diagnostics.Debug.WriteLine(ex.Message);
                if (ex.Message.StartsWith("Invalid object name"))
                {
                    string sMessage1 = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[S_Message]') AND type in (N'U')) 
                                    CREATE TABLE [dbo].[S_Message](
                                    	[MessageID] [int] IDENTITY(1,1) NOT NULL,
                                    	[SessionID] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[Level] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[Content] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                                    	[MessageDateTime] [datetime] NULL CONSTRAINT [DF_S_Message_MessageDateTime]  DEFAULT (getdate()),
                                     CONSTRAINT [PK_S_Message] PRIMARY KEY CLUSTERED ([MessageID] ASC) WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] ";
                    try
                    {
                        m_dataProvider.ExecuteNonQuery(sMessage1 + " " + sMessage );
                    }
                    catch(Exception){};
                }
            }
        }
        public void ReadSFile(string sKey, out string sFileName, out  string sFileExtension, out byte[] aFileContent, out string sFileContentType)
        {
            m_manager.ReadSFile(sKey, out sFileName, out sFileExtension, out aFileContent, out sFileContentType);
        }
        public string WriteSFile(string sKey, string sFileName, string sFileExtension, byte[] aFileContent, string sFileContentType)
        {
            return m_manager.WriteSFile(sKey,  sFileName,  sFileExtension,  aFileContent,  sFileContentType);
        }

//        public void ReadSFile(string sKey, out string sFileName, out  string sFileExtension,out byte[] aFileContent,out string sFileContentType)
//        {
//            sFileName = "";
//            sFileExtension = "";
//            aFileContent = null;
//            sFileContentType = "";
//            try
//            {
//                string sQueryLog = @"";

//                sQueryLog += " select FileName,FileExtension,FileContent,FileContentType from S_Files where FileID = '" + sKey + "'";
//                DataTable dt = m_dataProvider.GetDataTable(sQueryLog);
//                if (dt != null && dt.Rows.Count > 0)
//                {
//                    sFileName = dt.Rows[0][0].ToString();
//                    sFileExtension = dt.Rows[0][1].ToString();
//                    aFileContent = (byte[])dt.Rows[0][2];
//                    sFileContentType = dt.Rows[0][3].ToString();
//                }
//            }
//            catch (Exception ex)
//            {
//                System.Diagnostics.Debug.WriteLine(ex.Message);
//                throw ;
//            }
//        }
//        public string WriteSFile(string sKey, string sFileName, string sFileExtension, byte[] aFileContent, string sFileContentType)
//        {
//            string sFileLog = "";
//            string sFileID = sKey;
//            sFileName = sFileName.Replace("'", "''");
//            sFileExtension = sFileExtension.Replace("'", "''");
//            sFileContentType = sFileContentType.Replace("'", "''");
//            try
//            {
//                if (sKey != "") sFileLog += " delete from S_Files where FileID = '" + sKey + "'";
//                if (aFileContent != null)
//                {
//                    if (sKey != "")
//                    {
//                        sFileLog += " IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[s_Files]') AND type in (N'U')) SET IDENTITY_INSERT s_Files on";
//                        sFileLog += " insert into S_Files (FileID,FileName,FileExtension,FileContent,FileContentType) values (" + sKey + ",'"
//                            + sFileName + "','" + sFileExtension + "',@FileContent,'" + sFileContentType + "')";
//                        sFileLog += " IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[s_Files]') AND type in (N'U')) SET IDENTITY_INSERT s_Files on";
//                    }
//                    else
//                        sFileLog += " insert into S_Files (FileName,FileExtension,FileContent,FileContentType) values ('"
//                        + sFileName + "','" + sFileExtension + "',@FileContent,'" + sFileContentType + "')";

//                    sFileLog += " SELECT SCOPE_IDENTITY() fileid";
//                }
//                DataTable dt;
//                if (aFileContent != null)
//                    dt = m_dataProvider.GetDataTable(sFileLog, new IDataParameter[] { new SqlParameter("@FileContent", aFileContent) });
//                else
//                    dt = m_dataProvider.GetDataTable(sFileLog);
//                if (dt != null && dt.Rows.Count > 0)
//                    sFileID = dt.Rows[0][0].ToString();
//                return sFileID;
//            }
//            catch (Exception ex)
//            {
//                //                System.Diagnostics.Debug.WriteLine(ex.Message);
//                if (ex.Message.StartsWith("Invalid object name"))
//                {
//                    string sFileLog1 = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[S_Files]') )
//                        CREATE TABLE [dbo].[S_Files]([FileID] [int] IDENTITY(1,1) NOT NULL,	[FileName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, [FileExtension] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, [FileContent] [varbinary](max) NULL,	[FileContentType] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, CONSTRAINT [PK_S_Files] PRIMARY KEY CLUSTERED (	[FileID] ASC )WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] ";
//                    try
//                    {
//                        m_dataProvider.ExecuteNonQuery(sFileLog1 + " " + sFileLog);
//                    }
//                    catch (Exception ex1) { throw ; };
//                }
//                else
//                    throw ;
                
//            }
//            return "";
//        }

        //        CREATE TABLE [dbo].[DMSDocumentBlob](
//    [DocumentBlobID] [int] IDENTITY(1,1) NOT NULL,
//    [DocumentID] [int] NULL,
//    [DocumentContent] [varchar](max) NULL,
//    [Extension] [varchar](20) NULL,
//    [NumberOfPages] [int] NULL,
//    [BlobTypeID] [int] NULL,
//    [IsMainBlob] [int] NULL,
//    [FileName] [varchar](255) NULL,
// CONSTRAINT [PK_DMSDocumentBlob] PRIMARY KEY CLUSTERED 
//(
//    [DocumentBlobID] ASC
//)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
//) ON [PRIMARY]
        public void ReadBlobFile(string sKey, out string sFileName, out  string sFileExtension, out byte[] aFileContent, out string sFileContentType)
        {
            sFileName = "";
            sFileExtension = "";
            aFileContent = null;
            sFileContentType = "";
            try
            {
                string sQueryLog = @"";

                sQueryLog += " select FileName,Extension,DocumentContent,BlobTypeID from DMSDocumentBlob where DocumentBlobID = '" + sKey + "'";
                DataTable dt = m_dataProvider.GetDataTable(sQueryLog);
                if (dt != null && dt.Rows.Count > 0)
                {
                    sFileName = dt.Rows[0][0].ToString();
                    sFileExtension = dt.Rows[0][1].ToString();
                    try
                    {
                        aFileContent = (byte[])dt.Rows[0][2];
                    }
                    catch(Exception ){
                        aFileContent = Convert.FromBase64String((string)dt.Rows[0][2]);

                        //aFileContent = new byte[((string)dt.Rows[0][2]).Length * sizeof(char)];
                        //System.Buffer.BlockCopy(((string)dt.Rows[0][2]).ToCharArray(), 0, aFileContent, 0, aFileContent.Length);

                    }
                    sFileContentType = dt.Rows[0][3].ToString();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ;
            }
        }
        public string WriteBlobFile(string sKey, string sFileName, string sFileExtension, byte[] aFileContent, string sFileContentType)
        {
            string sFileID = sKey ;
            sFileName = sFileName.Replace("'", "''");
            sFileExtension = sFileExtension.Replace("'", "''");
            sFileContentType = sFileContentType.Replace("'", "''");
            try
            {
                string sDocumentID = "0";
                string sQueryLog = @" ";
                if (sKey.ToUpper().StartsWith("DOC"))
                {
                    sDocumentID = sKey.Substring(3);
                    sKey = "";
                }
                else
                    if (sKey != "") sQueryLog += " delete from DMSDocumentBlob where DocumentBlobID = '" + sKey + "'";
                if (aFileContent != null)
                {
                    if (sKey != "")
                    {
                        sQueryLog += " IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DMSDocumentBlob]') AND type in (N'U')) SET IDENTITY_INSERT DMSDocumentBlob on";
                        sQueryLog += " insert into DMSDocumentBlob (DocumentBlobID,FileName,Extension,DocumentContent,BlobTypeID) values (" + sKey + ",'"
                            + sFileName + "','" + sFileExtension + "',@FileContent,'" + sFileContentType + "')";
                        sQueryLog += " IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DMSDocumentBlob]') AND type in (N'U')) SET IDENTITY_INSERT DMSDocumentBlob off";
                    }
                    else
                        sQueryLog += " insert into DMSDocumentBlob (DocumentID,FileName,Extension,DocumentContent,BlobTypeID) values ('"
                            + sDocumentID + "','" + sFileName + "','" + sFileExtension + "',@FileContent,'" + sFileContentType + "')";

                    sQueryLog += " SELECT SCOPE_IDENTITY() fileid";
                }
                DataTable dt;
                if (aFileContent != null)
                    dt = m_dataProvider.GetDataTable(sQueryLog, new IDataParameter[] { new SqlParameter("@FileContent", aFileContent) });
                else
                    dt = m_dataProvider.GetDataTable(sQueryLog);
                if (dt != null && dt.Rows.Count > 0)
                    sFileID = dt.Rows[0][0].ToString();
                return sFileID;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ;
            }
        }

        
        public string Parse()
        {
            string sResult = "";
            //System.Diagnostics.Debug.WriteLine("PAR1:" + m_templateID.ToString() + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            if (ParserTrace == "1") Trace("start", m_templateID.ToString());
            string m_text_temp="";
            CCmd templateRoot = (CCmd)m_evaluator.RequestTemplateTree(m_templateID, ref m_text_temp);
            if ((templateRoot == null) || (m_text.IndexOf("#STARTMETA.", StringComparison.OrdinalIgnoreCase) > -1))
            {
                //System.Diagnostics.Debug.WriteLine("PAR2:" + "notree" + m_templateID.ToString() + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
                string s_text = m_text;

                //s_text = ReplaceSuperMacro(s_text);
                //if (ParserTrace == "1") Trace("parse1", s_text);
                //s_text = ReplaceGlobalMacro(s_text);
                //if (ParserTrace == "1") Trace("parse2", s_text);
                //s_text = ProcessMacros(s_text);
                //if (ParserTrace == "1") Trace("parse3", s_text);
                //s_text = ReplaceOverMacro(s_text);
                //if (ParserTrace == "1") Trace("parse4", s_text);
                if (s_text.IndexOf("#STARTMETA.", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    s_text = ProcessMeta(s_text);
                    //if (ParserTrace == "1") Trace("meta", s_text);
                    sResult = ParseExecute(s_text, false);
                }

                //return ParseParExecute(s_text);
                else
                    if (s_text.StartsWith("#startcomment#nocache#endcomment#", true, null))
                        sResult = ParseExecute(s_text, false);
                    else
                        sResult = ParseExecute(s_text, true);
                //System.Diagnostics.Debug.WriteLine("PAR2E:" + "notree" + m_templateID.ToString() + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            }
            else
            {
                //System.Diagnostics.Debug.WriteLine("PAR3:" + "parseroot" + m_templateID.ToString() + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
                if (m_text_temp != "notloaded") m_text = m_text_temp;
                sResult = ParseRootExecute(templateRoot);
                //System.Diagnostics.Debug.WriteLine("PAR3E:" + "parseroot" + m_templateID.ToString() + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            }
            if (ParserTrace == "1") Trace("result", sResult);

            return sResult;
        }
        public string ParseExecute(string m_text1) { return ParseExecute(m_text1, false); }

        public string ParseExecute(string m_text1, bool toCacheTree)
        {
            //DateTime dt1 = System.DateTime.Now;
            //DateTime dt2 = System.DateTime.Now;
            //DateTime dt3 = System.DateTime.Now;
            //if (m_templateID == 102) System.Diagnostics.Debug.WriteLine("*** Start parse   " + m_templateID + " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            //if (m_templateID == 92) System.Diagnostics.Debug.WriteLine("*** Start parse 92  " + m_templateID + " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            //if (m_templateID == 93) System.Diagnostics.Debug.WriteLine("*** Start parse 93 " + m_templateID + " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            //if (m_templateID == 92) Trace("ParseExecute0(" + m_templateID + ")", " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            //if (m_templateID == 93) Trace("ParseExecute0(" + m_templateID + ")", " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            if (ParserTrace == "1") Trace("ParseExecute1(" + m_templateID + ")", " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            //System.Diagnostics.Debug.WriteLine(" " + "(" + m_templateID + ") " + " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            CCmd root = new CCmd();
            root.Type = CommandType.GenericCommand;
            m_stack.Clear();
            m_stack.Push(root);

            //m_partialText = new StringBuilder(m_text.ToString());
            m_text = m_text1;
            m_partialText = m_text;

            //mc = generalFieldREx.Matches(m_partialText.ToString());
            mc = generalFieldREx.Matches(m_partialText);
            for (matchIndex = 0; matchIndex < mc.Count; matchIndex++)
            {
                //if (m_templateID == 102) System.Diagnostics.Debug.WriteLine(" " + matchIndex + ": " + " Time: " + System.DateTime.Now.ToString());
                m_skipCurrentMatch = false;
                Match m = mc[matchIndex];

                //this call can modify the m_skipCurrentMatch member variable
                CCmd cmd = GetNewCmd(m.Value, m.Index + m_partialTextOffset, true, false);

                //if the m_skipCurrentMatch has been modified, skip the current cycle.
                if (m_skipCurrentMatch == true)
                {
                    ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
                    continue;
                }

                if (cmd != null)
                {
                    if (cmd.IsBlockComand)
                    {
                        if (cmd.Type == CommandType.ElseIFCommand)
                        {
                            CCmd cmdIf = (CCmd)m_stack.Pop();
                            if (!(cmdIf is CIFCmd))
                                throw new Exception("ELSEIF command at line " + GetLine(m.Index + m_partialTextOffset).ToString() + " does not have a correspondig IF command associated.");
                            else
                            {
                                if (cmdIf.Type == CommandType.IFCommand)
                                    ((CCmd)m_stack.Peek()).ChildCommands.Add(cmdIf);
                            }
                            ((CIFCmd)cmdIf).ElseIfCommand = (CIFCmd)cmd;

                        }
                        m_stack.Push(cmd);
                    }
                    else
                    {
                        cmd.EndIndex = m.Index + m_partialTextOffset + m.Value.Length;
                        ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
                    }
                }
                else // this means we're dealing with a closing #*# tag
                {
                    bool bCloseOK = VerifyClosingTag(m);
                    if (bCloseOK)
                    {
                        CCmd cmdOK = (CCmd)m_stack.Pop();
                        if (m_stack.Count == 0)
                            throw new Exception("Empty stack!");
                        cmdOK.EndIndex = m.Index + m_partialTextOffset + m.Value.Length;
                        if (cmdOK.Type != CommandType.ElseIFCommand)
                            ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmdOK);
                    }
                    else
                        throw new Exception("The closing tag " + m.Value + " at line " + GetLine(m.Index + m_partialTextOffset).ToString() + " is not valid. Check the command it was intended for!");
                }
            }

            if (m_stack.Count >= 1)
                foreach (CCmd stackCommand in m_stack)
                    ValidateCommandTree(stackCommand,0,0);
            //if (m_templateID == 102)            System.Diagnostics.Debug.WriteLine("*** End Parse  " + m_templateID + " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            //if (m_templateID == 92)            System.Diagnostics.Debug.WriteLine("*** End Parse 92  " + m_templateID + " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            //if (m_templateID == 93)            System.Diagnostics.Debug.WriteLine("*** End Parse 93 " + m_templateID + " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            //if (m_templateID == 92) Trace("ParseExecute1(" + m_templateID + ")", " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            //if (m_templateID == 93) Trace("ParseExecute1(" + m_templateID + ")", " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            if (ParserTrace == "1") Trace("ParseExecute2(" + m_templateID + ")", " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            if (toCacheTree)
                m_evaluator.AddTemplateTree(m_templateID, root,m_text1 );
            return ParseRootExecute(root);
            ////dt2 = System.DateTime.Now; 
            //string content = root.Execute();
            ////if (m_templateID == 102)            System.Diagnostics.Debug.WriteLine("*** End ParseExecute  " + m_templateID + " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff")); 
            ////if (m_templateID == 92)            System.Diagnostics.Debug.WriteLine("*** End ParseExecute 92  " + m_templateID + " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff")); 
            ////if (m_templateID == 93)            System.Diagnostics.Debug.WriteLine("*** End ParseExecute 93 " + m_templateID + " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff")); 
            //content = content.Replace("%d%", "#");
            //content = content.Replace("%dash%", "#");
            //content = content.Replace("%macro%", "@@");
            //content = content.Replace("%percent%", "%");
            ////dt3 = System.DateTime.Now;
            ////System.Diagnostics.Debug.WriteLine(" " + "(" + m_templateID + ") parse: " + dt2.Subtract(dt1) + " exec: " + dt3.Subtract(dt1));
            ////if (m_templateID == 92) Trace("ParseExecute3(" + m_templateID + ")", " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            ////if (m_templateID == 93) Trace("ParseExecute3(" + m_templateID + ")", " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            //if (ParserTrace == "1") Trace("ParseExecute3(" + m_templateID + ")", " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            //return content;
        }
        public string ParseRootExecute(CCmd root)
        {
            string content = "";
            content = root.Execute(this);
            if (m_templateParams["noDash"] == null)
            {
                content = content.Replace("%d%", "#");
                content = content.Replace("%dash%", "#");
                content = content.Replace("%macro%", "@@");
                content = content.Replace("%at%", "@");
                content = content.Replace("%percent%", "%");
                content = content.Replace("_percent_", "%");
            }
            if (ParserTrace == "1") Trace("ParseExecute3(" + m_templateID + ")", " Time: " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            return content;
 
        }
 
//        public string Parse1()
//        {
////            if (m_templateID == 102) System.Diagnostics.Debug.WriteLine("*** Start parse  " + m_templateID + " Time: " + System.DateTime.Now.ToString()); 

//            CCmd root = new CCmd();
//            root.Type = CommandType.GenericCommand;
//            m_stack.Clear();
//            m_stack.Push(root);

//            //m_partialText = new StringBuilder(m_text.ToString());
//            m_partialText = m_text;


//            //mc = generalFieldREx.Matches(m_partialText.ToString());
//            mc = generalFieldREx.Matches(m_partialText);
//            for (matchIndex = 0; matchIndex < mc.Count; matchIndex++)
//            {
//                //if (m_templateID == 102) System.Diagnostics.Debug.WriteLine(" " + matchIndex + ": " + " Time: " + System.DateTime.Now.ToString());
//                m_skipCurrentMatch = false;
//                Match m = mc[matchIndex];

//                //this call can modify the m_skipCurrentMatch member variable
//                CCmd cmd = GetNewCmd(m.Value, m.Index + m_partialTextOffset, true, false);

//                //if the m_skipCurrentMatch has been modified, skip the current cycle.
//                if (m_skipCurrentMatch == true)
//                {
//                    ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
//                    continue;
//                }

//                if (cmd != null)
//                {
//                    if (cmd.IsBlockComand)
//                    {
//                        if (cmd.Type == CommandType.ElseIFCommand)
//                        {
//                            CCmd cmdIf = (CCmd)m_stack.Pop();
//                            if (!(cmdIf is CIFCmd))
//                                throw new Exception("ELSEIF command at line " + GetLine(m.Index + m_partialTextOffset).ToString() + " does not have a correspondig IF command associated.");
//                            else
//                            {
//                                if (cmdIf.Type == CommandType.IFCommand)
//                                    ((CCmd)m_stack.Peek()).ChildCommands.Add(cmdIf);
//                            }
//                            ((CIFCmd)cmdIf).ElseIfCommand = (CIFCmd)cmd;

//                        }
//                        m_stack.Push(cmd);
//                    }
//                    else
//                    {
//                        cmd.EndIndex = m.Index + m_partialTextOffset + m.Value.Length;
//                        ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmd);
//                    }
//                }
//                else // this means we're dealing with a closing #*# tag
//                {
//                    bool bCloseOK = VerifyClosingTag(m);
//                    if (bCloseOK)
//                    {
//                        CCmd cmdOK = (CCmd)m_stack.Pop();
//                        if (m_stack.Count == 0)
//                            throw new Exception("Empty stack!");
//                        cmdOK.EndIndex = m.Index + m_partialTextOffset + m.Value.Length;
//                        if (cmdOK.Type != CommandType.ElseIFCommand)
//                            ((CCmd)m_stack.Peek()).ChildCommands.Add((CCmd)cmdOK);
//                    }
//                    else
//                        throw new Exception("The closing tag " + m.Value + " at line " + GetLine(m.Index + m_partialTextOffset).ToString() + " is not valid. Check the command it was intended for!");
//                }
//            }

//            if (m_stack.Count > 1)
//                foreach (CCmd stackCommand in m_stack)
//                    ValidateCommandTree(stackCommand);
//            //if (m_templateID == 102) System.Diagnostics.Debug.WriteLine("*** execute parse  " + m_templateID + " Time: " + System.DateTime.Now.ToString());
//            string content = root.Execute();
//            //if (m_templateID == 102) System.Diagnostics.Debug.WriteLine("*** End parse  " + m_templateID + " Time: " + System.DateTime.Now.ToString()); 
//            return content;
//        }


        /// <summary>
        /// Encodes or decodes a string by meaning of URL and HTML integrity
        /// </summary>
        /// <param name="s">String to encode or decode</param>
        /// <param name="encOption">Option to encode or decode</param>
        /// <returns></returns>
        //public static string Encode(string s, EncodeOption encOption)
        //{
        //    string retVal = s;

        //    switch (encOption)
        //    {
        //        case EncodeOption.Encode:
        //            retVal = XDocHtmlEncode(XDocUrlEncode(retVal));
        //            //retVal = HttpUtility.HtmlEncode(XDocUrlEncode(retVal));
        //            break;
        //        case EncodeOption.Decode:
        //            retVal = XDocUrlDecode(XDocHtmlDecode(retVal));
        //            //retVal = XDocUrlDecode(HttpUtility.HtmlDecode(retVal));
        //            break;
        //        case EncodeOption.HTMLDecode:
        //            retVal = XDocHtmlDecode(retVal);
        //            //retVal = HttpUtility.HtmlDecode(retVal);
        //            break;
        //        case EncodeOption.HTMLEncode:
        //            retVal = XDocHtmlEncode(retVal);
        //            //retVal = HttpUtility.HtmlEncode(retVal);
        //            break;
        //        case EncodeOption.URLDecode:
        //            retVal = XDocUrlDecode(retVal);
        //            break;
        //        case EncodeOption.URLEncode:
        //            retVal = XDocUrlEncode(retVal);
        //            break;
        //        case EncodeOption.EndCDATA:
        //            retVal = retVal.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA3);
        //            retVal = retVal.Replace(Utils.CT_ENDCDATA, Utils.CT_ENDCDATA2);
        //            retVal = retVal.Replace("]]>", Utils.CT_ENDCDATA);
        //            break;
        //        case EncodeOption.SQLEscape:
        //            retVal = retVal.Replace("'", "''");
        //            break;
        //        case EncodeOption.PAREscape:
        //            retVal = retVal.Replace(".", "\\.");
        //            retVal = retVal.Replace("#", "%23");
        //            retVal = retVal.Replace("(", "%28");
        //            retVal = retVal.Replace(")", "%29");
        //            break;
        //        case EncodeOption.MACROEscape:
        //            retVal = retVal.Replace(",", "\\,");
        //            retVal = retVal.Replace(")", "\\)");
        //            break;
        //        case EncodeOption.XDOCEncode:
        //            retVal = retVal.Replace("#", "%23");
        //            retVal = retVal.Replace("(", "%28");
        //            retVal = retVal.Replace(")", "%29");
        //            break;
        //        case EncodeOption.XDOCDecode:
        //            retVal = retVal.Replace("%23", "#");
        //            retVal = retVal.Replace("%28", "(");
        //            retVal = retVal.Replace("%29", ")");
        //            break;
        //        case EncodeOption.EncodeSQLEscape:
        //            retVal = XDocHtmlEncode(XDocUrlEncode(retVal));
        //            retVal = retVal.Replace("'", "''");
        //            //retVal = HttpUtility.HtmlEncode(XDocUrlEncode(retVal));
        //            break;
        //        case EncodeOption.DecodeSQLEscape:
        //            retVal = XDocUrlDecode(XDocHtmlDecode(retVal));
        //            retVal = retVal.Replace("'", "''");
        //            //retVal = XDocUrlDecode(HttpUtility.HtmlDecode(retVal));
        //            break;
        //        case EncodeOption.HTMLDecodeSQLEscape:
        //            retVal = XDocHtmlDecode(retVal);
        //            retVal = retVal.Replace("'", "''");
        //            //retVal = HttpUtility.HtmlDecode(retVal);
        //            break;
        //        case EncodeOption.HTMLEncodeSQLEscape:
        //            retVal = XDocHtmlEncode(retVal);
        //            retVal = retVal.Replace("'", "''");
        //            //retVal = HttpUtility.HtmlEncode(retVal);
        //            break;
        //        case EncodeOption.URLDecodeSQLEscape:
        //            retVal = XDocUrlDecode(retVal);
        //            retVal = retVal.Replace("'", "''");
        //            break;
        //        case EncodeOption.URLEncodeSQLEscape:
        //            retVal = XDocUrlEncode(retVal);
        //            retVal = retVal.Replace("'", "''");
        //            break;
        //        case EncodeOption.JSEscape :
        //            retVal = retVal.Replace("\\", "\\\\");
        //            retVal = retVal.Replace("'", "\\'");
        //            retVal = retVal.Replace("\"", "\\\"");
        //            retVal = retVal.Replace("`", "\\`");
        //            retVal = retVal.Replace("\r", "\\r");
        //            retVal = retVal.Replace("\n", "\\n");
        //            break;
        //        case EncodeOption.HTMLEncodeJSEscape:
        //            retVal = XDocHtmlEncode (retVal);
        //            retVal = retVal.Replace("\\", "\\\\");
        //            retVal = retVal.Replace("'", "\\'");
        //            retVal = retVal.Replace("\"", "\\\"");
        //            retVal = retVal.Replace("`", "\\`");
        //            break;
        //        case EncodeOption.Text2HTML:
        //            retVal = XDocHtmlEncode(retVal);
        //            retVal = retVal.Replace("&#13;&#10;", "<br>");
        //            break;
        //        case EncodeOption.Base64Encode:
        //            byte[] binaryData = Encoding.UTF8.GetBytes(retVal);
        //            retVal = Convert.ToBase64String(binaryData, 0, binaryData.Length);
        //            break;
        //        case EncodeOption.Base64Decode:
        //            byte[] binaryDataD = Convert.FromBase64String(retVal);
        //            retVal = Encoding.UTF8.GetString (binaryDataD, 0, binaryDataD.Length);
        //            break;
        //        case EncodeOption.JSONEscape:
        //            retVal = Utils._JsonEscape (retVal);
        //            break;
        //        case EncodeOption.None:
        //            break;
        //        default:
        //            break;
        //    }

        //    return retVal;
        //}
        //private static string XDocHtmlDecode(string retVal)
        //{
        //    retVal = HttpUtility.HtmlDecode(retVal);
        //    return retVal;
        //}
        //private static string XDocHtmlEncode(string retVal)
        //{
        //    retVal = HttpUtility.HtmlEncode(retVal);
        //    retVal = retVal.Replace("\r\n", "&#13;&#10;");
        //    retVal = retVal.Replace("\r", "&#13;&#10;");
        //    retVal = retVal.Replace("\n", "&#13;&#10;");
        //    return retVal;
        //    //retVal = HttpUtility.HtmlEncode(retVal);
        //    //retVal = retVal.Replace("\r\n|\r|\n", "&#13;&#10;", "g"); // regex should be with modifier global
        //    //return retVal;
        //}
        
        //public static string XDocUrlDecode(string retVal)
        //{
        //    string sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
        //    if (sEncoding == null) sEncoding = "";
        //    if (sEncoding == "") sEncoding = "utf-8";
        //    System.Text.Encoding enc = System.Text.Encoding.GetEncoding(sEncoding);
        //    if (enc != null)
        //        retVal = HttpUtility.UrlDecode(retVal, enc);
        //    else
        //        retVal = HttpUtility.UrlDecode(retVal, System.Text.Encoding.Default);

        //    return retVal;
        //}
        //public static string XDocUrlEncode(string retVal)
        //{
        //    string sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
        //    if (sEncoding == null) sEncoding = "";
        //    if (sEncoding == "") sEncoding = "utf-8";
        //    System.Text.Encoding enc = System.Text.Encoding.GetEncoding(sEncoding);
        //    if (enc != null)
        //        retVal = HttpUtility.UrlEncode(retVal, enc );
        //    else
        //        retVal = HttpUtility.UrlEncode(retVal, System.Text.Encoding.Default);
        //    retVal = retVal.Replace("+", "%20");
        //    retVal = retVal.Replace("!", "%21");
        //    retVal = retVal.Replace("(", "%28");
        //    retVal = retVal.Replace(")", "%29");
        //    retVal = retVal.Replace("'", "%27");

        //    return retVal;
        //}
        //public static string JSONEscape(string aText)
        //{
        //    string result = "";
        //    foreach (char c in aText)
        //    {
        //        switch (c)
        //        {
        //            case '\\': result += "\\\\"; break;
        //            case '\"': result += "\\\""; break;
        //            case '\n': result += "\\n"; break;
        //            case '\r': result += "\\r"; break;
        //            case '\t': result += "\\t"; break;
        //            case '\b': result += "\\b"; break;
        //            case '\f': result += "\\f"; break;
        //            default: result += c; break;
        //        }
        //    }
        //    return result;
        //}
        #endregion


        #region Private Methods


        CCmd GetNewCmd(string txt, int initialIndex, bool acceptClosingBlockCommands, bool forceText)
        {// \((([^\)]*\\\))*[^\)]*|[^\)]*)\)
            bool removedSeparator = false;
            if (txt.StartsWith("#") && txt.EndsWith("#"))
            {
                txt = txt.Substring(SEPARATOR_LENGTH, txt.Length - 2 * SEPARATOR_LENGTH);
                removedSeparator = true;
            }

            CCmd cmd = null;

            try
            {
                if (txt.ToUpper() == START_BLOCK)
                {
                    m_ignoreBlock = true;
                    m_commentBlock = false;
                }
                else if (txt.ToUpper() == END_BLOCK)
                {
                    m_ignoreBlock = false;
                    m_commentBlock = false;
                }
                if (txt.ToUpper() == START_COMMENT)
                {
                    m_ignoreBlock = true;
                    m_commentBlock = true;
                }
                else if (txt.ToUpper() == END_COMMENT)
                {
                    m_ignoreBlock = false;
                    m_commentBlock = false;
                }

                if ((!m_ignoreBlock) )
                {
                    if (txt.ToUpper().StartsWith("ADDPAR.") && (removedSeparator))
                    {
                        cmd = GetADDPARCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("GETPARALL.") && (removedSeparator))
                    {
                        cmd = GetGETPARALLCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("GETPARSTRICT.") && (removedSeparator))
                    {
                        cmd = GetGETPARStrictCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("GETPAR.") && (removedSeparator))
                    {
                        cmd = GetGETPARCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("GETCFGPAR.") && (removedSeparator))
                    {
                        cmd = GetGETCFGPARCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("GETPARREPLACE.") && (removedSeparator))
                    {
                        cmd = GetGETPARREPLACECmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("GETDEFPAR.") && (removedSeparator))
                    {
                        cmd = GetGETDEFPARCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("GETITEMPAR.") && (removedSeparator))
                    {
                        cmd = GetGETITEMPARCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("FINDITEMPAR.") && (removedSeparator))
                    {
                        cmd = GetFINDITEMPARCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("APPVAL.") && (removedSeparator))
                    {
                        cmd = GetAPPENDVALCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("APPENDVAL.") && (removedSeparator))
                    {
                        cmd = GetAPPENDVALCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("APPFLD.") && (removedSeparator))
                    {
                        cmd = GetAPPENDFLDCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("APPENDFLD.") && (removedSeparator))
                    {
                        cmd = GetAPPENDFLDCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("DEL.") && (removedSeparator))
                    {
                        cmd = GetDELCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("DELFROM.") && (removedSeparator))
                    {
                        cmd = GetDELFROMCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("DELALL.") && (removedSeparator))
                    {
                        cmd = GetDELALLCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("DELITEM.") && (removedSeparator))
                    {
                        cmd =GetDELITEMCmd (txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("THROW.") && (removedSeparator))
                    {
                        cmd = GetThrowCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("FILE.") && (removedSeparator))
                    {
                        cmd = GetFILECmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SETPAR.") && (removedSeparator))
                    {
                        cmd = GetSETPARCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SETPAREXT.") && (removedSeparator))
                    {
                        cmd = GetSETPAREXTCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SETFLD.") && (removedSeparator))
                    {
                        cmd = GetSETFLDCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SETFLDEXT.") && (removedSeparator))
                    {
                        cmd = GetSETFLDEXTCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SETVAL.") && (removedSeparator))
                    {
                        cmd = GetSETVALCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SETNULLPAR.") && (removedSeparator))
                    {
                        cmd = GetSETNULLPARCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SETNULLPAREXT.") && (removedSeparator))
                    {
                        cmd = GetSETNULLPAREXTCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SETNULLFLD.") && (removedSeparator))
                    {
                        cmd = GetSETNULLFLDCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SETNULLFLDEXT.") && (removedSeparator))
                    {
                        cmd = GetSETNULLFLDEXTCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SETNULLVAL.") && (removedSeparator))
                    {
                        cmd = GetSETNULLVALCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("JPATHDATA.") && (removedSeparator))
                    {
                        cmd = GetXPATHCmd(txt, initialIndex, false, true, true);
                    }
                    else if (txt.ToUpper().StartsWith("XPATHDATA.") && (removedSeparator))
                    {
                        cmd = GetXPATHCmd(txt, initialIndex, false, false, true);
                    }
                    else if (txt.ToUpper().StartsWith("TXPATHDATA.") && (removedSeparator))
                    {
                        cmd = GetXPATHCmd(txt, initialIndex, true, false, true);
                    }
                    else if (txt.ToUpper().StartsWith("JPATH.") && (removedSeparator))
                    {
                        cmd = GetXPATHCmd(txt, initialIndex, false,true,false);
                    }
                    else if (txt.ToUpper().StartsWith("XPATH.") && (removedSeparator))
                    {
                        cmd = GetXPATHCmd(txt, initialIndex, false,false,false );
                    }
                    else if (txt.ToUpper().StartsWith("TXPATH.") && (removedSeparator))
                    {
                        cmd = GetXPATHCmd(txt, initialIndex, true,false,false);
                    }
                    else if (txt.ToUpper().StartsWith("SPARP."))
                    {
                        cmd = GetDefParCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SPARA."))
                    {
                        cmd = GetDefParCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("SPAR."))
                    {
                        cmd = GetDefParCmd (txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("JPARP.") && (removedSeparator))
                    {
                        cmd = GetJPARCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("JINS.") && (removedSeparator))
                    {
                        cmd = GetJINSCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("JAPP.") && (removedSeparator))
                    {
                        cmd = GetJAPPCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("JUPD.") && (removedSeparator))
                    {
                        cmd = GetJUPDCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("JDEL.") && (removedSeparator))
                    {
                        cmd = GetJDELCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("JPAR.") && (removedSeparator))
                    {
                        cmd = GetJPARCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("JDATA.") && (removedSeparator))
                    {
                        cmd = GetJDATACmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("JPROP.") && (removedSeparator))
                    {
                        cmd = GetJPROPCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("JLOOP.") && (removedSeparator))
                    {
                        cmd = GetJLOOPCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("REPDATA.") && (removedSeparator))
                    {
                        cmd = GetREPCmd(txt, initialIndex,true);
                    }
                    else if (txt.ToUpper().StartsWith("REP.") && (removedSeparator))
                    {
                        cmd = GetREPCmd(txt, initialIndex,false);
                    }
                    else if (txt.ToUpper().StartsWith("COUNT.") && (removedSeparator))
                    {
                        cmd = GetCOUNTCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("QRYDATA.") && (removedSeparator))
                    {
                        cmd = GetQRYCmd(txt, initialIndex,true);
                    }
                    else if (txt.ToUpper().StartsWith("QRY.") && (removedSeparator))
                    {
                        cmd = GetQRYCmd(txt, initialIndex,false);
                    }
                    else if (txt.ToUpper().StartsWith("PAR.") && (removedSeparator))
                    {
                        cmd = GetPARCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("FLD.") && (removedSeparator))
                    {
                        cmd = GetFLDCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("FLDBIN.") && (removedSeparator))
                    {
                        cmd = GetFLDBINCmd(txt);
                    }
                    else if (txt.ToUpper().StartsWith("ATTACH.") && (removedSeparator))
                    {
                        cmd = GetAttachCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("INCLUDEONCE"))
                    {
                        cmd = GetIncludeOnceCommand(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("PARINCLUDE."))
                    {
                        cmd = GetParIncludeCommand(txt, initialIndex);
                    }
                    else if ((txt.ToUpper().StartsWith("INCLUDE(") || txt.ToUpper().StartsWith("UDF(")))
                    {
                        cmd = GetIncludeCommand(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("IMPORTFLD."))
                    {
                        cmd = GetImportFldCommand(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("IMPORTSET."))
                    {
                        cmd = GetImportSetCommand(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("IMPORT."))
                    {
                        cmd = GetImportCommand(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("EXPORT."))
                    {
                        cmd = GetExportCommand(txt);
                    }
                    else if (txt.ToUpper().StartsWith("DELETETEMPLATE."))
                    {
                        cmd = GetDeleteTemplateCommand(txt);
                    }
                    else if (txt.ToUpper().StartsWith("TRANSFERLIKE."))
                    {
                        cmd = GetTransferLikeCommand(txt);
                    }
                    else if (txt.ToUpper().StartsWith("TRANSFER."))
                    {
                        cmd = GetTransferCommand(txt);
                    }
                    else if (txt.ToUpper().StartsWith("CFG."))
                    {
                        cmd = GetCfgCommand(txt);
                    }
                    else if (txt.ToUpper().StartsWith("USEPARAMETERS."))
                    {
                        cmd = GetUseParameters(txt);
                    }
                    else if (txt.ToUpper().StartsWith("FUNC."))
                    {
                        cmd = GetFUNCCommand(txt);
                    }                    
                    else if ((txt.ToUpper().StartsWith("IF") || txt.ToUpper().StartsWith("ELSEIF")) && (removedSeparator))
                    {
                        // because of a limitation of generalFieldREx which does not report the starting "#" 
                        //when the IF condition contains brackets (other than the start and end brackets)
                        int ifIndex = removedSeparator ? initialIndex : initialIndex - 1;

                        cmd = GetIFCommand(txt, ifIndex);
                    }
                    else if (txt.ToUpper().StartsWith("DEFPAR.") )
                    {
                        cmd = GetDefParCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("DEFQRY."))
                    {
                        cmd = GetDefQryCmd(txt, initialIndex);
                    }
                    else if (txt.ToUpper().StartsWith("MSG."))
                    {
                        cmd = GetMsgCmd(txt, initialIndex);
                    }
                    // else if (cmd == null && txt.IndexOf("#") == -1 && !txt.ToUpper().StartsWith("ENDXPATH") && !txt.ToUpper().StartsWith("ENDREP")
                   //&& !txt.ToUpper().StartsWith("ENDQRY") && !txt.ToUpper().StartsWith("ENDIF"))
                    else if (cmd == null && !txt.ToUpper().StartsWith("ENDJLOOP") && !txt.ToUpper().StartsWith("ENDJPROP") && !txt.ToUpper().StartsWith("ENDJPATH") && !txt.ToUpper().StartsWith("ENDXPATH") && !txt.ToUpper().StartsWith("ENDREP")
                        && !txt.ToUpper().StartsWith("ENDQRY") && !txt.ToUpper().StartsWith("ENDIF"))
                    {
                        cmd = new CTextCmd();
                        if (forceText)
                        {
                            if (txt.ToUpper() == END_BLOCK)
                                txt = "#" + END_BLOCK + "#";
                            (cmd as CTextCmd).ForceText(txt);
                        }
                    }
                }
                else
                {
                    cmd = new CTextCmd();
                    string blockContent = "";

                    int endBlockLen = 2 * SEPARATOR_LENGTH + (m_commentBlock?END_COMMENT .Length : END_BLOCK.Length);

                    //int i1 = m_text.ToString().ToUpper().IndexOf("#" + END_BLOCK + "#", initialIndex + 1);
                    int i1 = m_text.ToUpper().IndexOf("#" + (m_commentBlock?END_COMMENT:END_BLOCK) + "#", initialIndex + 1);
                    if (i1 != -1 && mc != null)
                    {
                        blockContent = m_text.ToString().Substring(initialIndex, i1 - initialIndex + endBlockLen);
                        //m_partialText = new StringBuilder(m_text.ToString().Substring(i1 + endBlockLen));
                        m_partialText = m_text.Substring(i1 + endBlockLen);
                        m_partialTextOffset = i1 + endBlockLen;
                        //mc = generalFieldREx.Matches(m_partialText.ToString());
                        mc = generalFieldREx.Matches(m_partialText);
                        matchIndex = -1;
                        m_skipCurrentMatch = true;
                        (cmd as CTextCmd).ForceText(blockContent);
                        m_ignoreBlock = false;
                    }
                }



                if (cmd != null)
                {
                    cmd.StartIndex = initialIndex;
                    cmd.Parser = this;
                }
                else if (!acceptClosingBlockCommands)
                    throw new Exception(" ");

            }
            catch (Exception ex)
            {
                string message = string.Format("Could not create new command from text {0} at line {1}", txt, GetLine(initialIndex)) + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
            return cmd;
        }

        CCmd GetCOUNTCmd(string txt, int initialIndex)
        {
            CCmd cmd = new CCountCmd();

            Match mTemp = wordREx.Match(txt.Substring(6));
            if (mTemp.Success)
                ((CCountCmd)cmd).QueryName = mTemp.Value;

            string paging = "";
            int pagingInitialIndex = -1;

            string paramString = "";
            int paramStringInitialIndex = -1;

            int io1 = txt.IndexOf("(");
            int io2 = txt.IndexOf(")");

            if (io1 > 0 && io2 != -1 && io2 > io1)
            {
                string list = txt.Substring(io1 + 1, io2 - io1 - 1);
                int index = initialIndex + SEPARATOR_LENGTH + io1;
                if (txt[io1 - 1] == '.')
                {
                    paging = list;
                    pagingInitialIndex = index;
                }
                else
                {
                    paramString = list;
                    paramStringInitialIndex = index;
                    int io21 = txt.IndexOf("(", io2 + 1);
                    if (io21 != -1)
                    {
                        int io22 = txt.IndexOf(")", io21 + 1);
                        if (io22 != -1)
                        {
                            paging = txt.Substring(io21 + 1, io22 - io21 - 1);
                            pagingInitialIndex = initialIndex + SEPARATOR_LENGTH + io21;
                        }
                        else
                            throw new Exception("The COUNT command at line " + GetLine(initialIndex).ToString() + " is missing a bracket");
                    }
                }
            }

            if (paging != "")
            {
                int pageSizeCmdIndex = pagingInitialIndex;
                CCmd pageSizeCmd = GetNewCmd(paging, pageSizeCmdIndex, false, false);
                pageSizeCmd.EndIndex = pageSizeCmdIndex + paging.Length;
                ((CCountCmd)cmd).PageSizeCmd = pageSizeCmd;
            }

            if (paramString != "")
            {
                ArrayList arrParams = GetParserParameters(paramString);
                ((CCountCmd)cmd).Parameters = new CCmd[arrParams.Count];

                for (int iParam = 0; iParam < arrParams.Count; iParam++)
                {
                    ParserParam pp = (ParserParam)arrParams[iParam];
                    ((CCountCmd)cmd).Parameters[iParam] = GetNewCmd(pp.Value.Trim(), paramStringInitialIndex + pp.Index, false, true);
                    ((CCountCmd)cmd).Parameters[iParam].EndIndex = paramStringInitialIndex + pp.Index + pp.Value.Length;
                }
            }
            else
                ((CCountCmd)cmd).Parameters = new CCmd[0];
            //((CCountCmd)cmd).Context = m_context;
            //((CCountCmd)cmd).Evaluator = m_evaluator;
            ((CCountCmd)cmd).TemplateID = m_templateID;


            return cmd;
        }

        private bool RegExMatch(string sRegEx, string sValue)
        {
            Regex objPattern = new Regex(sRegEx);
            return objPattern.IsMatch(sValue);
        }
        private CCmd GetDefQryCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit( txt,'.');
            //string sQueryName = (parts.Length > 1) ? parts[1].Replace("<tilda>", ".").Replace("<backslash>", "\\") : "";
            //string sQueryPar = (parts.Length > 2) ? parts[2].Replace("<tilda>", ".").Replace("<backslash>", "\\") : "";
            string sQueryName = (parts.Length > 1) ? parts[1] : "";
            string sQueryPar = (parts.Length > 2) ? parts[2] : "";

            CDEFQRYCmd cmd = new CDEFQRYCmd();
            cmd.QueryName = sQueryName;
            cmd.QueryPar = sQueryPar;
            return cmd;
        }
        private CCmd GetMsgCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sMsgLevel = (parts.Length > 1) ? parts[1] : "";
            string sMsgText = (parts.Length > 2) ? parts[2]: "";

            CMSGCmd cmd = new CMSGCmd();
            //int iMsgLevel = 10;
            //if (int.TryParse(sMsgLevel, out iMsgLevel))
            //    cmd.MsgLevel = iMsgLevel;
            cmd.MsgLevel = sMsgLevel;
            cmd.MsgText = sMsgText;
            return cmd;
        }

        private CCmd GetDefParCmd(string txt, int initialIndex)
        {
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            if (parts.Length < 2)
                throw new Exception(string.Format("The DefPar command at line {0} is not valid", GetLine(initialIndex)));

            string parameterName = parts[1];
            string val = "";
            for (int i = 2; i < parts.Length; i++)
                val += parts[i] + ".";
            if (val.EndsWith("."))
                val = val.Substring(0, val.Length - 1);
            if (val.StartsWith("'"))
                val = val.Substring(1);
            if (val.EndsWith("'"))
                val = val.Substring(0, val.Length - 1);
            txt = txt.ToUpper();

            if (txt.StartsWith("DEFPARR"))
            {
                if (val.Contains("==="))
                {
                    int i = val.IndexOf("===");
                    if (!RegExMatch(val.Substring(i + 3).Trim(), val.Substring(0, i).Trim()))
                        throw (new Exception("Invalid parameter"));
                    val = val.Substring(0, i).Trim();
                }

            }

            //m_paramDefaults[parameterName] = val;

            CDefParCmd cmd = new CDefParCmd();
            cmd.ParameterName = parameterName;
            cmd.ParameterValue = val;
            if (txt.StartsWith("SPARP")) cmd.isParSource = "1";
            if (txt.StartsWith("SPARA")) cmd.ReplaceAllParams = "1";
            if (txt.StartsWith("SPAR")) cmd.isSet = true;
            return cmd;
        }


        CCmd GetAttachCmd(string text, int initialIndex)
        {
            CAttachCmd cmd = new CAttachCmd();

            //consider the text to the first '(' for splitting
            int ioBrOpen = text.IndexOf("(");
            string tempText = text;
            if (ioBrOpen != -1)
                tempText = text.Substring(0, ioBrOpen);
            List<ParseUtils.ListParameter> parts = ParseUtils.GetParameterList(tempText, '\'', '.', false);

            string attType = parts[1].Text;
            if (attType.ToUpper() == "DB")
                cmd.AttachType = AttachType.Database;
            else if (attType.ToUpper() == "FILE")
                cmd.AttachType = AttachType.File;
            else
                throw new Exception("Command " + text + " at line " + GetLine(initialIndex).ToString() + " does not have an attachment type specified.");

            cmd.Extension = parts[2].Text;
            if (cmd.AttachType == AttachType.Database)
            {
                cmd.QueryName = parts[3].Text;
                cmd.FieldName = parts[4].Text;
                //cmd.Context = m_context;
                //cmd.Evaluator = m_evaluator;
                cmd.Parent = GetParentQueryCmd(cmd.QueryName);
                if (cmd.Parent == null)
                    throw new Exception("Command " + text + " at line " + GetLine(initialIndex).ToString() + " does not have a parent REP or QRY for query " + cmd.QueryName);
            }
            else
            {
                int ioBrClose = text.IndexOf(")", ioBrOpen + 1);
                string textToLookUp = text.Substring(ioBrOpen, ioBrClose - ioBrOpen + 1);
                int startIndex = initialIndex + ioBrOpen + 1; // 1 because we exclude the bracket
                textToLookUp = textToLookUp.Replace("(", "").Replace(")", "");
                if (textToLookUp.IndexOf("#") == -1)
                    cmd.FileName = textToLookUp;
                else // we know we have a non-block command
                {
                    CCmd childCmd = GetNewCmd(textToLookUp, startIndex, false, true);
                    if (childCmd != null)
                        cmd.ChildCommands.Add(childCmd);
                }
            }

            return (CCmd)cmd;

        }
        CCmd GetIFCommand(string txt, int initialIndex)
        {

            CCmd cmd = new CIFCmd();
            int ioOpenBr = txt.IndexOf('(');

            string errorBr = "IF command " + txt + " at line " + GetLine(initialIndex).ToString() + " does not contain valid brackets.";
            if (ioOpenBr == -1)
                throw new Exception(errorBr);
            int ioCloseBr = txt.LastIndexOf(')');
            if (ioCloseBr == -1)
                throw new Exception(errorBr);
            string condition = "";
            int conditionLength = ioCloseBr - ioOpenBr - 1;
            if (conditionLength >= 0)
                condition = txt.Substring(ioOpenBr + 1, conditionLength);
            int offset = initialIndex + ioOpenBr + 1 + SEPARATOR_LENGTH;// 1 because we step over the opening bracket

            MatchCollection mcParams = ifParamREx.Matches(condition);

            foreach (Match m in mcParams)
            {
                int nNewIndex = offset + m.Index;
                CCmd condCmd = GetNewCmd(m.Value, nNewIndex, false, true);
                condCmd.EndIndex = offset + m.Index + m.Value.Length;
                ((CIFCmd)cmd).ConditionCmds.Add(condCmd);
            }

            if (txt.ToUpper().StartsWith("IF"))
                ((CIFCmd)cmd).Type = CommandType.IFCommand;
            else
                ((CIFCmd)cmd).Type = CommandType.ElseIFCommand;

            return cmd;
        }
        CCmd GetXPATHCmd(string txt, int initialIndex, bool fromTemplate,bool isJPath, bool isDataTable)
        {
            //#XPATH.query_name(xml_file,path[,isrep/isqry[,param1]*])#
            //#JPATH.query_name(json,jpath[,isrep/isqry[,param1]*])#
            //           {inner-text}
            //#ENDXPATH.query_name#
            //param = memsort|memfilter|memdistinct|memcolumns|Exception
            CCmd cmd = new CXPATHCmd();

            ((CXPATHCmd)cmd).DataTable = isDataTable;

            Match mTemp = wordREx.Match(txt.Substring(6 + (isDataTable?4:0)));
            if (mTemp.Success)
                ((CXPATHCmd)cmd).QueryName = mTemp.Value;

            string paging = "";
            int pagingInitialIndex = -1;

            string paramString = "";
            int paramStringInitialIndex = -1;

            int io1 = txt.IndexOf("(");
            int io2 = txt.IndexOf(")");

            if (io1 > 0 && io2 != -1 && io2 > io1)
            {
                string list = txt.Substring(io1 + 1, io2 - io1 - 1);
                int index = initialIndex + SEPARATOR_LENGTH + io1 + 1;
                if (txt[io1 - 1] == '.')
                {
                    paging = list;
                    pagingInitialIndex = index;
                }
                else
                {
                    paramString = list;
                    paramStringInitialIndex = index;
                    int io21 = txt.IndexOf("(", io2 + 1);
                    if (io21 != -1)
                    {
                        int io22 = txt.IndexOf(")", io21 + 1);
                        if (io22 != -1)
                        {
                            paging = txt.Substring(io21 + 1, io22 - io21 - 1);
                            pagingInitialIndex = initialIndex + SEPARATOR_LENGTH + io21 + 1;
                        }
                        else
                            throw new Exception("The XPATH command at line " + GetLine(initialIndex).ToString() + " is missing a bracket");
                    }
                }
            }

            if (paging != "")
            {
                List<ParseUtils.ListParameter> parts = ParseUtils.GetParameterList(paging, '\'', ',', false);

                if (parts.Count == 2)
                {
                    int pageSizeCmdIndex = pagingInitialIndex + parts[0].Index;
                    int pageNumberCmdIndex = pagingInitialIndex + parts[1].Index;
                    CCmd pageSizeCmd = GetNewCmd(parts[0].Text, pageSizeCmdIndex, false, false);
                    CCmd pageNumberCmd = GetNewCmd(parts[1].Text, pageNumberCmdIndex, false, false);
                    pageSizeCmd.EndIndex = pageSizeCmdIndex + parts[0].Text.Length;
                    pageNumberCmd.EndIndex = pageNumberCmdIndex + parts[1].Text.Length;
                    ((CXPATHCmd)cmd).PageNumberCmd = pageNumberCmd;
                    ((CXPATHCmd)cmd).PageSizeCmd = pageSizeCmd;
                }
            }

            if (paramString != "")
            {
                ArrayList arrParams = GetParserParameters(paramString);
                ((CXPATHCmd)cmd).Parameters = new CCmd[arrParams.Count];

                for (int iParam = 0; iParam < arrParams.Count; iParam++)
                {
                    ParserParam pp = (ParserParam)arrParams[iParam];
                    if ((iParam == 0) && (fromTemplate))
                    {
                        //string sTemplateName = pp.Value.Trim();
                        ////sTemplateName = m_parser.ReplaceParameters(sTemplateName);
                        //RequestTemplateIDForInclude(sTemplateName );
                        //RequestTemplate(this.IDForInclude);
                        //if (this.TextForInclude == null)
                        //    throw new Exception("Could not retrieve template content for TemplateID=" + this.IDForInclude.ToString());
                        //string s_text = this.TextForInclude;
                        string s_text = pp.Value;
                        ((CXPATHCmd)cmd).Parameters[iParam] = GetNewCmd(s_text.Trim(), paramStringInitialIndex + pp.Index, false, true);
                        ((CXPATHCmd)cmd).FromTemplate = true;
                    }
                    else
                        ((CXPATHCmd)cmd).Parameters[iParam] = GetNewCmd(pp.Value.Trim(), paramStringInitialIndex + pp.Index, false, true);
                    ((CXPATHCmd)cmd).Parameters[iParam].EndIndex = paramStringInitialIndex + pp.Index + pp.Value.Length;
                }
            }
            else
                ((CXPATHCmd)cmd).Parameters = new CCmd[0];
            //((CXPATHCmd)cmd).Context = m_context;
            //((CXPATHCmd)cmd).Evaluator = m_evaluator;
            ((CXPATHCmd)cmd).TemplateID = m_templateID;
            ((CXPATHCmd)cmd).JPath = isJPath;


            return cmd;
        }

        private CCmd GetADDPARCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sVal = (parts.Length > 2) ? parts[2]: "";
            string sVarName = (parts.Length > 3) ? parts[3]: "";

            CADDPARCmd cmd = new CADDPARCmd();
            cmd.ParName = sParName;
            cmd.VarName = sVarName;
            cmd.Val = sVal;
            return cmd;
        }
        private CCmd GetGETPARALLCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');
 
            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sVarName = (parts.Length > 2) ? parts[2] : "";
            string sContextName = (parts.Length > 3) ? parts[3] : "";
            string sID = (parts.Length > 4) ? parts[4] : "ID";

            CGETPARCmd cmd = new CGETPARCmd();
            cmd.ParName = sParName;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            cmd.ReplaceAllParams = "1"; 
            return cmd;
        }

        private CCmd GetGETPARStrictCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sVarName = (parts.Length > 2) ? parts[2] : "";
            string sContextName = (parts.Length > 3) ? parts[3] : "";
            string sID = (parts.Length > 4) ? parts[4] : "ID";

            CGETPARCmd cmd = new CGETPARCmd();
            cmd.ParName = sParName;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            cmd.Strict  = "1";
            return cmd;
        }
        private CCmd GetGETPARCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sVarName = (parts.Length > 2) ? parts[2] : "";
            string sContextName = (parts.Length > 3) ? parts[3] : "";
            string sID = (parts.Length > 4) ? parts[4] : "ID";

            CGETPARCmd cmd = new CGETPARCmd();
            cmd.ParName = sParName;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }

        private CCmd GetGETCFGPARCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sCfgClass = (parts.Length > 2) ? parts[2] : "";
            string sCfgName = (parts.Length > 3) ? parts[3] : "";
            string sCfgDef = (parts.Length > 4) ? parts[4] : "";

            CGETCFGPARCmd cmd = new CGETCFGPARCmd();
            cmd.ParName = sParName;
            cmd.CfgClass = sCfgClass;
            cmd.CfgName = sCfgName;
            cmd.CfgDef = sCfgDef;
            return cmd;
        }
        private CCmd GetGETPARREPLACECmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sWhat = (parts.Length > 2) ? parts[2] : "";
            string sWith = (parts.Length > 3) ? parts[3] : "";
            string sVarName = (parts.Length > 4) ? parts[4] : "";
            string sContextName = (parts.Length > 5) ? parts[5] : "";
            string sID = (parts.Length > 6) ? parts[6] : "ID";

            CGETPARCmd cmd = new CGETPARCmd();
            cmd.ParName = sParName;
            cmd.What = sWhat;
            cmd.With = sWith;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetGETDEFPARCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sDefaultVal = (parts.Length > 2) ? parts[2] : "";
            string sVarName = (parts.Length > 3) ? parts[3] : "";
            string sContextName = (parts.Length > 4) ? parts[4] : "";
            string sID = (parts.Length > 5) ? parts[5] : "ID";

            CGETPARCmd cmd = new CGETPARCmd();
            cmd.ParName = sParName;
            cmd.DefaultVal = sDefaultVal ;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetGETITEMPARCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sIndexes = (parts.Length > 2) ? parts[2] : "";
            string sVarName = (parts.Length > 3) ? parts[3] : "";
            string sContextName = (parts.Length > 4) ? parts[4] : "";
            string sID = (parts.Length > 5) ? parts[5] : "ID";

            CGETITEMPARCmd cmd = new CGETITEMPARCmd();
            cmd.ParName = sParName;
            cmd.Indexes = sIndexes;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetFINDITEMPARCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sVal = (parts.Length > 2) ? parts[2] : "";
            string sVarName = (parts.Length > 3) ? parts[3] : "";
            string sContextName = (parts.Length > 4) ? parts[4] : "";
            string sID = (parts.Length > 5) ? parts[5] : "ID";

            CFINDITEMPARCmd cmd = new CFINDITEMPARCmd();
            cmd.ParName = sParName;
            cmd.Val  = sVal;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetDELITEMCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sIndexes = (parts.Length > 1) ? parts[1] : "";
            string sVarName = (parts.Length > 2) ? parts[2] : "";
            string sContextName = (parts.Length > 3) ? parts[3] : "";
            string sID = (parts.Length > 4) ? parts[4] : "ID";

            CDELITEMCmd cmd = new CDELITEMCmd();
            cmd.Indexes = sIndexes;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetAPPENDVALCmd(string txt, int initialIndex)
        {

            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sVal = (parts.Length > 1) ? parts[1] : "";
            string sVarName = (parts.Length > 2) ? parts[2] : "";
            string sContextName = (parts.Length > 3) ? parts[3] : "";
            string sID = (parts.Length > 4) ? parts[4] : "ID";

            CSETVALCmd cmd = new CSETVALCmd();
            cmd.Val = sVal;
            cmd.Append = true;
            if (parts[0] == "APPVAL") cmd.AppendChar = ""; else cmd.AppendChar = "|";
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetAPPENDFLDCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sQueryName = (parts.Length > 1) ? parts[1] : "";
            string sFieldName = (parts.Length > 2) ? parts[2] : "";
            string sVarName = (parts.Length > 3) ? parts[3] : "";
            string sContextName = (parts.Length > 4) ? parts[4] : "";
            string sID = (parts.Length > 5) ? parts[5] : "ID";

            CSETFLDCmd cmd = new CSETFLDCmd();
            cmd.QueryName = sQueryName;
            cmd.FieldName = sFieldName;
            cmd.Append = true;
            if (parts[0] == "APPFLD") cmd.AppendChar = ""; else cmd.AppendChar = "|";
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            if (sFieldName.ToUpper() == "FETCHID")
                ((CSETFLDCmd)cmd).IsFetchID = true;
            else if (parts[2].Trim().ToUpper() == "FETCHID1")
                ((CSETFLDCmd)cmd).IsFetchID1 = true;
            else if (sFieldName.ToUpper() == "ODDEVEN")
                ((CSETFLDCmd)cmd).OddEven = true;
            else if (sFieldName.ToUpper() == "FIRSTROW")
                ((CSETFLDCmd)cmd).IsFirstRow = true;
            else if (sFieldName.ToUpper() == "LASTROW")
                ((CSETFLDCmd)cmd).IsLastRow = true;
            else if (sFieldName.ToUpper() == "COUNT")
                ((CSETFLDCmd)cmd).IsCount = true;
            //((CSETFLDCmd)cmd).Context = m_context;
            //((CSETFLDCmd)cmd).Evaluator = m_evaluator;
            ((CSETFLDCmd)cmd).Parent = GetParentQueryCmd(((CSETFLDCmd)cmd).QueryName);
            if (((CSETFLDCmd)cmd).Parent == null)
                throw new Exception("Command " + txt + " does not have a parent QRY or REP for query " + ((CSETFLDCmd)cmd).QueryName);
            return cmd;
        }
        private CCmd GetDELCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sVarName = (parts.Length > 1) ? parts[1] : "";
            string sContextName = (parts.Length > 2) ? parts[2] : "";
            string sID = (parts.Length > 3) ? parts[3] : "ID";

            CDELCmd cmd = new CDELCmd();
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetDELFROMCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sVarName = (parts.Length > 1) ? parts[1] : "";
            string sContextName = (parts.Length > 2) ? parts[2] : "";
            string sID = (parts.Length > 3) ? parts[3] : "ID";

            CDELCmd cmd = new CDELCmd();
            cmd.DeleteFrom = true;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetDELALLCmd(string txt, int initialIndex)
        {
            CDELCmd cmd = new CDELCmd();
            cmd.DeleteAll = true;
            return cmd;
        }
        
        private CCmd GetSETVALCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sVal = (parts.Length > 1) ? parts[1] : "";
            string sVarName = (parts.Length > 2) ? parts[2] : "";
            string sContextName = (parts.Length > 3) ? parts[3] : "";
            string sID = (parts.Length > 4) ? parts[4] : "ID";

            CSETVALCmd cmd = new CSETVALCmd();
            cmd.Val = sVal ;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetSETNULLVALCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sVal = (parts.Length > 1) ? parts[1] : "";
            string sVarName = (parts.Length > 2) ? parts[2] : "";
            string sContextName = (parts.Length > 3) ? parts[3] : "";
            string sID = (parts.Length > 4) ? parts[4] : "ID";

            CSETVALCmd cmd = new CSETVALCmd();
            cmd.Val = sVal;
            cmd.OnlyNull = true;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetThrowCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            CThrowCmd cmd = new CThrowCmd();
            cmd.ParName = sParName;
            return cmd;
        }
        private CCmd GetFILECmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sInType = (parts.Length > 1) ? parts[1] : "";
            string sInVal = (parts.Length > 2) ? parts[2] : "";
            string sOutType = (parts.Length > 3) ? parts[3] : "";
            string sOutVal = (parts.Length > 4) ? parts[4] : "";

            CFILECmd cmd = new CFILECmd();
            cmd.InType = sInType;
            cmd.InVal = sInVal;
            cmd.OutType = sOutType;
            cmd.OutVal = sOutVal;
            return cmd;
        }
        private CCmd GetSETPAREXTCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sEncode = (parts.Length > 2) ? parts[2] : "";
            string sFormat = (parts.Length > 3) ? parts[3] : "";
            string sVarName = (parts.Length > 4) ? parts[4] : "";
            string sContextName = (parts.Length > 5) ? parts[5] : "";
            string sID = (parts.Length > 6) ? parts[6] : "ID";
  
            CSETPARCmd cmd = new CSETPARCmd();
            cmd.ParName = sParName;
            cmd.EncodeOption = sEncode;
            cmd.Format = sFormat;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            EncodeOption encOpt = GetEncodeOption(parts[2]);
            return cmd;
        }
        private CCmd GetSETNULLPAREXTCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sEncode = (parts.Length > 2) ? parts[2] : "";
            string sFormat = (parts.Length > 3) ? parts[3] : "";
            string sVarName = (parts.Length > 4) ? parts[4] : "";
            string sContextName = (parts.Length > 5) ? parts[5] : "";
            string sID = (parts.Length > 6) ? parts[6] : "ID";

            CSETPARCmd cmd = new CSETPARCmd();
            cmd.ParName = sParName;
            cmd.OnlyNull = true;
            cmd.Format = sFormat;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetSETPARCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sVarName = (parts.Length > 2) ? parts[2] : "";
            string sContextName = (parts.Length > 3) ? parts[3] : "";
            string sID = (parts.Length > 4) ? parts[4] : "ID";
  
            CSETPARCmd cmd = new CSETPARCmd();
            cmd.ParName = sParName;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }
        private CCmd GetSETNULLPARCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sParName = (parts.Length > 1) ? parts[1] : "";
            string sVarName = (parts.Length > 2) ? parts[2] : "";
            string sContextName = (parts.Length > 3) ? parts[3] : "";
            string sID = (parts.Length > 4) ? parts[4] : "ID";

            CSETPARCmd cmd = new CSETPARCmd();
            cmd.ParName = sParName;
            cmd.OnlyNull = true;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            return cmd;
        }

        private CCmd GetSETFLDEXTCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sQueryName = (parts.Length > 1) ? parts[1] : "";
            string sFieldName = (parts.Length > 2) ? parts[2] : "";
            string sEncode = (parts.Length > 3) ? parts[3] : "";
            string sFormat = (parts.Length > 4) ? parts[4] : "";
            string sVarName = (parts.Length > 5) ? parts[5] : "";
            string sContextName = (parts.Length > 6) ? parts[6] : "";
            string sID = (parts.Length > 7) ? parts[7] : "ID";

            CSETFLDCmd cmd = new CSETFLDCmd();
            cmd.QueryName = sQueryName;
            cmd.FieldName = sFieldName;
            cmd.EncodeOption = sEncode;
            cmd.Format = sFormat;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            if (sFieldName.ToUpper() == "FETCHID")
                ((CSETFLDCmd)cmd).IsFetchID = true;
            else if (parts[2].Trim().ToUpper() == "FETCHID1")
                ((CSETFLDCmd)cmd).IsFetchID1 = true;
            else if (sFieldName.ToUpper() == "ODDEVEN")
                ((CSETFLDCmd)cmd).OddEven = true;
            else if (sFieldName.ToUpper() == "FIRSTROW")
                ((CSETFLDCmd)cmd).IsFirstRow = true;
            else if (sFieldName.ToUpper() == "LASTROW")
                ((CSETFLDCmd)cmd).IsLastRow = true;
            else if (sFieldName.ToUpper() == "COUNT")
                ((CSETFLDCmd)cmd).IsCount = true;
            //((CSETFLDCmd)cmd).Context = m_context;
            //((CSETFLDCmd)cmd).Evaluator = m_evaluator;
            ((CSETFLDCmd)cmd).Parent = GetParentQueryCmd(((CSETFLDCmd)cmd).QueryName);
            if (((CSETFLDCmd)cmd).Parent == null)
                throw new Exception("Command " + txt + " does not have a parent QRY or REP for query " + ((CSETFLDCmd)cmd).QueryName);
            return cmd;
        }
        private CCmd GetSETNULLFLDEXTCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sQueryName = (parts.Length > 1) ? parts[1] : "";
            string sFieldName = (parts.Length > 2) ? parts[2] : "";
            string sEncode = (parts.Length > 3) ? parts[3] : "";
            string sFormat = (parts.Length > 4) ? parts[4] : "";
            string sVarName = (parts.Length > 5) ? parts[5] : "";
            string sContextName = (parts.Length > 6) ? parts[6] : "";
            string sID = (parts.Length > 7) ? parts[7] : "ID";

            CSETFLDCmd cmd = new CSETFLDCmd();
            cmd.OnlyNull = true;
            cmd.QueryName = sQueryName;
            cmd.FieldName = sFieldName;
            cmd.EncodeOption = sEncode;
            cmd.Format = sFormat;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            if (sFieldName.ToUpper() == "FETCHID")
                ((CSETFLDCmd)cmd).IsFetchID = true;
            else if (parts[2].Trim().ToUpper() == "FETCHID1")
                ((CSETFLDCmd)cmd).IsFetchID1 = true;
            else if (sFieldName.ToUpper() == "ODDEVEN")
                ((CSETFLDCmd)cmd).OddEven = true;
            else if (sFieldName.ToUpper() == "FIRSTROW")
                ((CSETFLDCmd)cmd).IsFirstRow = true;
            else if (sFieldName.ToUpper() == "LASTROW")
                ((CSETFLDCmd)cmd).IsLastRow = true;
            else if (sFieldName.ToUpper() == "COUNT")
                ((CSETFLDCmd)cmd).IsCount = true;
            //((CSETFLDCmd)cmd).Context = m_context;
            //((CSETFLDCmd)cmd).Evaluator = m_evaluator;
            ((CSETFLDCmd)cmd).Parent = GetParentQueryCmd(((CSETFLDCmd)cmd).QueryName);
            if (((CSETFLDCmd)cmd).Parent == null)
                throw new Exception("Command " + txt + " does not have a parent QRY or REP for query " + ((CSETFLDCmd)cmd).QueryName);
            return cmd;
        }
        private CCmd GetSETFLDCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sQueryName = (parts.Length > 1) ? parts[1] : "";
            string sFieldName = (parts.Length > 2) ? parts[2] : "";
            string sVarName = (parts.Length > 3) ? parts[3] : "";
            string sContextName = (parts.Length > 4) ? parts[4] : "";
            string sID = (parts.Length > 5) ? parts[5] : "ID";

            CSETFLDCmd cmd = new CSETFLDCmd();
            cmd.QueryName = sQueryName;
            cmd.FieldName = sFieldName;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            if (sFieldName.ToUpper() == "FETCHID")
                ((CSETFLDCmd)cmd).IsFetchID = true;
            else if (parts[2].Trim().ToUpper() == "FETCHID1")
                ((CSETFLDCmd)cmd).IsFetchID1 = true;
            else if (sFieldName.ToUpper() == "ODDEVEN")
                ((CSETFLDCmd)cmd).OddEven = true;
            else if (sFieldName.ToUpper() == "FIRSTROW")
                ((CSETFLDCmd)cmd).IsFirstRow = true;
            else if (sFieldName.ToUpper() == "LASTROW")
                ((CSETFLDCmd)cmd).IsLastRow = true;
            else if (sFieldName.ToUpper() == "COUNT")
                ((CSETFLDCmd)cmd).IsCount = true;
            //((CSETFLDCmd)cmd).Context = m_context;
            //((CSETFLDCmd)cmd).Evaluator = m_evaluator;
            ((CSETFLDCmd)cmd).Parent = GetParentQueryCmd(((CSETFLDCmd)cmd).QueryName);
            if (((CSETFLDCmd)cmd).Parent == null)
                throw new Exception("Command " + txt + " does not have a parent QRY or REP for query " + ((CSETFLDCmd)cmd).QueryName);
            return cmd;
        }
        private CCmd GetSETNULLFLDCmd(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sQueryName = (parts.Length > 1) ? parts[1] : "";
            string sFieldName = (parts.Length > 2) ? parts[2] : "";
            string sVarName = (parts.Length > 3) ? parts[3] : "";
            string sContextName = (parts.Length > 4) ? parts[4] : "";
            string sID = (parts.Length > 5) ? parts[5] : "ID";

            CSETFLDCmd cmd = new CSETFLDCmd();
            cmd.OnlyNull = true;
            cmd.QueryName = sQueryName;
            cmd.FieldName = sFieldName;
            cmd.VarName = sVarName;
            cmd.ContextName = sContextName;
            cmd.ID = sID;
            if (sFieldName.ToUpper() == "FETCHID")
                ((CSETFLDCmd)cmd).IsFetchID = true;
            else if (parts[2].Trim().ToUpper() == "FETCHID1")
                ((CSETFLDCmd)cmd).IsFetchID1 = true;
            else if (sFieldName.ToUpper() == "ODDEVEN")
                ((CSETFLDCmd)cmd).OddEven = true;
            else if (sFieldName.ToUpper() == "FIRSTROW")
                ((CSETFLDCmd)cmd).IsFirstRow = true;
            else if (sFieldName.ToUpper() == "LASTROW")
                ((CSETFLDCmd)cmd).IsLastRow = true;
            else if (sFieldName.ToUpper() == "COUNT")
                ((CSETFLDCmd)cmd).IsCount = true;
            //((CSETFLDCmd)cmd).Context = m_context;
            //((CSETFLDCmd)cmd).Evaluator = m_evaluator;
            ((CSETFLDCmd)cmd).Parent = GetParentQueryCmd(((CSETFLDCmd)cmd).QueryName);
            if (((CSETFLDCmd)cmd).Parent == null)
                throw new Exception("Command " + txt + " does not have a parent QRY or REP for query " + ((CSETFLDCmd)cmd).QueryName);
            return cmd;
        }



        CCmd GetREPCmd(string txt, int initialIndex, bool isDataTable)
        {
            CCmd cmd = new CREPCmd();
            ((CREPCmd)cmd).DataTable = isDataTable;

            Match mTemp = wordREx.Match(txt.Substring(4 + (isDataTable ? 4 : 0)));
            if (mTemp.Success)
                ((CREPCmd)cmd).QueryName = mTemp.Value;

            string paging = "";
            int pagingInitialIndex = -1;

            string paramString = "";
            int paramStringInitialIndex = -1;

            int io1 = txt.IndexOf("(");
            int io2 = txt.IndexOf(")");
            
            if (io1 > 0 && io2 != -1 && io2 > io1)
            {
                string list = txt.Substring(io1 + 1, io2 - io1 - 1);
                int index = initialIndex + SEPARATOR_LENGTH + io1 + 1;
                if (txt[io1 - 1] == '.')
                {
                    paging = list;
                    pagingInitialIndex = index;
                }
                else
                {
                    paramString = list;
                    paramStringInitialIndex = index;
                    int io21 = txt.IndexOf("(", io2 + 1);
                    if (io21 != -1)
                    {
                        int io22 = txt.IndexOf(")", io21 + 1);
                        if (io22 != -1)
                        {
                            paging = txt.Substring(io21 + 1, io22 - io21 - 1);
                            pagingInitialIndex = initialIndex + SEPARATOR_LENGTH + io21 + 1;
                        }
                        else
                            throw new Exception("The REP command at line " + GetLine(initialIndex).ToString() + " is missing a bracket");
                    }
                }
            }

            if (paging != "")
            {
                List<ParseUtils.ListParameter> parts = ParseUtils.GetParameterList(paging, '\'', ',', false);

                if (parts.Count == 2)
                {
                    int pageSizeCmdIndex = pagingInitialIndex + parts[0].Index;
                    int pageNumberCmdIndex = pagingInitialIndex + parts[1].Index;
                    CCmd pageSizeCmd = GetNewCmd(parts[0].Text, pageSizeCmdIndex, false, false);
                    CCmd pageNumberCmd = GetNewCmd(parts[1].Text, pageNumberCmdIndex, false, false);
                    pageSizeCmd.EndIndex = pageSizeCmdIndex + parts[0].Text.Length;
                    pageNumberCmd.EndIndex = pageNumberCmdIndex + parts[1].Text.Length;
                    ((CREPCmd)cmd).PageNumberCmd = pageNumberCmd;
                    ((CREPCmd)cmd).PageSizeCmd = pageSizeCmd;
                }
            }

            if (paramString != "")
            {
                ArrayList arrParams = GetParserParameters(paramString);
                ((CREPCmd)cmd).Parameters = new CCmd[arrParams.Count];

                for (int iParam = 0; iParam < arrParams.Count; iParam++)
                {
                    ParserParam pp = (ParserParam)arrParams[iParam];
                    ((CREPCmd)cmd).Parameters[iParam] = GetNewCmd(pp.Value.Trim(), paramStringInitialIndex + pp.Index, false, true);
                    ((CREPCmd)cmd).Parameters[iParam].EndIndex = paramStringInitialIndex + pp.Index + pp.Value.Length;
                }
            }
            else
                ((CREPCmd)cmd).Parameters = new CCmd[0];
            //((CREPCmd)cmd).Context = m_context;
            //((CREPCmd)cmd).Evaluator = m_evaluator;
            ((CREPCmd)cmd).TemplateID = m_templateID ;

            return cmd;
        }

        CCmd GetQRYCmd(string txt, int initialIndex, bool isDataTable)
        {
            CCmd cmd = new CQRYCmd();

            ((CQRYCmd)cmd).DataTable = isDataTable;

            Match mTemp = wordREx.Match(txt.Substring(4+(isDataTable ? 4 : 0)));
            if (mTemp.Success)
                ((CQRYCmd)cmd).QueryName = mTemp.Value;
            string prametersString = txt.Substring(4 + mTemp.Value.Length);
            int offset = initialIndex + 4 + mTemp.Value.Length + SEPARATOR_LENGTH;

            ArrayList arrParams = GetParserParameters(prametersString);
            ((CQRYCmd)cmd).Parameters = new CCmd[arrParams.Count];


            for (int iParam = 0; iParam < arrParams.Count; iParam++)
            {
                ParserParam pp = (ParserParam)arrParams[iParam];
                ((CQRYCmd)cmd).Parameters[iParam] = GetNewCmd(pp.Value.Trim(), offset + pp.Index, false, true);
                ((CQRYCmd)cmd).Parameters[iParam].EndIndex = offset + pp.Index + pp.Value.Length;
            }
            //((CQRYCmd)cmd).Context = m_context;
            //((CQRYCmd)cmd).Evaluator = m_evaluator;
            ((CQRYCmd)cmd).TemplateID = m_templateID ;

            return cmd;
        }
        CCmd GetJINSCmd(string txt) //JINS.<par_name>.<json_source>.[<jpath>].<attr_name>.<json_obj>
        {
            CCmd cmd = new CJSETCmd();
            ((CJSETCmd)cmd).JOP = "INS";
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            ((CJSETCmd)cmd).ParameterName = parts[1];
            if (parts.GetLength(0) >= 3) ((CJSETCmd)cmd).Source = parts[2];
            if (parts.GetLength(0) >= 4) ((CJSETCmd)cmd).JPath = parts[3];
            if (parts.GetLength(0) >= 5) ((CJSETCmd)cmd).JAttr = parts[4];
            if (parts.GetLength(0) >= 6) ((CJSETCmd)cmd).JObj = parts[5];
            return cmd;
        }
        CCmd GetJAPPCmd(string txt) //JAPP.<par_name>.<json_source>.[<jpath>].<json_obj/array>.[<index>]
        {
            CCmd cmd = new CJSETCmd();
            ((CJSETCmd)cmd).JOP = "APP";
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            ((CJSETCmd)cmd).ParameterName = parts[1];
            if (parts.GetLength(0) >= 3) ((CJSETCmd)cmd).Source = parts[2];
            if (parts.GetLength(0) >= 4) ((CJSETCmd)cmd).JPath = parts[3];
            if (parts.GetLength(0) >= 5) ((CJSETCmd)cmd).JObj = parts[4];
            if (parts.GetLength(0) >= 6) ((CJSETCmd)cmd).JAttr = parts[5];
            return cmd;
        }
        CCmd GetJUPDCmd(string txt) //JUPD.<par_name>.<json_source>.[<jpath>].[<json_obj>]
        {
            CCmd cmd = new CJSETCmd();
            ((CJSETCmd)cmd).JOP = "UPD";
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            ((CJSETCmd)cmd).ParameterName = parts[1];
            if (parts.GetLength(0) >= 3) ((CJSETCmd)cmd).Source = parts[2];
            if (parts.GetLength(0) >= 4) ((CJSETCmd)cmd).JPath = parts[3];
            if (parts.GetLength(0) >= 5) ((CJSETCmd)cmd).JObj = parts[4];
            return cmd;
        }
        CCmd GetJDELCmd(string txt) //JDEL.<par_name>.<json_source>.[<jpath>]
        {
            CCmd cmd = new CJSETCmd();
            ((CJSETCmd)cmd).JOP = "DEL";
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            ((CJSETCmd)cmd).ParameterName = parts[1];
            if (parts.GetLength(0) >= 3) ((CJSETCmd)cmd).Source = parts[2];
            string jPath = "";
            if (parts.GetLength(0) >= 4) jPath = parts[3];
            for (int i = 4; i < parts.Length; i++)
                jPath += "." + parts[i].Replace(".", "\\.");
            ((CJSETCmd)cmd).JPath = jPath;
            return cmd;
        }

        CCmd GetPARCmd(string txt)
        {
            CCmd cmd = new CPARCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            ((CPARCmd)cmd).ParameterName = parts[1];
            if (parts.GetLength(0) >= 3)
            {
                EncodeOption encOpt = GetEncodeOption(parts[2]);
                ((CPARCmd)cmd).EncodeOption = encOpt;
                if (parts.GetLength(0) >= 4) ((CPARCmd)cmd).Format = parts[3];
            }

            //if (((CPARCmd)cmd).EncodeOption == EncodeOption.None && m_parseForRedirect)
            //((CPARCmd)cmd).EncodeOption = EncodeOption.URLEncode;
            return cmd;
        }
        CCmd GetJPARCmd(string txt) //JPAR.<par_name>.<json_source>.[<jpath>]
        {
            CCmd cmd = new CJPARCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            ((CJPARCmd)cmd).ParameterName = parts[1];
            if (parts.GetLength(0) >= 3) ((CJPARCmd)cmd).Source = parts[2];
            string jPath = "";
            if (parts.GetLength(0) >= 4) jPath = parts[3];
            for (int i = 4; i < parts.Length; i++)
                jPath += "." + parts[i].Replace(".", "\\.");
            ((CJPARCmd)cmd).JPath = jPath;
            return cmd;

        }
        CCmd GetJDATACmd(string txt) //JDATA.[<par_name>].<_select_>.[<_conn_>].[<error_par>]
        {
            CCmd cmd = new CJDATACmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            ((CJDATACmd)cmd).ParameterName = parts[1];
            if (parts.GetLength(0) >= 3) ((CJDATACmd)cmd).Select = parts[2];
            if (parts.GetLength(0) >= 4) ((CJDATACmd)cmd).Conn = parts[3];
            if (parts.GetLength(0) >= 5) ((CJDATACmd)cmd).ErrorParamName = parts[4];
            if (parts[0] == "JDATAP") ((CJDATACmd)cmd).isParSource = "1";
            return cmd;
        }

        CCmd GetJLOOPCmd(string txt) //JLOOP.<par_name>.<json_source>.[<jpath>].[<page_size>].[<page_nr>].[<error_par>]
        {
            CCmd cmd = new CJLOOPCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            ((CJLOOPCmd)cmd).ParameterName = parts[1];
            if (parts.GetLength(0) >= 3) ((CJLOOPCmd)cmd).Source = parts[2];
            if (parts.GetLength(0) >= 4) ((CJLOOPCmd)cmd).JPath = parts[3];
            if (parts.GetLength(0) >= 5) ((CJLOOPCmd)cmd).PageSize = parts[4];
            if (parts.GetLength(0) >= 6) ((CJLOOPCmd)cmd).PageNr = parts[5];
            if (parts.GetLength(0) >= 7) ((CJLOOPCmd)cmd).ErrorParamName = parts[6];
            if (parts[0] == "JLOOPP") ((CJLOOPCmd)cmd).isParSource = "1";
            return cmd;
        }
        CCmd GetJPROPCmd(string txt) //CJPROP.<par_name>.<json_source>.[<jpath>]
        {
            CCmd cmd = new CJPROPCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            ((CJPROPCmd)cmd).ParameterName = parts[1];
            if (parts.GetLength(0) >= 3) ((CJPROPCmd)cmd).Source = parts[2];
            string jPath = "";
            if (parts.GetLength(0) >= 4) jPath = parts[3];
            for (int i = 4; i < parts.Length; i++)
                jPath += "." + parts[i].Replace(".", "\\.");
            ((CJPROPCmd)cmd).JPath = jPath;
            return cmd;
        }
        CCmd GetFUNCCommand(string txt)
        {
            //#FUNC.<Out Param>.<Functie>.<Format>.<Val1>.<Val2>.<Val3>.<Val4>.<Val5>.<Val6>#
            CCmd cmd = new CFUNCCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            if (parts.Length > 1) ((CFUNCCmd)cmd).ParameterName = parts[1];
            if (parts.Length > 2) ((CFUNCCmd)cmd).Operation = parts[2];
            if (parts.Length > 3) ((CFUNCCmd)cmd).Format = parts[3];
            if (parts.Length > 4) ((CFUNCCmd)cmd).Value1 = parts[4];
            if (parts.Length > 5) ((CFUNCCmd)cmd).Value2 = parts[5];
            if (parts.Length > 6) ((CFUNCCmd)cmd).Value3 = parts[6];
            if (parts.Length > 7) ((CFUNCCmd)cmd).Value4 = parts[7];
            if (parts.Length > 8) ((CFUNCCmd)cmd).Value5 = parts[8];
            if (parts.Length > 9) ((CFUNCCmd)cmd).Value6 = parts[9];
            return cmd;
        }

        CCmd GetFLDCmd(string txt)
        {
            CCmd cmd = new CFLDCmd();

            List<ParseUtils.ListParameter> listParams = ParseUtils.GetParameterList(txt, '\'', '.', false);

            string[] parts = ParseUtils.StringArrayFromParamList(listParams);
            if (parts.Length >= 3)
            {
                ((CFLDCmd)cmd).QueryName = parts[1];
                if (parts[2].Trim().ToUpper() == "FETCHID")
                    ((CFLDCmd)cmd).IsFetchID = true;
                else if (parts[2].Trim().ToUpper() == "FETCHID1")
                    ((CFLDCmd)cmd).IsFetchID1 = true;
                else if (parts[2].Trim().ToUpper() == "ODDEVEN")
                    ((CFLDCmd)cmd).OddEven = true;
                else if (parts[2].Trim().ToUpper() == "FIRSTROW")
                    ((CFLDCmd)cmd).IsFirstRow = true;
                else if (parts[2].Trim().ToUpper() == "LASTROW")
                    ((CFLDCmd)cmd).IsLastRow = true;
                else if (parts[2].Trim().ToUpper() == "COUNT")
                {
                    ((CFLDCmd)cmd).IsCount = true;
                    if (parts.Length > 3)
                    {
                        int pageSizeInitialIndex = -1;

                        ((CFLDCmd)cmd).PageSizeCmd = GetNewCmd(parts[3], pageSizeInitialIndex, false, false);
                    }
                }
                else
                {
                    ((CFLDCmd)cmd).FieldName = parts[2];
                    if (parts.Length >= 4)
                    {
                        EncodeOption encOpt = GetEncodeOption(parts[3]);
                        ((CFLDCmd)cmd).EncodeOption = encOpt;
                        if (parts.Length >= 5) ((CFLDCmd)cmd).Format = parts[4];
                    }
                }
                //((CFLDCmd)cmd).Context = m_context;
                //((CFLDCmd)cmd).Evaluator = m_evaluator;
                ((CFLDCmd)cmd).Parent = GetParentQueryCmd(((CFLDCmd)cmd).QueryName);
                if (((CFLDCmd)cmd).Parent == null)
                    throw new Exception("Command " + txt + " does not have a parent QRY or REP for query " + ((CFLDCmd)cmd).QueryName);
            }

            //if (((CFLDCmd)cmd).EncodeOption == EncodeOption.None && m_parseForRedirect)
            //((CFLDCmd)cmd).EncodeOption = EncodeOption.URLEncode;


            return cmd;

        }

        CCmd GetFLDBINCmd(string txt)
        {
            CCmd cmd = new CFLDCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            if (parts.Length == 3)
            {
                ((CFLDCmd)cmd).QueryName = parts[1];
                ((CFLDCmd)cmd).FieldName = parts[2];
                //((CFLDCmd)cmd).Context = m_context;
                //((CFLDCmd)cmd).Evaluator = m_evaluator;
                ((CFLDCmd)cmd).Parent = GetParentQueryCmd(((CFLDCmd)cmd).QueryName);
                if (((CFLDCmd)cmd).Parent == null)
                    throw new Exception("Command " + txt + " does not have a parent QRY or REP for query " + ((CFLDCmd)cmd).QueryName);

                ((CFLDCmd)cmd).BinaryField = true;
            }

            return cmd;
        }
        CCmd GetIncludeOnceCommand(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            bool bIgnore = (parts.Length > 2) ? (parts[2] == "1" ? true : false) : false;
            CCmd cmd = new CIncludeOnceCmd();
            ((CIncludeOnceCmd)cmd).TemplateName = parts [1];
            ((CIncludeOnceCmd)cmd).Ignore = bIgnore ;
            return cmd;
        }


        CCmd GetParIncludeCommand(string txt, int initialIndex)
        {
            CCmd cmd = new CIncludeCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            ((CIncludeCmd)cmd).ParName = txt.Substring("PARINCLUDE".Length + 1, txt.IndexOf('(') - "PARINCLUDE".Length - 1);
            string paramList = txt.Substring(txt.IndexOf('(') + 1);
            int lio = paramList.LastIndexOf(')');
            paramList = paramList.Remove(lio);
            int offset = initialIndex + txt.IndexOf('(') + 2;// 1 because i pass over (

            MatchCollection mcParams = ifParamREx.Matches(paramList);

            foreach (Match m in mcParams)
            {
                int nNewIndex = offset + m.Index;
                CCmd paramCmd = GetNewCmd(m.Value, nNewIndex, false, true);
                paramCmd.EndIndex = offset + m.Index + m.Value.Length;
                ((CIncludeCmd)cmd).ParamCmds.Add(paramCmd);
            }
            return cmd;
        }

        CCmd GetIncludeCommand(string txt, int initialIndex)
        {
            CCmd cmd = new CIncludeCmd();
            if (txt.ToUpper().StartsWith("INCLUDE"))
            {
                string paramList = txt.Substring(txt.IndexOf('(') + 1);
                int lio = paramList.LastIndexOf(')');
                paramList = paramList.Remove(lio);
                int offset = initialIndex + txt.IndexOf('(') + 2;// 1 because i pass over (

                MatchCollection mcParams = ifParamREx.Matches(paramList);

                foreach (Match m in mcParams)
                {
                    int nNewIndex = offset + m.Index;
                    CCmd paramCmd = GetNewCmd(m.Value, nNewIndex, false, true);
                    paramCmd.EndIndex = offset + m.Index + m.Value.Length;
                    ((CIncludeCmd)cmd).ParamCmds.Add(paramCmd);
                }
            }
            else //UDF command
            {
                int ioBr1 = txt.IndexOf("(");
                int ioBr2 = txt.LastIndexOf(")");
                if (ioBr1 == -1 || ioBr2 == -1)
                    throw new Exception(string.Format("The UDF command at line {0} is missing one or both brackets", GetLine(initialIndex)));
                string firstPart = txt.Substring(0, ioBr1);
                string templateName = firstPart.Replace("UDF.", "");

                string paramList = txt.Substring(ioBr1 + 1, ioBr2 - ioBr1 - 1);

                int offset = initialIndex + ioBr1 + 2;

                MatchCollection mcParams = ifParamREx.Matches(paramList);

                foreach (Match m in mcParams)
                {
                    int nNewIndex = offset + m.Index;
                    CCmd paramCmd = GetNewCmd(m.Value, nNewIndex, false, true);
                    paramCmd.EndIndex = offset + m.Index + m.Value.Length;
                    ((CIncludeCmd)cmd).ParamCmds.Add(paramCmd);
                }

                (cmd as CIncludeCmd).IsUDF = true;
                (cmd as CIncludeCmd).UDFTemplateName = templateName;
            }

            return cmd;
        }

        CCmd GetUseParameters(string txt)
        {
            CCmd cmd = new CUseParameters ();
            return cmd;
        }


        private CCmd GetImportFldCommand(string txt, int initialIndex)
        {
            //txt = txt.Replace("\\\\", "<backslash>"); txt = txt.Replace("\\.", "<tilda>");
            //string[] parts = txt.Split('.');
            string[] parts = Utils.MySplit(txt, '.');

            string sQueryName = (parts.Length > 1) ? parts[1] : "";
            string sFieldName = (parts.Length > 2) ? parts[2] : "";
            string sTemplateName = (parts.Length > 3) ? parts[3] : "";
            CImportFldCmd cmd = new CImportFldCmd();
            if (parts.Length > 4)
            {
                int iConnID = -1;
                if (int.TryParse(parts[4], out iConnID))
                    ((CImportFldCmd)cmd).ConnID = iConnID;
                else ((CImportFldCmd)cmd).Conn = parts[4];
            }

            cmd.QueryName = sQueryName;
            cmd.FieldName = sFieldName;
            cmd.TemplateName  = sTemplateName ;
            if (sFieldName.ToUpper() == "FETCHID")
                ((CImportFldCmd)cmd).IsFetchID = true;
            else if (parts[2].Trim().ToUpper() == "FETCHID1")
                ((CImportFldCmd)cmd).IsFetchID1 = true;
            else if (sFieldName.ToUpper() == "ODDEVEN")
                ((CImportFldCmd)cmd).OddEven = true;
            else if (sFieldName.ToUpper() == "FIRSTROW")
                ((CImportFldCmd)cmd).IsFirstRow = true;
            else if (sFieldName.ToUpper() == "LASTROW")
                ((CImportFldCmd)cmd).IsLastRow = true;
            else if (sFieldName.ToUpper() == "COUNT")
                ((CImportFldCmd)cmd).IsCount = true;
            //((CImportFldCmd)cmd).Context = m_context;
            //((CImportFldCmd)cmd).Evaluator = m_evaluator;
            ((CImportFldCmd)cmd).Parent = GetParentQueryCmd(((CImportFldCmd)cmd).QueryName);
            if (((CImportFldCmd)cmd).Parent == null)
                throw new Exception("Command " + txt + " does not have a parent QRY or REP for query " + ((CImportFldCmd)cmd).QueryName);
            return cmd;
        }

        CCmd GetImportSetCommand(string txt, int initialIndex)
        {
            CCmd cmd = new CImportCmd();
            ((CImportCmd)cmd).IsSet = true;
            string stxt = txt;
            if (stxt.IndexOf('(') > 0) stxt = stxt.Substring(0, stxt.IndexOf('('));

            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(stxt, '\'', '.', false));
            if (parts.Length > 1)
            {
                int iConnID = -1;
                if (int.TryParse(parts[1], out iConnID))
                    ((CImportCmd)cmd).ConnID = iConnID;
                else ((CImportCmd)cmd).Conn = parts[1];
            }
            if (parts.Length > 2) if (parts[2].EndsWith("Full")) ((CImportCmd)cmd).TemplateName = "ImportSetFull";
            if (parts.Length > 2) if (parts[2] == "Full") ((CImportCmd)cmd).TemplateName = "ImportSetFull";
            if (parts.Length > 2) if (parts[2] == "FULL") ((CImportCmd)cmd).TemplateName = "ImportSetFull";
            if (parts.Length > 2) if (parts[2] == "full") ((CImportCmd)cmd).TemplateName = "ImportSetFull";
            if (parts.Length > 2) if (parts[2] == "ALL") ((CImportCmd)cmd).TemplateName = "ImportSetFull";

            string paramList = txt.Substring(txt.IndexOf('(') + 1);
            int lio = paramList.LastIndexOf(')');
            paramList = paramList.Remove(lio);
            int offset = initialIndex + txt.IndexOf('(') + 2;// 1 because i pass over (

            MatchCollection mcParams = ifParamREx.Matches(paramList);

            foreach (Match m in mcParams)
            {
                int nNewIndex = offset + m.Index;
                CCmd paramCmd = GetNewCmd(m.Value, nNewIndex, false, true);
                paramCmd.EndIndex = offset + m.Index + m.Value.Length;
                ((CImportCmd)cmd).ParamCmds.Add(paramCmd);
            }

            return cmd;
        }
        CCmd GetImportCommand(string txt, int initialIndex)
        {
            CCmd cmd = new CImportCmd();
            string stxt = txt;
            if (stxt.IndexOf('(') > 0) stxt = stxt.Substring(0, stxt.IndexOf('('));

            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(stxt, '\'', '.', false));
            if (parts.Length > 1) ((CImportCmd)cmd).TemplateName = parts[1];
            //((CImportCmd)cmd).ConnID = -1;
            if (parts.Length > 2)
            {
                int iConnID = -1;
                if (int.TryParse(parts[2], out iConnID))
                    ((CImportCmd)cmd).ConnID = iConnID;
                else ((CImportCmd)cmd).Conn = parts[2];
            }

            string paramList = txt.Substring(txt.IndexOf('(') + 1);
            int lio = paramList.LastIndexOf(')');
            paramList = paramList.Remove(lio);
            int offset = initialIndex + txt.IndexOf('(') + 2;// 1 because i pass over (

            MatchCollection mcParams = ifParamREx.Matches(paramList);

            foreach (Match m in mcParams)
            {
                int nNewIndex = offset + m.Index;
                CCmd paramCmd = GetNewCmd(m.Value, nNewIndex, false, true);
                paramCmd.EndIndex = offset + m.Index + m.Value.Length;
                ((CImportCmd)cmd).ParamCmds.Add(paramCmd);
            }

            return cmd;
        }
        CCmd GetExportCommand(string txt)
        {
            CCmd cmd = new CExportCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            if (parts.Length > 1) ((CExportCmd)cmd).TemplateName = parts[1];
            //((CExportCmd)cmd).ConnID = -1;
            if (parts.Length > 2)
            {
                int iConnID = -1;
                if (int.TryParse(parts[2], out iConnID))
                    ((CExportCmd)cmd).ConnID = iConnID;
                else ((CExportCmd)cmd).Conn = parts[2];
                //if (!int.TryParse(parts[2], out iConnID))
                //{
                //    throw new Exception("Parameter ConnID is incorrectly specified : '" + parts[2] + "';");
                //}
                //else
                //    ((CExportCmd)cmd).ConnID = iConnID;
            }
            if (parts.Length > 3) ((CExportCmd)cmd).EncodeOption  = GetEncodeOption(parts[3]);
            return cmd;
        }
        CCmd GetTransferCommand(string txt)
        {
            CCmd cmd = new CTransferCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            if (parts.Length < 5)
                throw new Exception("Transfer command invalid! use: #TRANSFER.<source_template>.<source_connid>.<target_template>.<target_connid>#");

            ((CTransferCmd)cmd).SourceTemplateName = parts[1];
            int iConnID = -1;
            if (int.TryParse(parts[2], out iConnID))
                ((CTransferCmd)cmd).SourceConnID = iConnID;
            else ((CTransferCmd)cmd).SourceConn = parts[2];
            //if (!int.TryParse(parts[2], out iConnID))
            //{
            //    throw new Exception("Parameter SourceConnID is incorrectly specified : '" + parts[2] + "';");
            //}
            //else
            //    ((CTransferCmd)cmd).SourceConnID = iConnID;
            ((CTransferCmd)cmd).TargetTemplateName = parts[3];
            if (int.TryParse(parts[4], out iConnID))
                ((CTransferCmd)cmd).TargetConnID = iConnID;
            else ((CTransferCmd)cmd).TargetConn = parts[4];
            //if (!int.TryParse(parts[4], out iConnID))
            //{
            //    throw new Exception("Parameter TargetConnID is incorrectly specified : '" + parts[4] + "';");
            //}
            //else
            //    ((CTransferCmd)cmd).TargetConnID = iConnID;
            return cmd;
        }
        CCmd GetTransferLikeCommand(string txt)
        {
            //pattern.sourceconn.destconn
            CCmd cmd = new CTransferCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            if (parts.Length < 4)
                throw new Exception("Transfer command invalid! use: #TRANSFERLIKE.<template_pattern>.<source_connid>.<target_connid>#");
            ((CTransferCmd)cmd).TransferLike = true;
            ((CTransferCmd)cmd).SourceTemplateName = parts[1];
            int iConnID = -1;
            if (int.TryParse(parts[2], out iConnID))
                ((CTransferCmd)cmd).SourceConnID = iConnID;
            else ((CTransferCmd)cmd).SourceConn = parts[2];
            if (int.TryParse(parts[3], out iConnID))
                ((CTransferCmd)cmd).TargetConnID = iConnID;
            else ((CTransferCmd)cmd).TargetConn = parts[3];
            //if (!int.TryParse(parts[4], out iConnID))
            //{
            //    throw new Exception("Parameter TargetConnID is incorrectly specified : '" + parts[4] + "';");
            //}
            //else
            //    ((CTransferCmd)cmd).TargetConnID = iConnID;
            return cmd;
        }
        CCmd GetDeleteTemplateCommand(string txt)
        {
            CCmd cmd = new CDeleteTemplateCmd();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));
            if (parts.Length > 1) ((CDeleteTemplateCmd)cmd).TemplateName = parts[1];
            if (parts.Length > 2)
            {
                int iConnID = -1;
                if (int.TryParse(parts[2], out iConnID))
                    ((CDeleteTemplateCmd)cmd).ConnID = iConnID;
                else ((CDeleteTemplateCmd)cmd).Conn = parts[2];
            }
            return cmd;
        }

        CCmd GetCfgCommand(string txt)
        {
            CCmd cmd = new CCgfCmd ();
            string[] parts = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(txt, '\'', '.', false));

            if (parts.Length > 4)
            {
                ((CCgfCmd)cmd).CfgClass = parts[1];
                ((CCgfCmd)cmd).CfgName = parts[2];
                ((CCgfCmd)cmd).CfgDefault = parts[3];
                ((CCgfCmd)cmd).EncodeOption = GetEncodeOption(parts[4]);
            }
            else if (parts.Length > 3)
            {
                ((CCgfCmd)cmd).CfgClass = parts[1];
                ((CCgfCmd)cmd).CfgName = parts[2];
                ((CCgfCmd)cmd).CfgDefault = parts[3];
            }
            else if (parts.Length > 2)
            {
                ((CCgfCmd)cmd).CfgClass = parts[1];
                ((CCgfCmd)cmd).CfgName = parts[2];
            }
            else if (parts.Length > 1)
            {
                ((CCgfCmd)cmd).CfgName = parts[1];
            }
            return cmd;
        }

        bool VerifyClosingTag(Match m)
        {
            CCmd comm = (CCmd)m_stack.Peek();
            string tag = m.Value.Replace("#", "");
            if (tag.ToUpper() == "ENDIF" && !(comm is CIFCmd))
                return false;
            if (tag.ToUpper().StartsWith("ENDQRY."))
            {
                if (!(comm is CQRYCmd))
                    return false;
                string queryName = tag.Substring(tag.IndexOf(".") + 1);
                if (((CQRYCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
                    return false;
            }

            if (tag.ToUpper().StartsWith("ENDREP."))
            {
                if (!(comm is CREPCmd))
                    return false;
                string queryName = tag.Substring(tag.IndexOf(".") + 1);
                if (((CREPCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
                    return false;
            }

            if (tag.ToUpper().StartsWith("ENDXPATH."))
            {
                if (!(comm is CXPATHCmd ))
                    return false;
                string queryName = tag.Substring(tag.IndexOf(".") + 1);
                if (((CXPATHCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
                    return false;
            }

            if (tag.ToUpper().StartsWith("ENDJPATH."))
            {
                if (!(comm is CXPATHCmd))
                    return false;
                string queryName = tag.Substring(tag.IndexOf(".") + 1);
                if (((CXPATHCmd)comm).QueryName.ToUpper() != queryName.ToUpper())
                    return false;
            }
            if (tag.ToUpper().StartsWith("ENDJLOOP."))
            {
                if (!(comm is CJLOOPCmd ))
                    return false;
                string queryName = tag.Substring(tag.IndexOf(".") + 1);
                if (((CJLOOPCmd)comm).ParameterName.ToUpper() != queryName.ToUpper())
                    return false;
            }
            if (tag.ToUpper().StartsWith("ENDJPROP."))
            {
                if (!(comm is CJPROPCmd ))
                    return false;
                string queryName = tag.Substring(tag.IndexOf(".") + 1);
                if (((CJPROPCmd)comm).ParameterName.ToUpper() != queryName.ToUpper())
                    return false;
            }
            return true;
        }


        CCmd GetParentQueryCmd(string queryName)
        {
            object[] arrStack = m_stack.ToArray();
            for (int i = arrStack.GetLength(0) - 1; i >= 0; i--)
            {
                if (arrStack[i] is CQRYCmd && ((CQRYCmd)arrStack[i]).QueryName.ToUpper() == queryName.ToUpper())
                    return (CCmd)arrStack[i];
                else if (arrStack[i] is CREPCmd && ((CREPCmd)arrStack[i]).QueryName.ToUpper() == queryName.ToUpper())
                    return (CCmd)arrStack[i];
                else if (arrStack[i] is CXPATHCmd && ((CXPATHCmd)arrStack[i]).QueryName.ToUpper() == queryName.ToUpper())
                    return (CCmd)arrStack[i];
            }
            return null;
        }

        ArrayList GetParserParameters(string text)
        {
            int offset = 0;
            int io = text.IndexOf("(");
            if (io != -1)
            {
                //text = text.Replace("(","").Replace(")","");
                offset += text.IndexOf("(") + 1; // we removed the opening bracket
                int lio = text.LastIndexOf(")");
                if (lio != -1)
                {
                    if (io + 1 > lio - io - 1)
                        text = "";
                    else
                        text = text.Substring(io + 1, lio - io - 1);
                }
                else throw new Exception("No closing bracket has been found while parsing command parameters: " + text);
            }

            ArrayList arrRet = new ArrayList();

            List<ParseUtils.ListParameter> parameters = ParseUtils.GetParameterList(text, '\'', ',', false);
            foreach (ParseUtils.ListParameter param in parameters)
            {
                ParserParam pp;
                pp.Index = param.Index;
                pp.Value = param.Text;//.Trim();
                arrRet.Add(pp);
                //offset += param.Length + 1; // +1 because we step over the comma
            }

            return arrRet;
        }

        /// <summary>
        /// Identifies an encode option from a keyword
        /// </summary>
        /// <param name="s">The keyword found in the command syntax</param>
        /// <returns></returns>
        public static EncodeOption GetEncodeOption(string s)
        {
            EncodeOption encOpt = EncodeOption.None;
            foreach (string enumName in Enum.GetNames(typeof(EncodeOption)))
            {
                if (s.ToUpper().Trim() == enumName.ToUpper())
                {
                    encOpt = (EncodeOption)Enum.Parse(typeof(EncodeOption), enumName, false);
                    break;
                }
            }
            return encOpt;
        }

        public string ParseFormat(string strFormat, out CultureInfo ci_in, out CultureInfo ci)
        {
            ci_in = CultureInfo.InvariantCulture;
            ci = CultureInfo.InvariantCulture;
            if (strFormat != "")
            {
                if (strFormat.StartsWith("@"))
                {
                    CCmd cmd = new CPARCmd();
                    ((CPARCmd)cmd).ParameterName = strFormat.Substring(1);
                    cmd.Parser = this;
                    strFormat = cmd.Execute(this);
                }
                strFormat = strFormat.Replace("%dash%", "#");


                if (strFormat.Contains("|"))
                {
                    string strFormat1 = strFormat.Substring(strFormat.IndexOf("|") + 1);
                    if (strFormat1 == "NL") strFormat1 = "nl-NL";
                    if (strFormat1 == "US") strFormat1 = "en-US";
                    try { ci_in = CultureInfo.CreateSpecificCulture(strFormat1); }
                    catch (Exception e) { };
                    strFormat = strFormat.Substring(0, strFormat.IndexOf("|"));
                }
                if (strFormat.StartsWith("NL"))
                {
                    strFormat = strFormat.Substring(2);
                    ci = CultureInfo.CreateSpecificCulture("nl-NL");
                }
                if (strFormat.StartsWith("US"))
                {
                    strFormat = strFormat.Substring(2);
                    ci = CultureInfo.CreateSpecificCulture("en-US");
                }
            }
            return strFormat;
        }
        public string ApplyFormat(string oVal, string strFormat)
        {
            string retVal = oVal;
            CultureInfo ci_in = CultureInfo.InvariantCulture;
            CultureInfo ci = CultureInfo.InvariantCulture;
            bool b_isU = false;

            if (strFormat != "")
            {
                strFormat = ParseFormat(strFormat, out ci_in, out ci);
                if (strFormat.StartsWith("U"))
                {
                    b_isU = true;
                    strFormat = strFormat.Substring(1);
                }
                if (strFormat.StartsWith(">")) { retVal = oVal.ToString().ToUpper(); }
                else if (strFormat.StartsWith("<")) { retVal = oVal.ToString().ToLower(); }
                else
                {
                    double dVal;
                    DateTime dtVal;
                    if (double.TryParse(oVal.ToString(), System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        retVal = dVal.ToString(strFormat, ci);
                    else if (DateTime.TryParse(oVal.ToString(), ci_in, System.Globalization.DateTimeStyles.None, out dtVal))
                        retVal = dtVal.ToString(strFormat, ci);
                    else
                        retVal = oVal.ToString();
                }
                if (b_isU)
                {
                    retVal = retVal.Replace(',', '.');
                }
            }
            return retVal;
        }

        //public string ApplyFormat_good(string oVal, string strFormat)
        //{
        //    string retVal = oVal;
        //    if (strFormat != "")
        //    {
        //        if (strFormat.StartsWith("@"))
        //        {
        //            CCmd cmd = new CPARCmd();
        //            ((CPARCmd)cmd).ParameterName = strFormat.Substring(1);
        //            cmd.Parser = this;
        //            strFormat = cmd.Execute(this);
        //        }
        //        bool b_isU = false;
        //        strFormat = strFormat.Replace("%dash%", "#");

        //        CultureInfo ci_in = CultureInfo.InvariantCulture;
        //        CultureInfo ci = CultureInfo.InvariantCulture;

        //        if (strFormat.Contains("|"))
        //        {
        //            string strFormat1 = strFormat.Substring(strFormat.IndexOf("|") + 1);
        //            if(strFormat1 == "NL")  strFormat1 ="nl-NL";
        //            if(strFormat1 == "US")  strFormat1 ="en-US";
        //            try { ci_in = CultureInfo.CreateSpecificCulture(strFormat1); }catch (Exception  e){};
        //            strFormat = strFormat.Substring(0, strFormat.IndexOf("|"));
        //        }
        //        if (strFormat.StartsWith("NL"))
        //        {
        //            strFormat = strFormat.Substring(2);
        //            ci = CultureInfo.CreateSpecificCulture("nl-NL");
        //        }
        //        if (strFormat.StartsWith("US"))
        //        {
        //            strFormat = strFormat.Substring(2);
        //            ci = CultureInfo.CreateSpecificCulture("en-US");
        //        }
        //        if (strFormat.StartsWith("U"))
        //        {
        //            b_isU = true;
        //            strFormat = strFormat.Substring(1);
        //        }
        //        if (strFormat.StartsWith(">")) { retVal = oVal.ToString ().ToUpper ();}
        //        else if (strFormat.StartsWith("<")) { retVal = oVal.ToString().ToLower(); }
        //        else
        //        {
        //            double dVal;
        //            DateTime dtVal;
        //            if (double.TryParse(oVal.ToString(), System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                retVal = dVal.ToString(strFormat, ci);
        //            else if (DateTime.TryParse(oVal.ToString(), ci_in, System.Globalization.DateTimeStyles.None, out dtVal))
        //                retVal = dtVal.ToString(strFormat, ci);
        //            else
        //                retVal = oVal.ToString();
        //        }
        //        if (b_isU)
        //        {
        //            retVal = retVal.Replace(',', '.');
        //        }
        //    }
        //    return retVal;
        //}
        //public string ApplyFormat(string oVal, string strFormat)
        //{
        //    string retVal = oVal;
        //    if (strFormat != "")
        //    {
        //        if (strFormat.StartsWith("@"))
        //        {
        //            CCmd cmd = new CPARCmd();
        //            ((CPARCmd)cmd).ParameterName = strFormat.Substring(1);
        //            cmd.Parser = this;
        //            strFormat = cmd.Execute();
        //        }
        //        bool b_isU = false;
        //        strFormat = strFormat.Replace("%dash%", "#");
        //        CultureInfo ci = CultureInfo.InvariantCulture;
        //        if (strFormat.StartsWith("NL"))
        //        {
        //            strFormat = strFormat.Substring(2);
        //            ci = CultureInfo.CreateSpecificCulture("nl-NL");
        //        }
        //        if (strFormat.StartsWith("US"))
        //        {
        //            strFormat = strFormat.Substring(2);
        //            ci = CultureInfo.CreateSpecificCulture("en-US");
        //        }
        //        if (strFormat.StartsWith("U"))
        //        {
        //            b_isU = true;
        //            strFormat = strFormat.Substring(1);
        //        }
        //        if (strFormat != "")
        //        {
        //            try
        //            {
        //                double dVal = Convert.ToDouble(oVal.ToString());
        //                retVal = dVal.ToString(strFormat, ci);
        //            }
        //            catch
        //            {
        //                try
        //                {
        //                    DateTime dVal = Convert.ToDateTime(oVal.ToString());
        //                    retVal = dVal.ToString(strFormat, ci);
        //                }
        //                catch
        //                {
        //                    retVal = oVal.ToString();
        //                }
        //            }
        //        }
        //        else
        //            retVal = oVal.ToString();
        //        if (b_isU)
        //        {
        //            retVal = retVal.Replace(',', '.');
        //        }
        //    }
        //    return retVal;
        //}
        private int ValidateCommandTree(CCmd root, int i, int lev)
        {
            if (root == null) return i;
            if (root.EndIndex == -1 && root.Type != CommandType.GenericCommand && root.IsBlockComand)
                if (!(root is CIFCmd && (root as CIFCmd).ElseIfCommand != null))
                    throw new Exception("The command " + root.Type.ToString() + " at line " + GetLine(root.StartIndex).ToString() + " does not have a valid closing tag!");
            root.ID = i++;
            //System.Diagnostics.Debug.WriteLine(m_templateID.ToString () +  " (" + lev.ToString() + ") " + root.Type + " " + root.ID.ToString());
            if (root.Type == CommandType.IFCommand)
                i = ValidateCommandTree((root as CIFCmd).ElseIfCommand,i,lev);
            foreach (CCmd command in root.ChildCommands)
                i = ValidateCommandTree(command,i++, lev+1);
            return i;
        }

        //internal void RequestTemplate(int templateID)
        //{
        //    if (RequestTemplateText != null)
        //        RequestTemplateText(this, templateID);
        //}

        //internal void RequestTemplateIDForInclude(string templateName)
        //{
        //    if (RequestTemplateID != null)
        //        RequestTemplateID(this, templateName);
        //}
        internal void RequestTemplate(int templateID)
        {
            string sTemplateText = m_evaluator.RequestTemplateText(templateID);
            if (sTemplateText == "notloaded")
            {
                if (RequestTemplateText != null)
                    RequestTemplateText(this, templateID);
                sTemplateText = TextForInclude;
                if (!sTemplateText.StartsWith("#startcomment#nocache#endcomment#",true,null)) 
                    m_evaluator.AddTemplateText(templateID, sTemplateText); 
            }
            if (sTemplateText != "notloaded")
            {
                TextForInclude = sTemplateText;
                AdditionalIncludeParams[UserIDKey] = m_manager.UserID.ToString(CultureInfo.InvariantCulture);
                AdditionalIncludeParams[UserLoginKey] = m_manager.UserLogin;
                AdditionalIncludeParams[UserNameKey] = m_manager.UserName;
                AdditionalIncludeParams[ServerKey] = m_manager.ServerName;
                AdditionalIncludeParams[DatabaseKey] = m_manager.DatabaseName;
            }
        }

        public void parser_RequestTemplateText(CParser sender, int templateID)
        {
            RequestTemplate(templateID);
            sender.TextForInclude = TextForInclude;
            if (AdditionalIncludeParams != null)
                foreach (DictionaryEntry de in AdditionalIncludeParams)
                    sender.AdditionalIncludeParams[de.Key] = de.Value;
        }

        public void parser_RequestTemplateID(CParser sender, string templateName)
        {
            RequestTemplateIDForInclude(templateName);
            sender.IDForInclude = this.IDForInclude;
        }

        internal void RequestTemplateIDForInclude(string templateName)
        {
            int  iTemplateID = m_evaluator.RequestTemplateIDForInclude(templateName);
            if (iTemplateID == -1)
            {
                if (RequestTemplateID != null)
                    RequestTemplateID(this, templateName);
                iTemplateID = IDForInclude;
                m_evaluator.AddTemplateIDForInclude(templateName, iTemplateID); 
            }
            IDForInclude = iTemplateID;

        }

        public void parser_LogInclude(CParser sender, LogIncludeEventArgs e)
        {
            RequestLogInclude(e.IncludeParams, e.Indentation);
        }

        internal void RequestLogInclude(Hashtable includeParameters, int indentation)
        {
            if (LogInclude != null)
                LogInclude(this, new LogIncludeEventArgs(includeParameters, indentation));
        }

        public void parser_RequestHierarchy(XDataSourceModule.SourceResult sourceResult, string parameters)
        {
            RaiseRequestHierarchy(sourceResult, parameters);
        }

        #endregion

        public void RaiseRequestHierarchy(SourceResult sourceResult, string parameters)
        {
            if (RequestHierarchy != null)
            {
                RequestHierarchy(sourceResult, parameters);
            }
        }

    }

}
//old commands have support for %par% in all sections except queryname:

//<br>
//SETVAL.val.sett.context.id
//SETPAR.par.sett.context.id
//SETFLD.qry.fld.sett.context.id
//GETPAR.par.sett.context.id

//<hr>new commands:

//<br>
//SETNULLVAL.val.sett.context.id
//SETNULLPAR.par.sett.context.id
//SETNULLFLD.qry.fld.sett.context.id
//GETDEFPAR.par.defval.sett.context.id
//DEL.sett.context.id
//DELFROM.sett.context.id
//APPENDVAL.val.sett.context.id
//APPENDFLD.qry.fld.sett.context.id
//GETITEMPAR.par.index.sett.context.id
//DELITEM.index.sett.context.id
//<hr>
