using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using XDocuments;
using XHTMLMerge;
using IDynamic;
using System.Threading;
using System.Configuration;
using System.IO;
using System.Collections.Specialized;
using System.Reflection;
using System.Xml;
using XDataSourceModule;
using System.Runtime.Serialization.Formatters.Binary;
using KubionLogNamespace;

namespace XDocumentsWebControls
{
    [ToolboxData("<{0}:XDoc runat=server></{0}:XDoc>")]
    public class XDoc : WebControl, INamingContainer
    {
        #region Controls

        Panel 
            pTop = null,
            hiddenStyle = null,
            DivXDoc = null;
        PlaceHolder 
            DocumentBody = null,
            phControls = null;
        Button 
            btnSaveDocument = null, 
            btnSaveDocumentNoUpdates = null;
        HiddenField 
            hiddenDivContent = null,
            hiddenAnswer = null,
            hiddenUnique = null,
            hiddenParameters = null;


        #endregion Controls

        public override void Dispose()
        {
            if (xdm != null)
            {
                xdm.Dispose();
                xdm = null;
            }
            base.Dispose();
        }

        #region Members

        protected XDocuments.XDocManager xdm = null;
        protected XDocuments.IXTemplate xt = null;
        //protected string parameters = null;
        protected bool m_postedDataChecked = false;
        protected bool m_isDataPosted = false;
        protected DateTime dtStart = DateTime.Now;
        protected string m_PostedData = "";

        #endregion Members

        #region Public events

        public delegate void XDocCtrlEventHandler(object sender, string newParameters);
        public event XDocCtrlEventHandler AnswerEvent;

        #endregion Public events

        #region Properties

        public string Parameters
        {
            get { return (string)ViewState["Parameters"]; }
            set { ViewState["Parameters"] = value; }
        }
        public string ConnectionString
        {
            get { return (string)ViewState["ConnectionString"]; }
            set { ViewState["ConnectionString"] = value; }
        }
        public XDocManager XDocManager
        {
            get { return xdm ; }
            set { xdm = value; }
        }
        //public string Parameters
        //{
        //    get { return parameters; }
        //    set { parameters = value; }
        //}
        //public string ConnectionString
        //{
        //    get { return connectionString; }
        //    set { connectionString = value; }
        //}


        #endregion Properties

        #region Event handlers


        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }

        //protected override object SaveControlState()
        //{
        //    return parameters != "" ? (object)parameters : null;
        //}

        //protected override void LoadControlState(object state)
        //{
        //    if (state != null)
        //    {
        //        parameters = (string)state;
        //    }
        //}

        protected override void OnLoad(EventArgs e)
        {
            if (!ChildControlsCreated)
                EnsureChildControls();
            if (Page.IsPostBack == true) RegisterScripts();

            CheckPostData();
        }

        protected override void CreateChildControls()
        {
            pTop = new Panel();
            pTop.ID = "panelTop";
            pTop.Style[HtmlTextWriterStyle.PaddingLeft] = "0px";
            pTop.Style[HtmlTextWriterStyle.PaddingRight] = "0px";
            pTop.Style[HtmlTextWriterStyle.Width] = "100%";
            pTop.Style[HtmlTextWriterStyle.Height] = "100%";

            DivXDoc = new Panel();
            DivXDoc.ID = "DivXDoc";
            DivXDoc.Style[HtmlTextWriterStyle.Width] = "100%";
            DivXDoc.Style[HtmlTextWriterStyle.Height] = "100%";

            hiddenDivContent = new HiddenField();
            hiddenDivContent.ID = "hiddenDivContent";

            hiddenAnswer = new HiddenField();
            hiddenAnswer.ID = "hiddenAnswer";

            hiddenStyle = new Panel ();
            hiddenStyle.ID = "hiddenStyle";
            hiddenStyle.Style[HtmlTextWriterStyle.Display] = "none";

            hiddenUnique = new HiddenField();
            hiddenUnique.ID = "hiddenUnique";

            hiddenParameters = new HiddenField();
            hiddenParameters.ID = "hiddenParameters";

            phControls = new PlaceHolder();
            phControls.ID = "phControls";

            btnSaveDocument = new Button();
            btnSaveDocument.ID = "btnSaveDocument";
            btnSaveDocument.Text = "Save Document";
            btnSaveDocument.Style[HtmlTextWriterStyle.Display] = "none";
            btnSaveDocument.Click += new EventHandler(btnSaveDocument_Click);


            btnSaveDocumentNoUpdates = new Button();
            btnSaveDocumentNoUpdates.ID = "btnSaveDocumentNoUpdates";
            btnSaveDocumentNoUpdates.Text = "Save Document No Updates";
            btnSaveDocumentNoUpdates.Style[HtmlTextWriterStyle.Display] = "none";
            btnSaveDocumentNoUpdates.Click += new EventHandler(btnSaveDocumentNoUpdates_Click);

            DocumentBody = new PlaceHolder();
            //DocumentBody.Style[HtmlTextWriterStyle.Width] = "100%";
            //DocumentBody.Style[HtmlTextWriterStyle.Height] = "100%";
            DocumentBody.ID = "DocumentBody";
            //MIMI
            checkBuildPage();

            DivXDoc.Controls.Add(DocumentBody);

            pTop.Controls.Add(btnSaveDocument);
            pTop.Controls.Add(btnSaveDocumentNoUpdates);
            pTop.Controls.Add(hiddenAnswer);
            pTop.Controls.Add(hiddenStyle); 
            pTop.Controls.Add(hiddenDivContent);
            pTop.Controls.Add(hiddenUnique);
            pTop.Controls.Add(hiddenParameters);
            pTop.Controls.Add(phControls);
            pTop.Controls.Add(DivXDoc);

            this.Controls.Add(pTop);

            base.CreateChildControls();
        }
        void checkBuildPage()
        {
            if (Parameters == "") return;
            if (Page == null) return;
            //hhhh
            if (Page.IsPostBack == false) return;

            bool processControls = false;


            Hashtable providedParameters = GetHashtableFromQueryString(Parameters );
            string uc = providedParameters["UC"] as string;
            if (uc == null) uc = "";
            if (uc.ToLower() == "true" || uc == "1")
                processControls = true;

            if (!processControls)
            {
                if (HttpContext.Current.Request.QueryString["uc"] != null)
                {
                    uc = HttpContext.Current.Request.QueryString["uc"];
                    if (uc.ToLower() == "true" || uc == "1")
                        processControls = true;
                }
            }

            if (!Page.IsPostBack || processControls)
                if (HttpContext.Current.Request.QueryString["postbackGuid"] == null)
                    BuildPage();

        }

        public void btnSaveDocument_Click(object sender, System.EventArgs e)
        {
            SaveDocument(true);
        }

        public void btnSaveDocumentNoUpdates_Click(object sender, System.EventArgs e)
        {
            SaveDocument(false);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public string GetRenderString()
        {
            string sResponse = "";
            if (xt != null)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter tw = new StringWriter(sb);
                HtmlTextWriter hw = new HtmlTextWriter(tw);

                DocumentBody.RenderControl(hw);

                hw.Close();
                tw.Close();

                sResponse = sb.ToString();

            }
                return sResponse ;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        protected override void Render(HtmlTextWriter writer)
        {
            
            string toWrite = "";
            if ((xt != null) && (xt.IsData))
            {
                StringBuilder sb = new StringBuilder();
                StringWriter tw = new StringWriter(sb);
                HtmlTextWriter hw = new HtmlTextWriter(tw);

                DocumentBody.RenderControl(hw);

                hw.Close();
                tw.Close();

                toWrite = sb.ToString();

                //HttpContext.Current.Controls.Clear();
                //HttpContext.Current.Controls.Add(new LiteralControl(toWrite));

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Write(toWrite);
                try
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    //HttpContext.Current.Response.End();
                }
                catch (ThreadAbortException)
                {
                }
            }
            else
            {
                //base.Render(writer);
                StringBuilder sb1 = new StringBuilder();
                StringWriter tw1 = new StringWriter(sb1);
                HtmlTextWriter hw1 = new HtmlTextWriter(tw1);
                base.Render(hw1);
                hw1.Close();
                tw1.Close();
                toWrite = sb1.ToString();
                writer.Write(toWrite );
            }
            
            //WriteLog 
            //if (xdm != null) xdm.RequestLog(HttpContext.Current, dtStart, m_PostedData, toWrite);
        }



        #endregion Event handlers

        #region Public methods
        bool bClearCache = false;
        bool bClearIncludeOnce = false;
        bool bClearTemplatesCache = false;

        public void ClearCache()
        {
            bClearCache = true;
        }
        public void ClearIncludeOnce()
        {
            bClearIncludeOnce = true;
        }
        public void ClearTemplatesCache()
        {
            bClearTemplatesCache = true;
        }
        private XDocManager getXDocManager()
        {
            if (xdm == null)
                if (ConnectionString == "")
                    xdm = new XDocManager(ConnectionString);
                else
                    xdm = new XDocManager();
            xdm.myHttpContext = HttpContext.Current;
            return xdm;
        }
        
        bool inBuildPage = false;
        public void BuildPage()
        {
            if (inBuildPage) return;
            inBuildPage = true;
            dtStart = DateTime.Now;
            try
            {
                if (!this.ChildControlsCreated)
                    EnsureChildControls();
                DocumentBody.Controls.Clear();
                int templateID = -1;
                string templateName = "";

                IdentifyTemplate(out templateID, out templateName);

                xdm = getXDocManager();

                xt = null;

                string error = "";
                try
                {
                    if (templateID > -1)
                        xt = xdm.LoadTemplate(templateID);
                    else
                        xt = xdm.LoadTemplate(templateName);

                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }

                if (xt == null)
                {
                    if (templateID == 0)
                        throw new Exception("Neither the TemplateID nor the TemplateName parameter has been specified!");
                    string err = "Cannot find template";
                    if (templateName != "")
                        err += "(TemplateName = " + templateName + ")";
                    else
                        err += "(TemplateID = " + templateID.ToString() + ")";
                    err += Environment.NewLine + error;
                    throw (new Exception(err));
                }
                Hashtable providedParameters = GetHashtableFromQueryString(Parameters );
                ShowTemplate(providedParameters);
            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                bool dataTemplate = (xt != null && xt.IsData) || new WebUtils().ShowErrorAsData();
                ShowError(ex.Message, dataTemplate);
            }
            finally
            {
                if (xdm!=null) xdm.Dispose();
            }

            inBuildPage = false;
        }

        public void PageLoad(string initParameters)
        {
            string parameters = GetQueryString();
            if ((HttpContext.Current.Request.QueryString["callbackGuid"] == null) && (HttpContext.Current.Request.QueryString["postbackGuid"] == null)) parameters = initParameters;
            Parameters = parameters;
            if (!Page.IsPostBack)
                if (HttpContext.Current.Request.QueryString["postbackGuid"] == null)
                {
                    DateTime dt1 = DateTime.Now;
                    BuildPage();
                    DateTime dt2 = DateTime.Now;
                    //System.Diagnostics.Debug.WriteLine(dt2.Subtract(dt1));  
                }
        }
        #endregion Public methods

        #region Private methods
        string GetQueryString()
        {
            string cliUUID = "";
            string ret = "";
            foreach (string key in HttpContext.Current.Request.QueryString.AllKeys)
            {
                ret += key + "=" + HttpUtility.UrlEncode(HttpContext.Current.Request.QueryString[key], HttpContext.Current.Request.ContentEncoding) + "&";
                if (key.ToUpper() == "UUID")
                    cliUUID = HttpContext.Current.Request.QueryString[key];
            }

            string srvUUID = (string)HttpContext.Current.Session["UUID"];
            if (cliUUID == "")
            {
                srvUUID = System.Guid.NewGuid().ToString("N"); HttpContext.Current.Session["UUID"] = srvUUID;
            }
            else
                if (srvUUID == null) { srvUUID = cliUUID; HttpContext.Current.Session["UUID"] = srvUUID; }
                else
                    if (cliUUID != srvUUID)
                    {
                        ret += "cliUUID=" + cliUUID + "&srvUUID=" + srvUUID + "&";
                        HttpContext.Current.Session.Remove("UUID");
                    }
            string sSessionID = (string)HttpContext.Current.Session["SessionID"];
            if (sSessionID == null) { sSessionID = System.Guid.NewGuid().ToString("N"); HttpContext.Current.Session["SessionID"] = sSessionID; }
            ret += "SessionID=" + sSessionID;
            if (ret.EndsWith("&"))
                ret = ret.Remove(ret.Length - 1);
            return ret;
        }

        void CheckPostData()
        {
            if (m_postedDataChecked)
                return;

            m_postedDataChecked = true;

            HttpRequest request = HttpContext.Current.Request;

            XmlDocument doc = new WebUtils().CheckPostData();
            if (doc != null)
            {
                m_PostedData = doc.InnerText;
                string xmlParams = (doc.ChildNodes[0] as XmlElement).GetElementsByTagName(WebUtils.POST_PARAMS_NODE)[0].InnerText;
                string xmlTemplateContent = (doc.ChildNodes[0] as XmlElement).GetElementsByTagName(WebUtils.POST_CONTENT_NODE)[0].InnerText;
                string xmlAnswer = (doc.ChildNodes[0] as XmlElement).GetElementsByTagName(WebUtils.POST_ANSWER_NODE)[0].InnerText;
                string xmlUniqueID = (doc.ChildNodes[0] as XmlElement).GetElementsByTagName(WebUtils.POST_UNIQUEID_NODE)[0].InnerText;

                hiddenDivContent.Value = xmlTemplateContent;

                if (!string.IsNullOrEmpty(xmlParams))
                    Parameters = xmlParams;

                if (!string.IsNullOrEmpty(xmlAnswer))
                    hiddenAnswer.Value = xmlAnswer;

                if (!string.IsNullOrEmpty(xmlUniqueID))
                    hiddenUnique.Value = xmlUniqueID;

                m_isDataPosted = true;
                Parameters = "POST=1&" + Parameters ;
                SaveDocument(true);
            }
        }

        void IdentifyTemplate(out int templateID, out string templateName)
        {
            templateID = -1;
            templateName = "";
            string oTemplate = RequestQueryString("TemplateID");
            if (oTemplate != null)
            {
                if (!int.TryParse(oTemplate.ToString(), out templateID))
                    templateID = -1;
            }
            else
            {
                oTemplate = RequestQueryString("TemplateName");
                if (oTemplate != null)
                    templateName = oTemplate.ToString();
                else
                {
                    oTemplate = RequestQueryString("Context");
                    if (oTemplate != null)
                        if (m_isDataPosted) templateName = oTemplate.ToString() + "_Post";
                        else templateName = oTemplate.ToString() + "_BI";
                    
                    else 
                        templateID = 0;
                }
            }
        }

        void ToggleButtonVisible(Button button, bool visible)
        {
            string displayStyle = visible ? "inline" : "none";
            //button.Style["display"] = displayStyle;
            button.Style[HtmlTextWriterStyle.Display] = displayStyle;

        }

        string RequestQueryString(string item)
        {
            if (Parameters != null)
            {
                string[] aParams = Parameters.Split('&');
                foreach (string sParam in aParams)
                {
                    if (sParam.ToUpper().StartsWith(item.ToUpper() + "="))
                    {
                        //return HttpUtility.UrlDecode(sParam.Substring(item.Length + 1));
                        //return HttpUtility.UrlDecode(sParam.Substring(item.Length + 1),Encoding.Default );
                        // return Utils.XDocUrlDecode(sParam.Substring(item.Length + 1));
                        return HttpUtility.UrlDecode(sParam.Substring(item.Length + 1), HttpContext.Current.Request.ContentEncoding);
                    }
                }
            }
            return null;
        }
        public string ParseBlocks(string sText,int iBlocks)
        {//to do: remove "empty" style and script blocks
            string sRESEED = "1";
            string sSEED = "aaaa";
            string sSeedBlock = "";
            XDocCache m_xdocCache = null;
            if (HttpContext.Current != null) if (HttpContext.Current.Session != null) m_xdocCache = (XDocCache)HttpContext.Current.Session["XDocCache"];
            if (m_xdocCache!= null)
            {
                sRESEED = m_xdocCache.GetSV("_RESEED", "_", "1");
                sSEED = m_xdocCache.GetSV("_SEED", "_", sSEED);
                m_xdocCache.DelSV("_RESEED", "_", false);
                m_xdocCache.DelSV("_SEED", "_", false);
            }
            if (sRESEED == "1")
            {
                sSEED = Guid.NewGuid().ToString();
                HttpContext.Current.Session["SEED"] = sSEED;
            }
            else
            {
                if (HttpContext.Current != null) if (HttpContext.Current.Session != null) sSEED = (string)HttpContext.Current.Session["SEED"];
            }
            System.Diagnostics.Debug.WriteLine("Page_seed:" + sSEED + " on " + HttpContext.Current.Request.QueryString.ToString());
            sSeedBlock = "[block_script]_seed='" + sSEED  + "';[block]";
            
            sText = "[block_xdocmain]" + sSeedBlock + sText.Trim() + "[block]";

            int i=0;
            int lvl=-1;
            string[] aLvlText= new string [10];
            string[] aLvlBlock= new string [10];

            int ni = sText .IndexOf ("[block",1);
            while (ni!=-1)
            {
                string s = sText.Substring(i, ni - i);
                if (s.StartsWith("[block]"))
                {
                    if(lvl==0) throw new Exception ("invalid blocks construction: lvl-1");
                    aLvlText[lvl] += "[block]";
                    s = s.Substring("[block]".Length, s.Length - "[block]".Length);
                    i = ni;// +"[block]".Length - 1;
                    aLvlBlock [lvl-1] += aLvlText [lvl]+aLvlBlock [lvl];
                    //aLvlText [lvl]="";
                    //aLvlBlock [lvl]="";
                    lvl--;
                    aLvlText[lvl] += s;
                }
                else
                {
                    lvl++;
                    if (lvl == 10) throw new Exception("invalid blocks construction lvl10");
                    aLvlText[lvl] = s;
                    aLvlBlock [lvl]="";
                    i=ni;
                }
                ni = sText .IndexOf ("[block",i+1);
            }
            if(lvl!=0) 
                throw new Exception ("invalid blocks construction lvl0");
            aLvlText [0]+=sText .Substring (i,sText .Length -i);
            aLvlText[0] = aLvlText[0].Substring("[block_xdocmain]".Length, aLvlText[0].Length - "[block_xdocmain]".Length - "[block]".Length);
            string sResult;
            if (iBlocks == 1)
            {
                sResult = aLvlBlock[0];
            }
            else
            {
                sResult = aLvlText[0];
                if ((aLvlBlock[0].Trim () != "")&&(aLvlBlock[0].Trim () != sSeedBlock ))
                {
                    if (xt.IsData) throw new Exception("template " + xt.Name + " should not be data template");
                    sResult += "<pre id=\"hiddenblocksdiv\" style=\"display:none\">";
                    sResult += Utils.Encode( aLvlBlock[0],EncodeOption.HTMLEncode  );
                   // sResult += aLvlBlock[0];
                    sResult += "</pre><script>o=document.getElementById(\"hiddenblocksdiv\");_parseBlock(_htmlDecode(o.innerHTML));o.innerHTML=\"\";o.parentNode.removeChild(o);</script>";
//                    sResult += "</pre><script>o=document.getElementById(\"hiddenblocksdiv\");_parseBlock(_htmlDecode(o.innerHTML));</script>";

                    //if (xt.IsData) throw new Exception("template " + xt.Name + " should not be data template");
                    //sResult += "<div id=\"hiddenblocksdiv\" style=\"display:none\">";
                    //// sResult += Utils.Encode( aLvlBlock[0],EncodeOption.HTMLEncode  );
                    //sResult += aLvlBlock[0];
                    //sResult += "</div><script>o=document.getElementById(\"hiddenblocksdiv\");_parseBlock(o.innerHTML,1);o.innerHTML=\"\";o.parentNode.removeChild(o);</script>";
                }
            }
            sResult = sResult.Replace("[block_script][block]", "");
            sResult = sResult.Replace("[block_style][block]", "");
            return sResult;
        }
        //public string ShiftBlocks(string s_text)
        //{

        //    string sBlocks = "[blockssection]";
        //    if (s_text.Contains("[blockssection]"))
        //    {
        //        sBlocks = s_text.Substring(s_text.IndexOf("[blockssection]"), s_text.Length - s_text.IndexOf("[blockssection]"));
        //        s_text = s_text.Substring(0, s_text.IndexOf("[blockssection]") - 1);
        //    }

        //    Regex regexPar = new Regex(@"\[block_[^\]\#]+\](.)*?\[block\]", RegexOptions.Singleline | RegexOptions.IgnoreCase);
        //    MatchCollection mc = regexPar.Matches(s_text);
        //    for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
        //    {
        //        Match m = mc[matchIndex];
        //        string sBlock = m.Value;
        //        s_text = s_text.Replace(sBlock, "");
        //        sBlocks += sBlock;
        //    }
        //    return s_text + sBlocks;
        //}

        // long GetObjectSize(object obj)
        //{
        //    BinaryFormatter bf = new BinaryFormatter();
        //    MemoryStream ms = new MemoryStream();
        //    bf.Serialize(ms, obj);
        //    long size = ms.Length;
        //    ms.Dispose();
        //    return size;
        //}
        void ShowTemplate(Hashtable parameters)
        {
           bool unattended = false;

           XDocCache m_xdocCache = null;
           XDocCacheTemplates m_xdocCacheTemplates = null;
           Hashtable hHeaders = null;

           string m_Cache = "";
           if (HttpContext.Current != null)  if (HttpContext.Current.Session != null) m_Cache = (string)HttpContext.Current.Session["Cache"];
           if (HttpContext.Current != null)  if (HttpContext.Current.Session != null) m_xdocCache = (XDocCache)HttpContext.Current.Session["XDocCache"];
           if (HttpContext.Current != null)  if (HttpContext.Current.Application != null) m_xdocCacheTemplates = (XDocCacheTemplates)HttpContext.Current.Application["XDocCacheTemplates"];
           if (m_xdocCache != null) { xdm.DocCache = m_xdocCache; }
           if (m_xdocCacheTemplates != null) { xdm.DocCacheTemplates = m_xdocCacheTemplates; }
           if (HttpContext.Current != null) if (HttpContext.Current.Application != null) hHeaders = (Hashtable)HttpContext.Current.Application["Headers"];
           if (hHeaders == null) { hHeaders = ReadHTTPHeaders(); }


           if (HttpContext.Current.Request.QueryString["cache"] != null)
           {
               m_Cache = HttpContext.Current.Request.QueryString["cache"];
               HttpContext.Current.Session["Cache"] = m_Cache;
           }
           if (m_Cache == "0") ClearTemplatesCache();

           if (HttpContext.Current.Request.QueryString["init"] != null)
               if (HttpContext.Current.Request.QueryString["init"] == "1") ClearCache();

           if ((HttpContext.Current.Request.QueryString["callbackGuid"] == null) && (HttpContext.Current.Request.QueryString["postbackGuid"] == null)) ClearIncludeOnce();


           if (bClearCache ) if (xdm.DocCache != null) xdm.DocCache.ClearCache ();
           if (bClearIncludeOnce) if (xdm.DocCache != null) xdm.DocCache.ClearIncludeOnce();
           if (bClearTemplatesCache) if (xdm.DocCacheTemplates != null) xdm.DocCacheTemplates.ClearTemplatesCache();
           if (HttpContext.Current != null) { if (xdm.DocCache == null) xdm.DocCache = new XDocCache(); xdm.DocCache.FillCookies(HttpContext.Current.Request.Cookies); }
           IXDocument doc = xt.ProcessTemplate(parameters);

            m_xdocCache = xdm.DocCache;
            m_xdocCacheTemplates = xdm.DocCacheTemplates;

            if (HttpContext.Current != null) if (HttpContext.Current.Session != null) HttpContext.Current.Session["XDocCache"] = m_xdocCache;
            if (HttpContext.Current != null) if (HttpContext.Current.Application != null) HttpContext.Current.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
            if (HttpContext.Current != null) if (HttpContext.Current.Application != null) HttpContext.Current.Application["Headers"] = hHeaders;
            if (HttpContext.Current != null) if (m_xdocCache != null) m_xdocCache.WriteCookies(HttpContext.Current.Response.Cookies); 
            
            if (parameters["NOBLOCKS"] == null)
            {
                if (parameters["BLOCK"] != null)
                {
                    doc.Content = ParseBlocks(doc.Content, 1);
                    xt.IsData = true;
                }
                else
                    doc.Content = ParseBlocks(doc.Content, 0);
            }
            //perform redirect 
            if (xt.Unattended)
            {
                PerformRedirect(doc.Redirect, parameters);
                return;
            }


            if (hiddenAnswer.Value == "")
                hiddenAnswer.Value = xt.Redirect;


            if (RequestQueryString("UC") != null && (RequestQueryString("UC").ToUpper() == "TRUE" || RequestQueryString("UC") == "1"))
                ProcessUC(doc.Content);

            else if (doc.Content.Contains ("<ctrl:"))
                ProcessUC(doc.Content);
            else 

                if (xt.IsData)
                {
                    AddHtmlToDocumentBody(doc.Content, xt.UniqueID);
                    return;
                }
                else
                {
                    AddHtmlToDocumentBody(doc.Content, xt.UniqueID);

                    hiddenUnique.Value = xt.UniqueID;

                    string showSave = RequestQueryString("ShowSave");

                    bool buttonVisible = !unattended && (showSave != null && (showSave.ToUpper() == "TRUE" || showSave == "1"));
                    ToggleButtonVisible(btnSaveDocument, buttonVisible);
                    ToggleButtonVisible(btnSaveDocumentNoUpdates, buttonVisible);

                    hiddenParameters.Value = XDocManager.QueryStringFromHashtable(parameters);
                }
            if(Page!=null) Page.ClientScript.RegisterClientScriptInclude("XDocumentsJS", Page.ClientScript.GetWebResourceUrl(typeof(XDocumentsWebControls.XDoc), "XDocumentsWebControls.Resources.XDocuments.js"));
            RegisterScripts();
            if (hHeaders != null) AddHTTPHeaders(Page.Response, hHeaders);
        }

        Hashtable ReadHTTPHeaders()
        {
            Hashtable hHeaders = null;
            if (ConfigurationManager.AppSettings["HTTPHeaders"] != null && ConfigurationManager.AppSettings["HTTPHeaders"] != "")
            {
                string[] aHTTPHeaders = ConfigurationManager.AppSettings["HTTPHeaders"].ToString().Split(',');
                hHeaders = CollectionsUtil.CreateCaseInsensitiveHashtable();
                foreach (string sHeader in aHTTPHeaders)
                {
                    if (ConfigurationManager.AppSettings["HTTPHeader-" + sHeader] != null)
                        hHeaders.Add(sHeader, ConfigurationManager.AppSettings["HTTPHeader-" + sHeader].ToString());
                }
            }
            return hHeaders;
        }

        void AddHTTPHeaders(HttpResponse response, Hashtable hHeaders)
        {

            foreach (string key in hHeaders.Keys)
            {
                response.AppendHeader(key, hHeaders[key].ToString());
            }

        }

        void RegisterScripts()
        {
            if (xt == null) return;
            if (Page == null) return;
            //register scripts
            string script = string.Format("function {0}_GetDivContent(){{document.getElementById(\"{1}\").value = document.getElementById(\"{2}\").innerHTML;}}{3}",
                        this.ClientID, hiddenDivContent.ClientID, DivXDoc.ClientID, Environment.NewLine);

            //function %UNIQUE%_save() { %UNIQUE%_save(1); } 
            //function %UNIQUE%_saveNoUpdates() { %UNIQUE%_saveNoUpdates(1); } 
            //function %UNIQUE%_save(withValidation) { _save("%UNIQUE%", withValidation, "ctrlXDoc_btnSaveDocument"); } 
            //function %UNIQUE%_saveNoUpdates(withValidation) { _saveNoUpdates("%UNIQUE%", withValidation, "ctrlXDoc_btnSaveDocumentNoUpdates"); } 
            //function %UNIQUE%_setAnswer(answer) { document.getElementById("ctrlXDoc_hiddenAnswer").value = answer; } 
            //function %UNIQUE%_callInitialize() { _callInitialize("%UNIQUE%"); } 
            //function %UNIQUE%_callInitializeA() { _callInitializeA("%UNIQUE%"); } 
            //function %UNIQUE%_makePostback(callbackFn) { url = window.location.href; content = document.getElementById("ctrlXDoc_DivXDoc").innerHTML; parameters = document.getElementById("ctrlXDoc_hiddenParameters").value; answer = document.getElementById("ctrlXDoc_hiddenAnswer").value; targetUniqueID = document.getElementById("ctrlXDoc_hiddenUnique").value; _makePostback(url, callbackFn, content, parameters, answer, targetUniqueID) } 

            script += string.Format(
                "function {4}_save(){3}" +
                "{{{3}" +
                "{4}_save(1);{3}" +
                "}}{3}" +
                "function {4}_saveNoUpdates(){3}" +
                "{{{3}" +
                "{4}_saveNoUpdates(1);{3}" +
                "}}{3}" +
                "function {4}_save(withValidation){3}" +
                "{{{3}" +
                "_save(\"{4}\", withValidation, \"{0}\");{3}" +
                "}}{3}" +
                "function {4}_saveNoUpdates(withValidation){3}" +
                "{{{3}" +
                "_saveNoUpdates(\"{4}\", withValidation, \"{1}\");{3}" +
                "}}{3}" +
                "function {4}_setAnswer(answer){3}" +
                "{{{3}" +
                "document.getElementById(\"{2}\").value = answer;{3}" +
                "}}{3}" +
                "function {4}_makePostback(callbackFn){3}" +
                "{{{3}" +
                "url = window.location.href;{3}" +
                "content = document.getElementById(\"{5}\").innerHTML;{3}" +
                "parameters = document.getElementById(\"{6}\").value;{3}" +
                "answer = document.getElementById(\"{7}\").value;{3}" +
                "targetUniqueID = document.getElementById(\"{8}\").value;{3}" +
                " _makePostback(url, callbackFn, content, parameters, answer, targetUniqueID);{3}" +
                "}}{3}",
                btnSaveDocument.ClientID, btnSaveDocumentNoUpdates.ClientID, hiddenAnswer.ClientID, " ", xt.UniqueID,
                DivXDoc.ClientID, hiddenParameters.ClientID, hiddenAnswer.ClientID, hiddenUnique.ClientID);

            script += " _strInitialSessionId=\"" + (string)HttpContext.Current.Session["SESSIONID"] + "\";";
            script += " _strUUID=\"" + (string)HttpContext.Current.Session["UUID"] + "\";";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString("N"), script, true);

            string initializeScript = string.Format("_addEvent(window, \"load\", _callInitialize);");
            //string initializeScript = string.Format("if (typeof({0}_callInitialize) !== \"undefined\") window.attachEvent(\"onload\",{0}_callInitialize);", xt.UniqueID);

            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString("N"), initializeScript, true);

        }

        void AddHtmlToDocumentBody(string html, string uniqueID)
        {
            if (html != null && html != "")
            {
                btnSaveDocument.Attributes["onclick"] = this.ClientID + "_GetDivContent();";
                btnSaveDocument.Attributes["onclick"] +=   "document.forms[0].setAttribute('onsubmit', '');";
                btnSaveDocumentNoUpdates.Attributes["onclick"] = this.ClientID + "_GetDivContent();";
                btnSaveDocumentNoUpdates.Attributes["onclick"] += "document.forms[0].setAttribute('onsubmit', '');";
            }

            DocumentBody.Controls.Clear();
            LiteralControl lit = new LiteralControl();
            lit.Text = html;
            DocumentBody.Controls.Add(lit);

            if (!xt.IsData) AddUniqueMarkup(phControls, uniqueID);
        }

        /// <summary>
        /// Adds controls accessible to the template and used to save the document.
        /// Only for backward compatibility.
        /// </summary>
        void AddUniqueMarkup(PlaceHolder placeHolder, string uniqueID)
        {
            string html4buttons = "<INPUT type=\"button\" style=\"display:none\" id=\"{0}\" onclick=\"document.getElementById('{1}').click();\" />";

            string btnHtmlSave = string.Format(html4buttons, uniqueID + btnSaveDocument.ID, btnSaveDocument.ClientID);
            string btnHtmlSaveNoUpd = string.Format(html4buttons, uniqueID + btnSaveDocumentNoUpdates.ID, btnSaveDocumentNoUpdates.ClientID);

            placeHolder.Controls.Add(new LiteralControl(btnHtmlSave));
            placeHolder.Controls.Add(new LiteralControl(btnHtmlSaveNoUpd));
        }

        private void SetAnswer(string sAnswer)
        {
            if (AnswerEvent != null)
            {
                AnswerEvent(this, sAnswer);
            }
        }

        private void SaveDocument(bool promoteUpdates)
        {
            if (!this.ChildControlsCreated)
                EnsureChildControls();
            try
            {
                int templateID;
                string templateName;
                IdentifyTemplate(out templateID, out templateName);

                //xdm = new XDocManager(ConnectionString);
                xdm = getXDocManager();

                //recover the uniqueID generated at xt.ProcessTemplate(..) at the previous postback and copied in the hidden by GetDivContent()
                string uniqueID = hiddenUnique.Value;

                Hashtable providedParameters = GetHashtableFromQueryString(Parameters);
                IXDocument doc = null;
                if (templateID > -1)
                    doc = xdm.NewDocumentBasedOnTemplate(templateID);
                else
                    doc = xdm.NewDocumentBasedOnTemplate(templateName);

                if (doc == null)
                    throw new Exception("Could not load document based on TemplateID = " + templateID.ToString() + " or TemplateName = " + templateName);

/*out of memory*/
                doc.Content = hiddenDivContent.Value;
                doc.UniqueID = uniqueID;
                if (hiddenAnswer.Value != "")
                    doc.InitialRedirect = hiddenAnswer.Value;

                Hashtable newRedirectParameters = doc.Save(promoteUpdates, providedParameters);

                hiddenDivContent.Value = ""; // otherwise the document body will remain copied in this hidden

                PerformRedirect(doc.Redirect, newRedirectParameters);

                return;
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
            finally
            {
                //if (xdm != null) xdm.Dispose();
            }
        }

        void PerformRedirect(string redirect, Hashtable newRedirectParameters)
        {

            //if (m_isDataPosted)
            if ((m_isDataPosted) && (!redirect.ToUpper().StartsWith("RELOAD")) && (!redirect.ToUpper().StartsWith("ANSWER")))
            {
                ReturnAnswerToPostClient(redirect);
            }
            else
            {

                if (redirect.ToUpper().Contains("RELOAD"))
                {
                    Parameters = GetURLParameterList(newRedirectParameters);
                    BuildPage();
                }
                else
                {
                    SetAnswer(redirect);
                }
            }
        }

        void ReturnAnswerToPostClient(string answer)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Write(answer);
            try
            {
                HttpContext.Current.Response.End();
            }
            catch (ThreadAbortException)
            {

            }
        }

        private void ShowError(string sAnswer, bool dataTemplate)
        {
            if (dataTemplate)
            {
                System.Web.HttpContext.Current.Response.Clear();
                sAnswer = sAnswer.Replace(Environment.NewLine, "<br/>");
                string text = "<font color=\"red\" size=\"2\">" + sAnswer + "</font>";
                System.Web.HttpContext.Current.Response.Write(text);
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                try
                {
                    System.Web.HttpContext.Current.Response.End();
                }
                catch (ThreadAbortException)
                {

                }

            }
            else
            {
                DocumentBody.Controls.Clear();
                Literal lit = new Literal();
                sAnswer = sAnswer.Replace(Environment.NewLine, "<br/>");
                lit.Text = "<font color=\"red\" size=\"2\">" + sAnswer + "</font>";
                DocumentBody.Controls.Add(lit);
            }
        }

        private void ShowError(string sAnswer)
        {
            bool showAsData = new WebUtils().ShowErrorAsData();
            ShowError(sAnswer, showAsData);
        }


        Hashtable GetHashtableFromQueryString(string Parameters)
        {
            Hashtable ret = XDocManager.HashtableFromQueryString(Parameters);
            return ret;
        }

        string GetUniqueID()
        {
            /*string ret = btnSaveDocument.ClientID.Replace(btnSaveDocument.ID, "");
            if (ret.EndsWith("_"))
                ret = ret.Remove(ret.Length - 1);
            return ret; */

            return this.ClientID;
        }

        string GetURLParameterList(Hashtable receivedParams)
        {
            string ret = "";
            if (receivedParams == null)
                return "";

            Hashtable h = XDocManager.HashtableFromQueryString(Parameters);
            if (h != null)
                foreach (DictionaryEntry de in h)
                    if (!receivedParams.ContainsKey(de.Key))
                        receivedParams[de.Key] = de.Value;

            IDictionaryEnumerator en = receivedParams.GetEnumerator();
            while (en.MoveNext())
            {
                ret += en.Key.ToString() + "=" + Utils.Encode(en.Value.ToString(), EncodeOption.URLEncode) + "&";
            }

            if (ret.EndsWith("&"))
                ret = ret.Substring(0, ret.Length - 1);

            return ret;
        }

        #endregion Private methods

        #region Dynamic Usercontrols

        Hashtable GetUCParameters(string tag)
        {
            int fio = tag.IndexOf('(');
            Hashtable ret = CollectionsUtil.CreateCaseInsensitiveHashtable();
            if (fio != -1)
            {
                int lio = tag.LastIndexOf(')');
                //string strRawParams = tag.Substring(fio + 1, lio - fio - 1);

                string[] rawParams = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(tag, ("'").ToCharArray()[0], ',', true));

                foreach (string param in rawParams)
                {
                    int io = param.IndexOf("=");
                    if (io != -1)
                    {
                        string name = param.Substring(0, io).Trim();
                        string val = param.Substring(io + 1).Trim();
                        if (val.StartsWith("'") && val.Length > 0)
                            val = val.Substring(1);
                        if (val.EndsWith("'") && val.Length > 0)
                            val = val.Substring(0, val.Length - 1);

                        ret[name] = val;
                    }
                    else
                        throw new Exception("The parameter " + param + " does not contain the '=' character");

                }
            }
            return ret;
        }

        string GetUCName(string tag)
        {
            int fio = tag.IndexOf('(');
            if (fio == -1)
                return tag.Substring(4, tag.Length - (4 + 2));
            return tag.Substring(4, fio - 4);
        }

        string GetWebControlName(string tag)
        {
            if (tag == null || tag == "")
                return "";
            string key = "ctrl:";
            int fio = tag.ToLower().IndexOf(key);
            if (fio != -1)
            {
                int lio = tag.IndexOf(" ", fio + 1);
                return tag.Substring(fio + key.Length, lio - fio - key.Length).TrimEnd().TrimStart();
            }
            return "";

        }

        SortedList<int, UCMatch> GetUCMatches(string originalText)
        {

            SortedList<int, UCMatch> ret = new SortedList<int, UCMatch>();
            if (originalText == null)
                return ret;

            string ucId = "<uc:";
            string ctrlId = "<ctrl:";
            string closingBr = "/>";

            string text = originalText.ToLower();

            int firstIndex = 0;

            while (firstIndex != -1)
            {
                int idx = text.IndexOf(ucId, firstIndex);
                if (idx != -1)
                {
                    int idx2 = text.IndexOf(closingBr, idx + 1);
                    if (idx2 == -1)
                        throw new Exception("User control tag is missing the closing angular bracket");
                    string tag = originalText.Substring(idx, idx2 - idx + closingBr.Length);
                    UCMatch match = new UCMatch(tag, idx, true);
                    ret[match.Index] = match;
                    firstIndex = idx + match.Length;
                }
                else
                    break;
            }

            firstIndex = 0;

            while (firstIndex != -1)
            {
                int idx = text.IndexOf(ctrlId, firstIndex);
                if (idx != -1)
                {
                    int idx2 = text.IndexOf(closingBr, idx + 1);
                    if (idx2 == -1)
                        throw new Exception("Web control tag is missing the closing angular bracket");
                    string tag = originalText.Substring(idx, idx2 - idx + closingBr.Length);
                    UCMatch match = new UCMatch(tag, idx, false);
                    ret[match.Index] = match;
                    firstIndex = idx + match.Length;
                }
                else
                    break;
            }

            return ret;

        }

        void ProcessUC(string docHtml)
        {

            SortedList<int, UCMatch> allMatches = GetUCMatches(docHtml);


            //add first part of the template, before the first UC
            if (allMatches.Count > 0)
            {
                int firstOccurence = allMatches.Keys[0];
                if (allMatches[firstOccurence].Index > 0)
                {
                    string firstPart = docHtml.Substring(0, allMatches[firstOccurence].Index);
                    DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(firstPart));
                }
            }

            for (int k = 0; k < allMatches.Count; k++)
            {
                UCMatch m = allMatches[allMatches.Keys[k]];

                // add the middle template parts (between controls)
                if (k > 0)
                {
                    UCMatch previousMatch = allMatches[allMatches.Keys[k - 1]];
                    int start = previousMatch.Index + previousMatch.Length;
                    int end = m.Index - 1;
                    string templateText = docHtml.Substring(start, end - start + 1);
                    DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(templateText));
                }

                if (m.IsUC) // this is an usercontrol
                {
                    string tag = m.Value;

                    string ucName = GetUCName(tag);
                    Control uc = TemplateControl.LoadControl(ucName + ".ascx");

                    string ucID = System.IO.Path.GetFileNameWithoutExtension(ucName);
                    int count = 0;
                    bool found = true;
                    while (found)
                    {
                        if (DocumentBody.FindControl(ucID + "_" + count.ToString()) != null)
                        {
                            found = true;
                            count++;
                        }
                        else
                            found = false;
                    }
                    ucID += "_" + count.ToString();
                    uc.ID = ucID;

                    DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, uc);

                    Hashtable controlParameters = GetUCParameters(tag);

                    (uc as IDynamicUC).SetParameters(controlParameters);
                    (uc as IDynamicUC).DynamicUCEvent += new DynamicUCEventHandler(XDocUC_DynamicUCEvent);
                }
                else // this is a web control
                {
                    string typeName = GetWebControlName(m.Value);
                    if (typeName == "")
                        throw new Exception("The web control " + m.Value + " does not have a type name");

                    Hashtable tagAttributes = ParseUtils.TagAttributes(m.Value);

                    string path = ParseUtils.GetTagAttribute(tagAttributes, "assembly");
                    if (path == "")
                        throw new Exception("The web control " + m.Value + " does not have an assembly attribute");

                    path = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Bin"), path);

                    Assembly assmbl = Assembly.LoadFrom(path);

                    Control ctrl = assmbl.CreateInstance(typeName) as Control;
                    
                    if (ctrl != null)
                    {
                        string ctrlID = tagAttributes["id"] as String;
                        if (ctrlID != "")
                        {
                            ctrl.ID = ctrlID;

                            IDictionaryEnumerator en = tagAttributes.GetEnumerator();
                            while (en.MoveNext())
                            {
                                if (en.Key.ToString().ToLower() == "assembly" || en.Key.ToString().ToLower() == "id")
                                    continue;
                                try
                                {
                                    PropertyInfo pi = assmbl.GetType(typeName).GetProperty(en.Key.ToString());
                                    pi.SetValue(ctrl, en.Value.ToString(), null);
                                }
                                catch{};

                            }

                            DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, ctrl);
                            ////MIMI
                            //m_ctrl = m.Value;
                        }
                    }
                }
            }

            // add the last template part

            if (allMatches.Count > 0)
            {
                int lastOccurence = allMatches.Keys[allMatches.Count - 1];
                if (allMatches[lastOccurence].Index > 0)
                {
                    int index = allMatches[lastOccurence].Index + allMatches[lastOccurence].Length;
                    if (index < docHtml.Length)
                    {
                        string lastPart = docHtml.Substring(index);
                        DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(lastPart));
                    }
                }
            }
            else // there is no user control or web control
            {
                DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(docHtml));
            }

            string separator = this.ClientID == "" ? "" : "_";
            btnSaveDocument.Attributes["onclick"] = this.ClientID + separator + "GetDivContent();";
            btnSaveDocument.Attributes["onclick"] += "document.forms[0].setAttribute('onsubmit', '');";
            btnSaveDocumentNoUpdates.Attributes["onclick"] = this.ClientID + separator + "GetDivContent();";
            btnSaveDocumentNoUpdates.Attributes["onclick"] += "document.forms[0].setAttribute('onsubmit', '');";
            if (!xt.IsData) AddUniqueMarkup(DocumentBody, xt.UniqueID);
        }

        void XDocUC_DynamicUCEvent(object sender, DynamicUCEventArgs e)
        {
            SetAnswer(e.EventParameter.ToString());
        }

        #endregion Dynamic Usercontrols

        private class UCMatch
        {
            public int Index = -1;
            public string Value = "";
            public bool IsUC = true;

            public int Length
            {
                get
                {
                    if (Value != null)
                        return Value.Length;
                    return 0;

                }
            }

            public UCMatch(string value, int index, bool isUC)
            {
                Index = index;
                Value = value;
                IsUC = isUC;
            }

            public override string ToString()
            {
                if (Value != null)
                    return Value;
                return "";
            }
        }

        public string GetHTML(string strTemplate, string strParameters)
        {
            return GetHTML(strTemplate, strParameters, HttpContext.Current  );
        }
        public string GetHTML(string strTemplate, string strParameters, HttpContext context)
        {
            //XDocManager xdoc = new XDocManager();
            xdm = getXDocManager();

            XDocCache m_xdocCache = null;
            XDocCacheTemplates m_xdocCacheTemplates = null;

            //if (context != null) if (context.Application != null) m_xdocCache = (XDocCache)context.Application["XDocCache"];
            if (context != null) if (context.Session != null) m_xdocCache = (XDocCache)context.Session["XDocCache"];
            if (context != null) if (context.Application != null) m_xdocCacheTemplates = (XDocCacheTemplates)context.Application["XDocCacheTemplates"];

            if (m_xdocCache != null) { xdm.DocCache = m_xdocCache; }
            if (m_xdocCacheTemplates != null) { xdm.DocCacheTemplates = m_xdocCacheTemplates; }

            IXTemplate xt = xdm.LoadTemplate(strTemplate);
            IXDocument xd = xt.ProcessTemplate(strParameters);
            xd.Content = this.ProcessUC2String(xd.Content);

            m_xdocCache = xdm.DocCache;
            m_xdocCacheTemplates = xdm.DocCacheTemplates ;
            if (context != null) if (context.Session != null) context.Session["XDocCache"] = m_xdocCache;
            if (context != null) if (context.Application != null) context.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;

            return xd.Content;
        }
        public string GetHTML(string strTemplate, Hashtable htParameters)
        {
            return GetHTML(strTemplate, htParameters, HttpContext.Current);
        }
        public string GetHTML(string strTemplate, Hashtable htParameters, HttpContext context)
        {
            //XDocManager xdoc = new XDocManager();
            xdm = getXDocManager();

            XDocCache m_xdocCache = null;
            XDocCacheTemplates m_xdocCacheTemplates = null;

            //if (context != null) if (context.Application != null) m_xdocCache = (XDocCache)context.Application["XDocCache"];
            if (context != null) if (context.Session != null) m_xdocCache = (XDocCache)context.Session["XDocCache"];
            if (context != null) if (context.Application != null) m_xdocCacheTemplates = (XDocCacheTemplates)context.Application["XDocCacheTemplates"];

            if (m_xdocCache != null) { xdm.DocCache = m_xdocCache; }
            if (m_xdocCacheTemplates != null) { xdm.DocCacheTemplates = m_xdocCacheTemplates; }

            IXTemplate xt = xdm.LoadTemplate(strTemplate);
            IXDocument xd = xt.ProcessTemplate(htParameters);
            xd.Content = this.ProcessUC2String(xd.Content);

            m_xdocCache = xdm.DocCache;
            m_xdocCacheTemplates = xdm.DocCacheTemplates;
            if (context != null) if (context.Session != null) context.Session["XDocCache"] = m_xdocCache;
            if (context != null) if (context.Application != null) context.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;

            return xd.Content;
        }

        string ProcessUC2String(string docHtml)
        {
            StringBuilder sb = new StringBuilder();
            SortedList<int, UCMatch> allMatches = GetUCMatches(docHtml);

            //add first part of the template, before the first UC
            if (allMatches.Count > 0)
            {
                int firstOccurence = allMatches.Keys[0];
                if (allMatches[firstOccurence].Index > 0)
                {
                    string firstPart = docHtml.Substring(0, allMatches[firstOccurence].Index);
                    //DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(firstPart));
                    sb.Append(firstPart);
                }
            }

            for (int k = 0; k < allMatches.Count; k++)
            {
                UCMatch m = allMatches[allMatches.Keys[k]];

                // add the middle template parts (between controls)
                if (k > 0)
                {
                    UCMatch previousMatch = allMatches[allMatches.Keys[k - 1]];
                    int start = previousMatch.Index + previousMatch.Length;
                    int end = m.Index - 1;
                    string templateText = docHtml.Substring(start, end - start + 1);
                    //DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(templateText));
                    sb.Append(templateText);
                }

                if (m.IsUC) // this is an usercontrol
                {
                    string tag = m.Value;

                    string ucName = GetUCName(tag);
                    Control uc = TemplateControl.LoadControl(ucName + ".ascx");

                    string ucID = System.IO.Path.GetFileNameWithoutExtension(ucName);
                    int count = 0;
                    //bool found = true;
                    //while (found)
                    //{
                    //if (DocumentBody.FindControl(ucID + "_" + count.ToString()) != null)
                    //if (DocumentBody.FindControl(ucID + "_" + count.ToString()) != null)
                    //{
                    //    found = true;
                    //    count++;
                    //}
                    //else
                    //    found = false;
                    //}
                    ucID += "_" + count.ToString();
                    uc.ID = ucID;

                    //DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, uc);
                    StringBuilder sb2 = new StringBuilder();
                    StringWriter sw = new StringWriter(sb2);

                    using (HtmlTextWriter writer = new HtmlTextWriter(sw))
                    {
                        uc.RenderControl(writer);
                    }
                    sb.Append(sb2.ToString());


                    Hashtable controlParameters = GetUCParameters(tag);

                    (uc as IDynamicUC).SetParameters(controlParameters);
                    (uc as IDynamicUC).DynamicUCEvent += new DynamicUCEventHandler(XDocUC_DynamicUCEvent);
                }
                else // this is a web control
                {
                    string typeName = GetWebControlName(m.Value);
                    if (typeName == "")
                        throw new Exception("The web control " + m.Value + " does not have a type name");

                    Hashtable tagAttributes = ParseUtils.TagAttributes(m.Value);

                    string path = ParseUtils.GetTagAttribute(tagAttributes, "assembly");
                    if (path == "")
                        throw new Exception("The web control " + m.Value + " does not have an assembly attribute");

                    path = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Bin"), path);

                    Assembly assmbl = Assembly.LoadFrom(path);
                    Control ctrl = assmbl.CreateInstance(typeName) as Control;

                    if (ctrl != null)
                    {
                        string ctrlID = tagAttributes["id"] as String;
                        if (ctrlID != "")
                        {
                            ctrl.ID = ctrlID;

                            IDictionaryEnumerator en = tagAttributes.GetEnumerator();
                            while (en.MoveNext())
                            {
                                if (en.Key.ToString().ToLower() == "assembly" || en.Key.ToString().ToLower() == "id")
                                    continue;
                                try
                                {
                                    PropertyInfo pi = assmbl.GetType(typeName).GetProperty(en.Key.ToString());
                                    pi.SetValue(ctrl, en.Value.ToString(), null);
                                }
                                catch { };

                            }

                            //DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, ctrl);

                            StringBuilder sb2 = new StringBuilder();
                            StringWriter sw = new StringWriter(sb2);

                            using (HtmlTextWriter writer = new HtmlTextWriter(sw))
                            {
                                ctrl.RenderControl(writer);
                            }
                            sb.Append(sb2.ToString());
                            ////MIMI
                            //m_ctrl = m.Value;
                        }
                    }
                }
            }

            // add the last template part

            if (allMatches.Count > 0)
            {
                int lastOccurence = allMatches.Keys[allMatches.Count - 1];
                if (allMatches[lastOccurence].Index > 0)
                {
                    int index = allMatches[lastOccurence].Index + allMatches[lastOccurence].Length;
                    if (index < docHtml.Length)
                    {
                        string lastPart = docHtml.Substring(index);
                        //DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(lastPart));
                        sb.Append(lastPart);
                    }
                }
            }
            else // there is no user control or web control
            {
                //DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(docHtml));
                sb.Append(docHtml);
            }

            string separator = this.ClientID == "" ? "" : "_";
            //btnSaveDocument.Attributes["onclick"] = this.ClientID + separator + "GetDivContent();";
            //btnSaveDocument.Attributes["onclick"] += "document.forms[0].setAttribute('onsubmit', '');";
            //btnSaveDocumentNoUpdates.Attributes["onclick"] = this.ClientID + separator + "GetDivContent();";
            //btnSaveDocumentNoUpdates.Attributes["onclick"] += "document.forms[0].setAttribute('onsubmit', '');";
            //if (!xt.IsData) AddUniqueMarkup(DocumentBody, xt.UniqueID);

            return sb.ToString();
        }

    }
}
