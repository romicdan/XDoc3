using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Web;
using System.IO;


namespace XDocumentsWebControls
{
    class WebUtils
    {
        internal const string
            POST_ROOT_NODE = "xDocPostData",
            POST_PARAMS_NODE = "parameters",
            POST_CONTENT_NODE = "content",
            POST_ANSWER_NODE = "answer",
            POST_UNIQUEID_NODE = "uniqueID";

 
        public XmlDocument CheckPostData()
        {
            HttpRequest request = HttpContext.Current.Request;
            if (request.HttpMethod.ToUpper() == "POST" && request.InputStream != null && request.InputStream.Length > 0 && request.InputStream.CanRead)
            {
                //byte[] bts = new byte[request.InputStream.Length];
                //request.InputStream.Seek(0, SeekOrigin.Begin);
                //request.InputStream.Read(bts, 0, bts.Length);
                //request.InputStream.Seek(0, SeekOrigin.Begin);
                //string s = request.ContentEncoding.GetString(bts);
 
                //inject //                  string s = new StreamReader(request.InputStream).ReadToEnd();
                //[^<>]*

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ProhibitDtd = true; /* true to disable inline DTDs completely */
                settings.XmlResolver = null; /* XmlResolver objects are used to resolve external references, including external entities */
                XmlReader reader = XmlReader.Create(request.InputStream, settings);

                XmlDocument doc = new XmlDocument();
                try
                {
                    //inject //                    doc.LoadXml(s);
                    doc.Load(reader); 
                }
                catch (XmlException xmlEx)
                {
                    return null;
                }

                if (doc.ChildNodes.Count != 0 || doc.ChildNodes[0].Name == "xDocPostData")
                    return doc;
            }

            return null;
        }

        internal bool ShowErrorAsData()
        {
            return
                !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["callbackGuid"]) ||
                !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["postbackGuid"]) ||
                CheckPostData() != null;
        }
    }
}
