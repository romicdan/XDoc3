using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using XDocuments;
using System.Diagnostics;
using KubionDataNamespace;
using KubionLogNamespace;
using System.Configuration;

namespace XDocHQ
{
    public partial class DependenciesForm : Form
    {

    #region Members

        XDocManager m_manager = null;
        IDName idnaSelected;    //Dit is de geselecteerde template waarvan we de afhankelijkheden gaan tonen.        
        public int idClicked = -1; //Deze waarde geeft het ID wat getoond moet worden in XDocHQ, na dubbelklikken in het grid.
        private static TraceSwitch appSwitch = new TraceSwitch("DependenciesFormTraceSwitch", "DependenciesFormTraceSwitch","Error");
        private int _IDinNewW = -2; //This is the ID that was clicked upon with the right mouse button. It will be used to open the template in a new window
        
    #endregion Members

    #region Events

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DependenciesForm_Load(object sender, EventArgs e) //Het formulier wordt geladen en de grids worden gevuld
        {   
            this.Text = "Dependencies of " + idnaSelected.Name + " (ID " + idnaSelected.ID.ToString() + ")";
            
            try
            {
                m_manager = new XDocManager();
            }
            catch (Exception ex)
            {
                string message = "An error occured while loading the Dependencies form\n\n" + ex.Message;
                MessageBox.Show(message, "Error loading form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(message);
                Environment.Exit(0);                
            }

            DateTime nuBegin = DateTime.Now;  

            BindDependentTemplatesUsedBy(); //We vullen het linker grid met templates die het gekozen template gebruiken.
            BindDependentTemplatesUses();   //We vullen het rechter grid met templates die door het gekozen template gebruikt worden.

            if (appSwitch.TraceInfo)
            {   //We meten de tijd dat het vullen van de grids geduurd heeft, wanneer de TraceSwitch dit verlangt.
                TimeSpan deltaT = (DateTime.Now - nuBegin);
                string deltasTring = deltaT.Seconds.ToString() + "_" + deltaT.Milliseconds.ToString();
                MessageBox.Show("Loading time of BindDependentTemplates(), in seconds_milliseconds:\n\n" + deltasTring  +
                                    "\n\n\nIf you don't want to show this trace information,\n"+ 
                                    "set the TraceSwitch 'DependenciesFormTraceSwitch' to a value less than 3.\n"+ 
                                    "For instance: value=\"0\", of value=\"Off\"", "Trace-informatie  -  " + deltasTring );
            }
        }

        private void dgvUsedBy_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {            
            if (dgvUsedBy.SelectedCells.Count > 0)
            {
                int.TryParse(dgvUsedBy.SelectedCells[0].OwningRow.Cells[0].Value.ToString(),out idClicked);                
                this.Close();
            }
        }

        private void dgvUses_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (dgvUses.SelectedCells.Count > 0)
            {
                int.TryParse(dgvUses.SelectedCells[0].OwningRow.Cells[0].Value.ToString(), out idClicked);                
                this.Close();
            }
        }

        void dgvUsedBy_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {   //We hebben een right mouseclick, dus toon: Open in new window:                                        
                DataGridView.HitTestInfo testInfo = dgvUsedBy.HitTest(e.X, e.Y);    //We proberen een ID op te halen op basis van de positie van de muis 

                if (testInfo.RowIndex > -1)
                {   //There was no row found under the mouseposition, so there is no use in showing the contextmenu!
                    int.TryParse(dgvUsedBy.Rows[testInfo.RowIndex].Cells[0].Value.ToString(), out _IDinNewW);  //op het moment van de rechtermuisklik.                    
                    dgvUsedBy.ContextMenu.Show(dgvUsedBy, e.Location);
                }
            }
        }

        void dgvUses_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {   //We hebben een right mouseclick, dus toon: Open in new window:                                        
                DataGridView.HitTestInfo testInfo = dgvUses.HitTest(e.X, e.Y);    //We proberen een ID op te halen op basis van de positie van de muis 

                if (testInfo.RowIndex > -1)
                {   //There was no row found under the mouseposition, so there is no use in showing the contextmenu!
                    int.TryParse(dgvUses.Rows[testInfo.RowIndex].Cells[0].Value.ToString(),out _IDinNewW);  //op het moment van de rechtermuisklik.

                    dgvUsedBy.ContextMenu.Show(dgvUses, e.Location);        //Strange trick: The contextmenu of the other grid is used to be shown in this grid. 
                    //No assignment of a contextmenu for each grid is needed.
                }
            }
        }

        private void ContextMenuItemNewWindow_Click(object sender, System.EventArgs e)
        {
            if (_IDinNewW > -1)
            {
                Process.Start("XDocHQ.exe", "templateid=" + _IDinNewW.ToString());  //Start a new XDocHQ with the old id.
                _IDinNewW = -2;     //Make sure you don't use the old ID the next time
            }
            else
            {
                MessageBox.Show("Was not able to find a good ID.\n\nTry again.", "Failed to get ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
    #endregion Events

    #region Methods

        public DependenciesForm(IDName inSelected) //De enige contructor
        {
            InitializeComponent();

            idnaSelected = inSelected; //Zet de globale variabele op het gekozen template            

            MenuItem[] menuItems = { ContextMenuItemNewWindow };
            dgvUsedBy.ContextMenu = new ContextMenu(menuItems);             //The context menu comes about when rightclicking on a grid
        }

        void BindDependentTemplatesUsedBy() //We gaan het linker grid vullen: WORDT GEBRUIKT DOOR
        {
            ClientData cd = null;

            try
            {
                SortOrder sortOrder = dgvUsedBy.SortOrder;
                DataGridViewColumn sortColumn = dgvUsedBy.SortedColumn;
                //IDName[] templatesAll = m_manager.GetTemplatesList();
                DataTable UsedByTemplates = new DataTable();
                string strTempNaam = idnaSelected.Name.ToLower();
                string strTempID = idnaSelected.ID.ToString();

                UsedByTemplates.Columns.Add("ID", typeof(int));
                UsedByTemplates.Columns.Add("Name", typeof(string));

                if (ConfigurationManager.AppSettings["XDocConnectionString"] != null)
                {
                    cd = new ClientData(ConfigurationManager.AppSettings["XDocConnectionString"]);
                    string sSQL = "SELECT templateid ID, templatename Name FROM S_TEMPLATES " +
                                    " WHERE (TemplateContent like '%templatename=" + strTempNaam + "&%') " +
                                    " OR    (TemplateContent like '%templatename=" + strTempNaam + "\"%') " +
                                    " OR    (TemplateContent like '%templatename=" + strTempNaam + ")%') " +
                                    " OR    (TemplateContent like '%templateid=" + strTempID + "&%') " +
                                    " OR    (TemplateContent like '%templateid=" + strTempID + "\"%') " +
                                    " OR    (TemplateContent like '%templateid=" + strTempID + ")%') ";                    
                    UsedByTemplates = cd.DTExecuteQuery(sSQL);
                }
                else
                {
                    MessageBox.Show("Failed to find XDocConnectionString in the config file!", "Error in config file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //We zetten nog wat instellingen van het datagrid:
                dgvUsedBy.SuspendLayout();
                dgvUsedBy.ClearSelection();
                dgvUsedBy.DataSource = UsedByTemplates;
                dgvUsedBy.Columns["ID"].Width = 60;
                dgvUsedBy.Columns["Name"].Width = 300;
                dgvUsedBy.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgvUsedBy.ClearSelection();
                dgvUsedBy.ResumeLayout();
            }
            catch (Exception ex)
            {
                string message = "An error occured while filling the UsedBygrid (left).\n\n" + ex.Message;
                KubionLog.WriteLine(message);
                MessageBox.Show(message, "Error filling data grid", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (cd != null)
                {
                    cd.Dispose();
                }
            }
        }
        
        void BindDependentTemplatesUses() //We gaan het rechter grid vullen:
        {
            try
            {
                SortOrder sortOrder = dgvUsedBy.SortOrder;
                DataGridViewColumn sortColumn = dgvUsedBy.SortedColumn;
                IDName[] templatesAll = m_manager.GetTemplatesList();
                DataTable UsesTemplates = new DataTable();
                List<string> UsedStrings = GetUsedStrings ("templatename=");
                List<string> UsedIDs = GetUsedStrings("templateid=");
                bool blnFound = false;

                UsesTemplates.Columns.Add("ID", typeof(int));
                UsesTemplates.Columns.Add("Name", typeof(string));

                foreach (IDName idName in templatesAll)
                {
                    blnFound = false;
                    foreach (string strUsed in UsedStrings)
                    {   //We kijken of de naam voorkomt in de lijst gevonden referenties in de Content van de geselecteerde Template
                        if (strUsed == idName.Name.ToLower())
                        {
                            UsesTemplates.Rows.Add(idName.ID, idName.Name);
                            UsedStrings.Remove(strUsed); //We halen de hit uit de lijst, zodat de volgende itteratie versneld wordt
                            UsedIDs.Remove(idName.ID.ToString());
                            blnFound = true;
                            break;
                        }                        
                    }
                    if (!blnFound)
                    {   //Current idName (template) is already added to datagrid, so don't add again.
                        foreach (string strUsId in UsedIDs)
                        {   //We kijken of de id voorkomt in de lijst gevonden referenties in de Content van de geselecteerde Template
                            if (strUsId == idName.ID.ToString())
                            {
                                UsesTemplates.Rows.Add(idName.ID, idName.Name);
                                UsedIDs.Remove(strUsId); //We halen de hit uit de lijst, zodat de volgende itteratie versneld wordt                                
                                break;
                            }
                        }
                    }
                }

                int iLeftOver = UsedStrings.Count + UsedIDs.Count;

                if (iLeftOver > 0)
                {   //Hier moet  een melding komen wanneer een string niet gevonden is. Dit is simpelweg het geval wanneer er nog strings over zijn in UsedStrings. Er staat dan een verkeerde referentie in een template.
                    StringBuilder strBuild = new StringBuilder("There ");
                    if (iLeftOver == 1)
                    {
                        strBuild.Append(" is 1 template which is being referenced, but which doesn't exist:\n\n");
                    }
                    else
                    {
                        strBuild.Append("are " + iLeftOver.ToString() + " templates which are being referenced, but which don't exist:\n\n");
                    }
                    if (UsedStrings.Count > 0)
                    {
                        strBuild.AppendLine("Referenced by name:");
                    }
                    foreach (string s in UsedStrings)
                    {
                        strBuild.AppendLine("template name = \"" + s + "\"");
                    }
                    if (UsedIDs.Count > 0)
                    {
                        strBuild.AppendLine("\nReferenced by ID:");
                    }
                    foreach (string s in UsedIDs)
                    {
                        
                        strBuild.AppendLine("template ID = \"" + s + "\"");
                    }                    
                    MessageBox.Show(strBuild.ToString (),"Templates referenced don't exist!",MessageBoxButtons .OK, MessageBoxIcon.Warning);
                    KubionLog.WriteLine(strBuild.ToString ());
                }                

                //We maken nog wat instellingen voor het datagrid
                dgvUses.SuspendLayout();
                dgvUses.ClearSelection();
                dgvUses.DataSource = UsesTemplates;
                dgvUses.Columns["ID"].Width = 60;
                dgvUses.Columns["Name"].Width = 300;
                dgvUses.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgvUses.ClearSelection();
                dgvUses.ResumeLayout();
            }
            catch (Exception ex)
            {
                string message = "An error occured while filling the Usesgrid (right).\n\n" + ex.Message;
                KubionLog.WriteLine(message);
                MessageBox.Show(message, "Error filling data grid", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //private string[] MakeMatchStrings() //Hier wordt eenmalig de lijst met te zoeken strings gemaakt, zodat deze niet bij iedere itteratie opnieuw gemaakt wordt in 'IsUsedBy'
        //{
        //    string[] rtrnString = new string[3];
        //    string strTempNaam = idnaSelected.Name.ToLower();

        //    rtrnString[0] = "templatename=" + strTempNaam + "&";
        //    rtrnString[1] = "templatename=" + strTempNaam + "\"";
        //    rtrnString[2] = "templatename=" + strTempNaam + ")";

        //    return rtrnString;
        //}
        //private bool IsUsedBy(ref int docId, ref string[] strMatches) //In deze routine wordt gekeken of het geselecteerde Template aangeroepen wordt in het meegegeven template
        //{   //TODO: Probeer deze routine zo snel mogelijk te maken; dit is de bottleneck. Tests wijzing uit dat wanneer deze routine triviaal wordt (altijd blnMatch=true), dan is BindDependentTemplates() in +/- 50 milliseconde klaar, terwijl hij nu +/- 3700 milliseconde duurt, scheelt een factor 74...
        //    bool blnMatch = false;

        //    try //Ik heb snelheid getest: Het weghalen van de TRY in deze bottleneckfunctie geeft geen significantie snelheidsverbetering!!!
        //    {
        //        IXTemplate xTemp = m_manager.LoadTemplate(docId);
        //        if ((xTemp.Content.ToLower().Contains(strMatches[0])) || (xTemp.Content.ToLower().Contains(strMatches[1])) || (xTemp.Content.ToLower().Contains(strMatches[2])))
        //        {
        //            blnMatch = true;
        //        }
        //    }
        //    catch { }

        //    return blnMatch;
        //}
        private List<string> GetUsedStrings(string strIncludeType) //Hier wordt een lijst strings opgesteld waarin gevonden gebruikte strings van Templatenamen staan:
        {   
            List<string> UsedStrings = new List<string>();
            int iPos = 0; //Positie totaan waar gezocht is in Content
            int iEind = 0;
            string strGevondenTempNaam;
            int TypeLength = strIncludeType.Length;

            try
            {
                string strContent = string.Empty;

                try //Hier nog een extra Try-Catch, voor het geval dat de template met gegeven ID door een andere applicatie verwijderd is. We kunnen dan gewoon door gaan.
                {
                    IXTemplate xTemp = m_manager.LoadTemplate(idnaSelected.ID);
                    strContent = xTemp.Content.ToLower();
                }
                catch { }

                iPos = strContent.IndexOf(strIncludeType);

                while (iPos >= 0) //Zolang er nog een hit voor komt na het punt totaan waar we gezocht hebben
                {
                    iEind = EindeTemplateNaam(ref strContent, ref iPos); //Hier houdt de templatenaam op, dus we kunnen de tekst gaan knippen

                    if (iEind > iPos)
                    {
                        strGevondenTempNaam = strContent.Substring(iPos + TypeLength, iEind - iPos - TypeLength);
                        if (!(UsedStrings.Contains(strGevondenTempNaam)))
                        {
                            UsedStrings.Add(strGevondenTempNaam );
                        }
                    }

                    iPos = strContent.IndexOf(strIncludeType, iPos + TypeLength -1); //Zoek de volgende templatereferentie
                }
            }           
            catch (Exception ex)
            {
                string message = "An error occured while searching for dependencies of the selected template.\n\n" + ex.Message;
                MessageBox.Show(message, "Error filling data grid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(message);
            }            

            return UsedStrings;			
        }

        private int EindeTemplateNaam(ref string strContent, ref int iPos) //We gaan kijken op welke positie na iPos het eerste een ), & of " voor komt                        
        {   
            int iReturn, i2, i3, iLen;
            iReturn = strContent.IndexOf(")", iPos);
            i2 = strContent.IndexOf("&", iPos);
            i3 = strContent.IndexOf("\"", iPos);
            iLen = strContent.Length;

            if (iReturn < 0)
            {   //Dit is een fix voor als hij niet gevonden wordt
                iReturn = iLen + 10;
            }
            if (i2 < 0)
            {   //Dit is een fix voor als hij niet gevonden wordt
                i2 = iLen + 10;
            }
            if (i3 < 0)
            {   //Dit is een fix voor als hij niet gevonden wordt
                i3 = iLen + 10;
            }

            if (i2 < iReturn)
            {   //pak het kleinste getal
                iReturn = i2;
            }
            if (i3 < iReturn)
            {   //pak het kleinste getal
                iReturn = i3;
            }

            if (iReturn > iLen)
            {   //Blijkbaar kwam er geen afsluitteken meer voor
                iReturn = -2;
            }    
            
            return iReturn;
        }

    #endregion Methods             

    }
}