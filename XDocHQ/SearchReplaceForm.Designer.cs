namespace XDocHQ
{
    partial class SearchReplaceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVolgende = new System.Windows.Forms.Button();
            this.btnVervangen = new System.Windows.Forms.Button();
            this.btnAnnuleren = new System.Windows.Forms.Button();
            this.btnAllesVervangen = new System.Windows.Forms.Button();
            this.tbxZoekterm = new System.Windows.Forms.TextBox();
            this.tbxVervangTerm = new System.Windows.Forms.TextBox();
            this.lblZoekterm = new System.Windows.Forms.Label();
            this.lblVervangTerm = new System.Windows.Forms.Label();
            this.lblMsgBegin = new System.Windows.Forms.Label();
            this.lblAantal = new System.Windows.Forms.Label();
            this.lblMsgEind = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnVolgende
            // 
            this.btnVolgende.Location = new System.Drawing.Point(283, 12);
            this.btnVolgende.Name = "btnVolgende";
            this.btnVolgende.Size = new System.Drawing.Size(100, 24);
            this.btnVolgende.TabIndex = 3;
            this.btnVolgende.Text = "&Search next";
            this.btnVolgende.UseVisualStyleBackColor = true;
            this.btnVolgende.Click += new System.EventHandler(this.btnVolgende_Click);
            // 
            // btnVervangen
            // 
            this.btnVervangen.Location = new System.Drawing.Point(283, 42);
            this.btnVervangen.Name = "btnVervangen";
            this.btnVervangen.Size = new System.Drawing.Size(100, 24);
            this.btnVervangen.TabIndex = 4;
            this.btnVervangen.Text = "&Replace";
            this.btnVervangen.UseVisualStyleBackColor = true;
            this.btnVervangen.Click += new System.EventHandler(this.btnVervangen_Click);
            // 
            // btnAnnuleren
            // 
            this.btnAnnuleren.CausesValidation = false;
            this.btnAnnuleren.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAnnuleren.Location = new System.Drawing.Point(283, 102);
            this.btnAnnuleren.Name = "btnAnnuleren";
            this.btnAnnuleren.Size = new System.Drawing.Size(100, 24);
            this.btnAnnuleren.TabIndex = 6;
            this.btnAnnuleren.TabStop = false;
            this.btnAnnuleren.Text = "&Cancel";
            this.btnAnnuleren.UseVisualStyleBackColor = true;
            this.btnAnnuleren.Click += new System.EventHandler(this.btnAnnuleren_Click);
            // 
            // btnAllesVervangen
            // 
            this.btnAllesVervangen.Location = new System.Drawing.Point(283, 72);
            this.btnAllesVervangen.Name = "btnAllesVervangen";
            this.btnAllesVervangen.Size = new System.Drawing.Size(100, 24);
            this.btnAllesVervangen.TabIndex = 5;
            this.btnAllesVervangen.Text = "Replace &all";
            this.btnAllesVervangen.UseVisualStyleBackColor = true;
            this.btnAllesVervangen.Click += new System.EventHandler(this.btnAllesVervangen_Click);
            // 
            // tbxZoekterm
            // 
            this.tbxZoekterm.Location = new System.Drawing.Point(94, 15);
            this.tbxZoekterm.Name = "tbxZoekterm";
            this.tbxZoekterm.Size = new System.Drawing.Size(165, 20);
            this.tbxZoekterm.TabIndex = 1;
            this.tbxZoekterm.TextChanged += new System.EventHandler(this.tbxZoekterm_TextChanged);
            this.tbxZoekterm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxZoekterm_KeyDown);
            // 
            // tbxVervangTerm
            // 
            this.tbxVervangTerm.Location = new System.Drawing.Point(94, 45);
            this.tbxVervangTerm.Name = "tbxVervangTerm";
            this.tbxVervangTerm.Size = new System.Drawing.Size(165, 20);
            this.tbxVervangTerm.TabIndex = 2;
            this.tbxVervangTerm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxVervangTerm_KeyDown);
            // 
            // lblZoekterm
            // 
            this.lblZoekterm.AutoSize = true;
            this.lblZoekterm.Location = new System.Drawing.Point(2, 18);
            this.lblZoekterm.Name = "lblZoekterm";
            this.lblZoekterm.Size = new System.Drawing.Size(59, 13);
            this.lblZoekterm.TabIndex = 6;
            this.lblZoekterm.Text = "Search for:";
            // 
            // lblVervangTerm
            // 
            this.lblVervangTerm.AutoSize = true;
            this.lblVervangTerm.Location = new System.Drawing.Point(2, 48);
            this.lblVervangTerm.Name = "lblVervangTerm";
            this.lblVervangTerm.Size = new System.Drawing.Size(64, 13);
            this.lblVervangTerm.TabIndex = 7;
            this.lblVervangTerm.Text = "Replace by:";
            // 
            // lblMsgBegin
            // 
            this.lblMsgBegin.AutoSize = true;
            this.lblMsgBegin.Location = new System.Drawing.Point(91, 108);
            this.lblMsgBegin.Name = "lblMsgBegin";
            this.lblMsgBegin.Size = new System.Drawing.Size(0, 13);
            this.lblMsgBegin.TabIndex = 8;
            // 
            // lblAantal
            // 
            this.lblAantal.AutoSize = true;
            this.lblAantal.Location = new System.Drawing.Point(146, 108);
            this.lblAantal.Name = "lblAantal";
            this.lblAantal.Size = new System.Drawing.Size(0, 13);
            this.lblAantal.TabIndex = 9;
            // 
            // lblMsgEind
            // 
            this.lblMsgEind.AutoSize = true;
            this.lblMsgEind.Location = new System.Drawing.Point(187, 108);
            this.lblMsgEind.Name = "lblMsgEind";
            this.lblMsgEind.Size = new System.Drawing.Size(0, 13);
            this.lblMsgEind.TabIndex = 10;
            // 
            // SearchReplaceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnAnnuleren;
            this.ClientSize = new System.Drawing.Size(395, 152);
            this.Controls.Add(this.lblMsgEind);
            this.Controls.Add(this.lblAantal);
            this.Controls.Add(this.lblMsgBegin);
            this.Controls.Add(this.lblVervangTerm);
            this.Controls.Add(this.lblZoekterm);
            this.Controls.Add(this.tbxVervangTerm);
            this.Controls.Add(this.tbxZoekterm);
            this.Controls.Add(this.btnAllesVervangen);
            this.Controls.Add(this.btnAnnuleren);
            this.Controls.Add(this.btnVervangen);
            this.Controls.Add(this.btnVolgende);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchReplaceForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Search-and-Replace";
            this.Load += new System.EventHandler(this.SearchReplaceForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();
            this.GotFocus += new System.EventHandler(SearchReplaceForm_GotFocus);

        }
        #endregion

        private System.Windows.Forms.Button btnVolgende;
        private System.Windows.Forms.Button btnVervangen;
        private System.Windows.Forms.Button btnAnnuleren;
        private System.Windows.Forms.Button btnAllesVervangen;
        private System.Windows.Forms.TextBox tbxZoekterm;
        private System.Windows.Forms.TextBox tbxVervangTerm;
        private System.Windows.Forms.Label lblZoekterm;
        private System.Windows.Forms.Label lblVervangTerm;
        private System.Windows.Forms.Label lblMsgBegin;
        private System.Windows.Forms.Label lblAantal;
        private System.Windows.Forms.Label lblMsgEind;
    }
}