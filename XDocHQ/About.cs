using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace XDocHQ
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();

            txtConnection.Text = ConfigurationManager.AppSettings["XDocConnectionString"];
            Version ver = new Version(Application.ProductVersion);
            lbVersion.Text = ver.Major.ToString() + ".";
            if (ver.Minor.ToString().Length < 2)
            {
                lbVersion.Text += "0" + ver.Minor.ToString();
            }
            else
            {
                lbVersion.Text += ver.Minor.ToString();
            }
            if (ver.Build != 0)
            {   //Toon revisionnummer als het bestaat
                lbVersion.Text += "." + ver.Build.ToString();
            }

            txtConnection.Select(0, 0);
        }
    }
}